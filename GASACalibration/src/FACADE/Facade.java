package FACADE;

import DOMAIN.ActualDataFresim;
import DOMAIN.ActualDataNetsim;
import DOMAIN.ParametersSet;
import DOMAIN.MEMETIC.*;
import PERSISTENCE.*;
import DOMAIN.*;
import DOMAIN.Model.CorsimModel;
import DOMAIN.SPSA.SPSA;
import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

//Scheme of theta   [0] Rt, [1] SC, [2] EC, [3] MinV, [4] MaxV,  [5] Value, [6] lineNumber,[7] InNode, [8]EndNode
//Scheme of filter  [0] InNode, [1] EndNode, [2] RT, [3] calibrate?
/**
 * Unifies the functionalities from the other layers, and contains
 * the necessary attributes to run the calibration.
 *
 * @author Carlos Gaviria, Cristian Arteaga and Victor Molano
 * @layer Facade
 */
public class Facade {

    private double Coefficient_A;
    private double Coefficient_a;
    private double Coefficient_c;
    private double Coefficient_alpha;
    private double Coefficient_gamma;
    private int CounterK;
    private Map<String, Graph> Graphs;
    private static Persistence persistence;
    private int[][] initial;
    private int[][] calibrated;
    //GA
    private double Pop;
    private double Gen;
    private double CrossPer;
    private double SelPer;
    private double Step;
    private double Fat;
    private double MutPer;
    //SA
    private double IniTemp;
    private double CoolRate;
    //TS
    private double Tenure;
    private double NeighSize;
    //Other parameters
    private int simulatorRuns;

    public Facade() {
        persistence = new Persistence();
        Coefficient_A = 5;
        Coefficient_a = 7715.8;
        Coefficient_c = 2.7598;
        Coefficient_alpha = 0.6531;
        Coefficient_gamma = 1 / 6;
        CounterK = 150;
        Graphs = new HashMap<String, Graph>();

    }

    /**
     * Create a new set of parameters
     *
     * @param ps
     * @return boolean, false if error
     * @author Carlos Gaviria and Cristian Arteaga
     */
    public boolean newParametersSet(String[][] ps) {
        try {
            ParametersSet pa = new ParametersSet(ps);
            persistence.setParameters_Set(pa);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    /**
     * Saves in the path directory a set of parameters, stored into data
     * parameter
     *
     * @param data
     * @param path
     * @return boolean, false if error
     * @author Carlos Gaviria and Cristian Arteaga
     */
    public boolean saveParametersSet(String[][] data, String path) {
        boolean output = InputPersistence.saveInput(data, path);
        if (output) {
            ParametersSet pa = new ParametersSet(data);
            persistence.setParameters_Set(pa);
            return true;
        }
        return false;
    }

    /**
     * loads into Parameters_Set the data contained into a file specified in
     * path
     *
     * @param path
     * @return boolean, false if error
     * @author Carlos Gaviria and Cristian Arteaga
     */
    public boolean loadParametersSet(String path) {
        String data[][] = InputPersistence.loadInput(path);
        if (data != null) {
            ParametersSet pa = new ParametersSet(data);
            persistence.setParameters_Set(pa);
            return true;
        }
        return false;
    }

    /**
     * Creates a new Actual Data Netsim
     *
     * @param ADN_Data
     * @return boolean, false if error
     * @author Carlos Gaviria and Cristian Arteaga
     */
    public boolean newActualDataNetsim(String[][] ADN_Data) {
        try {
            ActualDataNetsim na = new ActualDataNetsim(ADN_Data);
            persistence.setActual_Data_Netsim(na);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    /**
     * saves a set of parameters(actual data netsim), stored into data parameter
     * in the directory path
     *
     * @param data
     * @param path
     * @return boolean, false if error
     * @author Carlos Gaviria and Cristian Arteaga
     */
    public boolean saveActualDataNetsim(String[][] data, String path) {
        boolean output = InputPersistence.saveInput(data, path);
        if (output) {
            ActualDataNetsim ad = new ActualDataNetsim(data);
            persistence.setActual_Data_Netsim(ad);
            return true;
        }
        return false;
    }

    /**
     * returns the data(actual data netsim) contained from a file indicated by
     * path
     *
     * @param path
     * @return boolean, false if error
     * @author Carlos Gaviria and Cristian Arteaga
     */
    public boolean loadActualDataNetsim(String path) {
        String data[][] = InputPersistence.loadInput(path);
        if (data != null) {
            ActualDataNetsim ad = new ActualDataNetsim(data);
            persistence.setActual_Data_Netsim(ad);
            return true;
        }
        return false;
    }

    /**
     * Creates a new Actual Data Fresim
     *
     * @param ADF_Data
     * @return boolean, false if error
     * @author Carlos Gaviria and Cristian Arteaga
     */
    public boolean newActualDataFresim(String[][] ADF_Data) {
        try {
            ActualDataFresim na = new ActualDataFresim(ADF_Data);
            persistence.setActual_Data_Fresim(na);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    /**
     * saves a set of parameters(actual data fresim) stored into data parameter
     * indicated in path
     *
     * @param data
     * @param path
     * @return boolean, false if error
     * @author Carlos Gaviria and Cristian Arteaga
     */
    public boolean saveActualDataFresim(String[][] data, String path) {
        boolean output = InputPersistence.saveInput(data, path);
        if (output) {
            ActualDataFresim ad = new ActualDataFresim(data);
            persistence.setActual_Data_Fresim(ad);
            return true;
        }
        return false;
    }

    /**
     * loads the data(actual data fresim) contained from a file indicated by
     * path
     *
     * @param path
     * @return boolean, false if error
     * @author Carlos Gaviria and Cristian Arteaga
     */
    public boolean loadActualDataFresim(String path) {
        String data[][] = InputPersistence.loadInput(path);
        if (data != null) {
            ActualDataFresim ad = new ActualDataFresim(data);
            persistence.setActual_Data_Fresim(ad);
            return true;
        }
        return false;
    }

    /**
     * Run the Calibration Process considering that a calibration algorithm has
     * been previously selected
     *
     * @param algorithm
     * @author Carlos Gaviria and Cristian Arteaga
     */
    public void runCalibration(String algorithm) {
        this.initial = persistence.getDataMatrix();
        UTIL.Tools.printMatrixToFileInt(this.initial, UTIL.Log.getFileinitialendparameters(), "Initial", "RecordType,StartColumn,EndColumn,MinValue,MaxValue,Value,LineNumber,InitialNode,EndNode");
        generateGraph("Initial");
        if (algorithm.equals("GA")) {
            UTIL.Log.info("*******************************************");
            UTIL.Log.info("**** Starting genetic calibration... ******");
            UTIL.Log.info("*******************************************");
//            UTIL.Log.info("Start time " + Tools.actualTimestamp("dd-MMM-yyyy h:mm:ss"));
            MemeticAlgorithm memetic = new MemeticAlgorithm(persistence);
            memetic.runGA((int) Pop, (int) Gen, (int) CrossPer, (int) SelPer, (int) Step, (int) Fat, (int) MutPer);
        }

        if (algorithm.equals("GASA")) {
            UTIL.Log.info("*******************************************");
            UTIL.Log.info("**** Starting memetic calibration... ******");
            UTIL.Log.info("*******************************************");
            MemeticAlgorithm memetic = new MemeticAlgorithm(persistence);
            memetic.runGASA((int) Pop, (int) Gen, (int) CrossPer, (int) SelPer, (int) Step, (int) Fat, (int) MutPer, IniTemp, CoolRate);
        }
        if (algorithm.equals("GATS")) {
            UTIL.Log.info("*******************************************");
            UTIL.Log.info("**** Starting memetic calibration... ******");
            UTIL.Log.info("*******************************************");
            MemeticAlgorithm memetic = new MemeticAlgorithm(persistence);
            memetic.runGATS((int) Pop, (int) Gen, (int) CrossPer, (int) SelPer, (int) Step, (int) Fat, (int) MutPer, (int) Tenure, (int) NeighSize);
        }
        if (algorithm.equals("SPSA")) {
            UTIL.Log.info("*******************************************");
            UTIL.Log.info("****** Starting SPSA calibration... *******");
            UTIL.Log.info("*******************************************");
            SPSA spsa = new SPSA(persistence, 300, Coefficient_A, Coefficient_a, Coefficient_c, Coefficient_alpha, Coefficient_gamma);
            spsa.run();
        }

        UTIL.Log.info("*******************************************");
        UTIL.Log.info("********* Calibration finished ************");
        UTIL.Log.info("*******************************************");
        double[][] avg = this.doMultiRun();
        generateGraphGivenOutput("Average", avg);
        generateGraph("Calibrated");
        generateGEHDataGivenOutput(avg);
        this.calibrated = persistence.getDataMatrix();
        UTIL.Tools.printMatrixToFileInt(this.calibrated, UTIL.Log.getFileinitialendparameters(), "Calibrated", "RecordType,StartColumn,EndColumn,MinValue,MaxValue,Value,LineNumber,InitialNode,EndNode");
    }

    public void setPercentageVolume(String PercentageVolume) {
        persistence.setPercentageVolume(Integer.parseInt(PercentageVolume));
    }

    public void setPercentageSpeed(String PercentageSpeed) {
        persistence.setPercentageSpeed(Integer.parseInt(PercentageSpeed));
    }

    public void setPathTrf(String PathTrf) {
        persistence.setPathTrf(PathTrf);
    }

    public double[][] getInitialVolumesGraph() {
        return Graphs.get("InitialVolume").getDataGraph();
    }

    public double[][] getInitialSpeedsGraph() {
        return Graphs.get("InitialSpeed").getDataGraph();
    }

    public double[][] getCalibratedVolumesGraph() {
        return Graphs.get("CalibratedVolume").getDataGraph();
    }

    public double[][] getCalibratedSpeedsGraph() {
        return Graphs.get("CalibratedSpeed").getDataGraph();
    }

    public double[][] getInitialGEHGraph() {
        return Graphs.get("InitialGEH").getDataGraph();
    }

    public double[][] getFinalGEHGraph() {
        return Graphs.get("CalibratedGEH").getDataGraph();
    }

    public double[][] getAVGVolumesGraph() {
        return Graphs.get("AverageVolume").getDataGraph();
    }

    public double[][] getAVGSpeedsGraph() {
        return Graphs.get("AverageSpeed").getDataGraph();
    }

    public double[][] getAVGGEHGraph() {
        return Graphs.get("AverageGEH").getDataGraph();
    }

    /**
     * Initialize all the parameters necessary for the spsa algorithm
     *
     * @param Coefficient_A
     * @param Coefficient_a
     * @param Coefficient_c
     * @param Coefficient_alpha
     * @param Coefficient_gamma
     */
    public void setSPSACoefficients(double Coefficient_A, double Coefficient_a, double Coefficient_c, double Coefficient_alpha, double Coefficient_gamma) {
        this.Coefficient_A = Coefficient_A;
        this.Coefficient_a = Coefficient_a;
        this.Coefficient_c = Coefficient_c;
        this.Coefficient_alpha = Coefficient_alpha;
        this.Coefficient_gamma = Coefficient_gamma;
    }

    /**
     * Initialize all the parameters necessary for the genetic algorithm
     *
     * @param Pop
     * @param Gen
     * @param CrossPer
     * @param SelPer
     * @param Step
     * @param Fat
     * @param MutPer
     */
    public void setGAParameters(double Pop, double Gen, double CrossPer, double SelPer, double Step, double Fat, double MutPer) {
        this.Pop = Pop;
        this.Gen = Gen;
        this.CrossPer = CrossPer;
        this.SelPer = SelPer;
        this.Step = Step;
        this.Fat = Fat;
        this.MutPer = MutPer;
    }

    /**
     * Initialize all the parameters necessary for the simulated annealing
     * algorithm
     *
     * @param IniTemp
     * @param CoolRate
     */
    public void setSAParameters(double IniTemp, double CoolRate) {
        this.IniTemp = IniTemp;
        this.CoolRate = CoolRate;
    }

    /**
     * Initialize all the parameters necessary for the tabu search algorithm
     *
     * @param Tenure
     * @param NeighSize
     */
    public void setTSParameters(double Tenure, double NeighSize) {
        this.Tenure = Tenure;
        this.NeighSize = NeighSize;
    }

    /**
     * Using the type parameter this method generates a graph and add it to the
     * container of graphics in this class
     *
     * @param type
     */
    public void generateGraph(String type) {//Type belongs ["Initial","End"]
        Graph volume = new Graph();
        Graph speed = new Graph();
        Graph geh = new Graph();
        UTIL.Tools.executeCorsim(persistence.getPathTrf());
        String path_out_model = UTIL.Tools.removeExtension(persistence.getPathTrf()) + ".out";

        int count_netsim = PERSISTENCE.ProcessPersistence.countDataNetsim(path_out_model);
        int count_fresim = PERSISTENCE.ProcessPersistence.countDataFresim(path_out_model);

        int count_real_netsim = (persistence.getActual_Data_Netsim().getADN_Data()).length;
        int count_real_fresim = (persistence.getActual_Data_Fresim().getADF_Data()).length;

        double[][] output_real = UTIL.Tools.addMatrix(UTIL.Tools.stringMatrixToDoubleMatrix(persistence.getActual_Data_Netsim().getADN_Data()), UTIL.Tools.stringMatrixToDoubleMatrix(persistence.getActual_Data_Fresim().getADF_Data()), count_real_netsim, count_real_fresim);
        double[][] output_model = UTIL.Tools.addMatrix(PERSISTENCE.ProcessPersistence.extractDataNetsim(path_out_model, count_netsim), PERSISTENCE.ProcessPersistence.extractDataFresim(path_out_model, count_fresim), count_real_netsim, count_real_fresim);
        volume.setVolSpeedGraphic(output_real, output_model, "Volume");
        speed.setVolSpeedGraphic(output_real, output_model, "Speed");
        geh.setGEHGraph(output_real, output_model);
        Graphs.put(type + "Volume", volume);
        Graphs.put(type + "Speed", speed);
        Graphs.put(type + "GEH", geh);

    }

    /**
     * Using the type and output model given this method generates a graph and
     * add it to the container of graphics in this class
     *
     * @param type
     * @param output_model
     */
    public void generateGraphGivenOutput(String type, double[][] output_model) {//Type belongs ["Initial","End"]
        Graph volume = new Graph();
        Graph speed = new Graph();
        Graph geh = new Graph();

        int count_real_netsim = (persistence.getActual_Data_Netsim().getADN_Data()).length;
        int count_real_fresim = (persistence.getActual_Data_Fresim().getADF_Data()).length;

        double[][] output_real = UTIL.Tools.addMatrix(UTIL.Tools.stringMatrixToDoubleMatrix(persistence.getActual_Data_Netsim().getADN_Data()), UTIL.Tools.stringMatrixToDoubleMatrix(persistence.getActual_Data_Fresim().getADF_Data()), count_real_netsim, count_real_fresim);

        volume.setVolSpeedGraphic(output_real, output_model, "Volume");
        speed.setVolSpeedGraphic(output_real, output_model, "Speed");
        geh.setGEHGraph(output_real, output_model);
        Graphs.put(type + "Volume", volume);
        Graphs.put(type + "Speed", speed);
        Graphs.put(type + "GEH", geh);

    }

    /**
     * This method generates GEH data for the model given as parameter
     *
     * @param output_model
     */
    public void generateGEHDataGivenOutput(double[][] output_model) {
        int count_real_netsim = (persistence.getActual_Data_Netsim().getADN_Data()).length;
        int count_real_fresim = (persistence.getActual_Data_Fresim().getADF_Data()).length;

        double[][] output_real = UTIL.Tools.addMatrix(UTIL.Tools.stringMatrixToDoubleMatrix(persistence.getActual_Data_Netsim().getADN_Data()), UTIL.Tools.stringMatrixToDoubleMatrix(persistence.getActual_Data_Fresim().getADF_Data()), count_real_netsim, count_real_fresim);
        double rms = UTIL.Tools.NRMS(output_real, output_model, persistence.getPercentageVolume());
        double[][] dataGEH = UTIL.Tools.setGEHGraphPrintConsole(output_real, output_model);
        double[][] dataVol = UTIL.Tools.setGraphics(output_real, output_model, "Volume");
        double[][] dataSpe = UTIL.Tools.setGraphics(output_real, output_model, "Speed");

        UTIL.Tools.printMatrixToFile(dataGEH, UTIL.Log.getFileoutgeh(), "Multiple runs averaged results");
        UTIL.Tools.printMatrixToFile(dataVol, UTIL.Log.getFileoutvolspeed(), "Volume - Multiple runs averaged results");
        UTIL.Tools.printMatrixToFile(dataSpe, UTIL.Log.getFileoutvolspeed(), "Speed - Multiple runs averaged results");

    }

    /**
     * Starts a corsim simulation process using different seeds, at the end
     * obtains all the output for each seed and make an average to be compared
     * with the real data
     *
     * @return
     */
    public double[][] doMultiRun() {
        int n = simulatorRuns;
        int size = 0;
        String separator = File.separator;
        String pathtrf = persistence.getPathTrf();
        String folder = UTIL.Log.getFolder() + separator + "MultiRun";
        folder = UTIL.Tools.createFolder(folder);
        String newPath = folder + separator + "CalibratedModel.trf";
        File trf = new File(newPath);
        UTIL.Tools.copyFile(pathtrf, newPath);
        UTIL.Tools.executeCorsimMultiple(trf.getAbsolutePath(), folder, n);
        List<double[][]> multiout = new ArrayList<double[][]>();
        for (int i = 0; i < n; i++) {
            String path_out_model = folder + separator + "CalibratedModel_" + (i + 1) + ".out";
            int count_netsim = PERSISTENCE.ProcessPersistence.countDataNetsim(path_out_model);
            int count_fresim = PERSISTENCE.ProcessPersistence.countDataFresim(path_out_model);
            double[][] output_model = UTIL.Tools.addMatrix(PERSISTENCE.ProcessPersistence.extractDataNetsim(path_out_model, count_netsim), PERSISTENCE.ProcessPersistence.extractDataFresim(path_out_model, count_fresim), count_netsim, count_fresim);
            size = output_model.length;
            multiout.add(output_model);
        }
        int i = 0;
        double[][] avg = new double[size][5];
        for (double[][] outmodel : multiout) {
            for (int j = 0; j < outmodel.length; j++) {
                avg[j][0] = outmodel[j][0];
                avg[j][1] = outmodel[j][1];
                avg[j][2] = avg[j][2] + outmodel[j][2];
                avg[j][3] = avg[j][3] + outmodel[j][3];
                avg[j][4] = outmodel[j][4];
            }
        }

        for (int j = 0; j < avg.length; j++) {
            avg[j][2] = (double) avg[j][2] / (double) n;
            avg[j][3] = (double) avg[j][3] / (double) n;
        }

        int count_real_netsim = (persistence.getActual_Data_Netsim().getADN_Data()).length;
        int count_real_fresim = (persistence.getActual_Data_Fresim().getADF_Data()).length;

        double[][] output_real = UTIL.Tools.addMatrix(UTIL.Tools.stringMatrixToDoubleMatrix(persistence.getActual_Data_Netsim().getADN_Data()), UTIL.Tools.stringMatrixToDoubleMatrix(persistence.getActual_Data_Fresim().getADF_Data()), count_real_netsim, count_real_fresim);

        double rms = UTIL.Tools.NRMS(output_real, avg, persistence.getPercentageVolume());
        UTIL.Log.info("Multirun RMS=" + rms);
        return avg;

    }

    public int getSimulatorRuns() {
        return simulatorRuns;
    }

    public void setSimulatorRuns(int simulatorRuns) {
        this.simulatorRuns = simulatorRuns;
    }

    public int[][] getInitialFilter() {
        return persistence.getInitialFilter();
    }

    public void setFilter(int[][] filter) {
        persistence.setFilter(filter);
    }

    public void setImprovedFilter(int[][] filter) {
        persistence.setImprovedFilter(filter);
    }

    public int[][] getImprovedFilter() {
        return persistence.getImprovedFilterMatrix();
    }

    public String[] getImprovedFilterRows() {
        return persistence.getImprovedFilterRows();
    }

    public String[] getImprovedFilterColumns() {
        String[] columns = persistence.getImprovedFilterColumns();
        for (int i = 0; i < columns.length; i++) {
            columns[i] = "RT " + columns[i];
        }
        return columns;
    }

    public void initializeCorsimModel() {
        persistence.initializeCorsimModel();
    }

    public CorsimModel getCorsimmodel() {
        return persistence.getCorsimmodel();
    }

    public void setCorsimModel(CorsimModel model) {
        persistence.setCorsimmodel(model);
    }

    public int[][] getDataTheta() {
        return persistence.getDataMatrix();
    }

    public int[][] getDataThetaNoFilter() {
        return persistence.getDataMatrixNoFilter();
    }

    public void tempTestImprovedFilter() {
//        int[][] initialm = persistence.getDataMatrixNoFilter();
        int[][] filtered = persistence.getDataMatrix();
//        UTIL.Tools.printMatrixToConsoleInteger(initialm, "----Initial---");
        UTIL.Tools.printMatrixToConsoleInteger(filtered, "----Filtered---");

    }

    /**
     * Considering the quantity of values to calibrate this method try auto
     * assign proper parameter values for the algorithms
     */
    public void autocalculateGAParameters() {
        long size = persistence.getDataMatrix().length;
        if (size <= 100) {//Small model
            Pop = 4;
            Gen = 20;
            Fat = 2;
            SelPer = 60;
            CrossPer = 75;
            MutPer = 7;
            Step = 2;
        }
        if (size > 100 && size <= 800) {//Medium model
            Pop = 4;
            Gen = 32;
            Fat = 4;
            SelPer = 60;
            CrossPer = 75;
            MutPer = 7;
            Step = 2;
        }
        if (size > 800) {//Big model
            Pop = 4;
            Gen = 16;
            Fat = 2;
            SelPer = 60;
            CrossPer = 80;
            MutPer = 7;
            Step = 2;
        }

    }

    public double getPop() {
        return Pop;
    }

    public double getGen() {
        return Gen;
    }

    public double getCrossPer() {
        return CrossPer;
    }

    public double getSelPer() {
        return SelPer;
    }

    public double getStep() {
        return Step;
    }

    public double getFat() {
        return Fat;
    }

    public double getMutPer() {
        return MutPer;
    }
}