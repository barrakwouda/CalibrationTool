package CLI;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;


/**
 *
 * @author Mathias
 */
public class CoeffReader {
    
    /**
     * Give the list of the lines of the text file located on file_path
     * @param file_path
     * @return
     * @throws FileNotFoundException 
     */
    public static List<String> readTextFile(String file_path) throws FileNotFoundException{
        Scanner s = new Scanner(new File(file_path));
        ArrayList<String> list = new ArrayList<String>();
        while (s.hasNext()){
            list.add(s.next());
        }
        s.close();
        return list;
    }
    
    /**
     * Get the value of the element with the name given in parameter
     * For example, if in the list there is "length=80", getFrom(list,"length") will
     * return the value : 80.0 .
     * @param list
     * @param name
     * @return 
     */
     public static double getFrom(List<String> list,String name){
        double variable=-1;
        
        for (String element : list){
            String id=element.split("=")[0];
            if (name.equals(id)){
                System.out.println("Coeff found : "+id);
                variable=Double.valueOf(element.split("=")[1]);
            }
        }
        return variable;
    }
}
