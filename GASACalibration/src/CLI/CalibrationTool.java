package CLI;

import DOMAIN.Model.CorsimModel;
import FACADE.Facade;
import GUI.GuiTools;
import PERSISTENCE.InputPersistence;
import static UTIL.Tools.initializeEntriesDesc;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import java.io.IOException;
import GUI.Graphics;
import GUI.LineGraphics;
import java.io.File;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author Mathias
 */
public class CalibrationTool {

    private Facade facade;
    private String path_trf;
    private String path_cps;
    private String path_adn;
    private String path_adf;
    private String path_links;
    private String Speedweight;
    private String Volweight;
    private String actual_data_netsim_path;
    private String actual_data_fresim_path;
    private String[][] actual_data_netsim = null;
    private String[][] actual_data_fresim = null;
    private String[][] Stringdata;
    private String[][] Stringdata1;
    private String[][] parameters_set;
    private List<String> coeff_list;
    private boolean check_path_trf = false;
    private boolean check_vol_weight = false;
    private boolean check_speed_weight = false;
    private boolean check_parameters_set = false;
    private boolean check_links_selected = false;
    private boolean check_7_arguments=false;
    

    /*Algorithm Settings*/
    private String algorithm;
    //SPSA
    private double A;
    private double Alpha;
    private double Gamma;
    private double a;
    private double c;
    //GA
    private double Pop;
    private double Gen;
    private double CrossPer;
    private double SelPer;
    private double Step;
    private double Fat;
    private double MutPer;
    //SA
    private double IniTemp;
    private double CoolRate;
    //TS
    private double Tenure;
    private double NeighSize;
    //Simulator
    private int simulatorRuns;
    //LOG
    private String filelog;

    DefaultTableModel DTM_param = new DefaultTableModel();
    private javax.swing.JTable jTable1;

    public CalibrationTool(String args[]) {
        UTIL.Tools.executeCommand("clean.bat");
        runCalibration(args);
    }
    
    /**
     * Launches the Calibration with the arguments given in the command line 
     * (absolute paths)
     * 1st argument : .trf file
     * 2nd argument : .cps file (parameters)
     * 3rd argument : .sel file (selected links)
     * 4th argument : .adn file (actual netsim data)
     * 5th (or 4th) argument : .adf file (actual fresim data)
     * 6th (or 5th) argument : Calibration algorithm (String variable)
     * 7th (or 6th) argument : Coefficients file
     * @param args 
     */
    public void runCalibration(String[] args){
        int args_length=args.length;
        if (6 <= args_length && args_length <= 7) {
            Facade facade = new Facade();
            jTable1 = new javax.swing.JTable();

            Volweight = "70";
            check_vol_weight = true;
            Speedweight = "30";
            check_speed_weight = true;

            // TRF Path
            setPath_trf(args[0]);
            System.out.println("TRF Path : " + getPath_trf());
            if (!"".equals(path_trf)) {
                check_path_trf = true;
            }

            if (check_path_trf) {
                facade.setPathTrf(path_trf);
                UTIL.Log.setPathtrf(path_trf);
            }

            // CPS_Path (Parameters)
            setPath_cps(args[1]);
            System.out.println("CPS Path : " + getPath_cps());
            if (!"".equals(path_cps)) {
                initializeEntriesDesc();
                String[][] data = InputPersistence.loadInput(getPath_cps());
                String[] DataNameCols = new String[7];
                DataNameCols[0] = "Record type";
                DataNameCols[1] = "Start column";
                DataNameCols[2] = "End column";
                DataNameCols[3] = "Min value";
                DataNameCols[4] = "Max value";
                DataNameCols[5] = "RT Description";
                DataNameCols[6] = "Entry Description";
                DTM_param = new DefaultTableModel(data, DataNameCols);
                for (int i = 0; i < data.length; i++) {
                    String[] RT_desc = UTIL.Tools.getEntryDescription(data[i][0], 78, 80);
                    String[] ENT_desc = UTIL.Tools.getEntryDescription(data[i][0], Long.valueOf(data[i][1]), Long.valueOf(data[i][2]));
                    DTM_param.setValueAt(RT_desc[4], i, 5);
                    DTM_param.setValueAt(ENT_desc[4], i, 6);
                }
                jTable1.setModel(DTM_param);

                String[] row = GuiTools.validIntegerMatrix(jTable1);
                if (row == null) {
                    //volumenw,speedw,string[] datatable
                    List param = new ArrayList();
                    param.add(Volweight);
                    param.add(Speedweight);
                    param.add(GuiTools.jtableToMatrix(jTable1));
                    parameters_set = (String[][]) param.get(2);
                    facade.newParametersSet(parameters_set);
                    check_parameters_set = true;
                } else {
                    System.out.println("this row: " + Arrays.toString(row) + ""
                            + "\n - Contains some non integer character or negative value \n - "
                            + "Start Column bigger than End Column, Min value bigger than Max value \n - "
                            + "Record type must be between 0 and 210 \n - "
                            + "Error, adding volumes with speed weight must equal 100, Cannot continue with calibration");
                }
            }
            // Links Selection

            facade.initializeCorsimModel();

            setPath_links(args[2]);
            System.out.println("Path_link_selection : " + getPath_links());
            if (!"".equals(path_links)) {
                CorsimModel modelLoaded = PERSISTENCE.InputPersistence.loadModelInput(getPath_links());
                facade.setCorsimModel(modelLoaded);
                facade.autocalculateGAParameters();
                facade.tempTestImprovedFilter();//Print theta and theta filtered values
                check_links_selected = true;
            }

            // Actual data
            if (check_path_trf && check_vol_weight && check_speed_weight && check_parameters_set && check_links_selected) {
                List list_actual_data = new ArrayList();
                if (args_length == 6) {
                    System.out.println("Actual data file path : " + args[3]);
                    if (args[3].substring(args[3].length() - 3).equals("adn")) {
                        setActual_data_netsim_path(args[3]);

                        Stringdata = loadStringData(args[3]);

                        list_actual_data.add(Stringdata);
                        actual_data_netsim = (String[][]) list_actual_data.get(0);
                        actual_data_fresim = new String[0][0];

                    } else if (args[3].substring(args[3].length() - 3).equals("adf")) {
                        setActual_data_fresim_path(args[4]);

                        Stringdata1 = loadStringData(args[3]);

                        list_actual_data.add(Stringdata1);
                        actual_data_netsim = new String[0][0];
                        actual_data_fresim = (String[][]) list_actual_data.get(0);
                    }else{
                        System.out.println("Unable to load the actual_data file");
                    }
                } else { //7 Arguments : .adn and .adf files
                    check_7_arguments=true;
                    System.out.println("Actual data file path (adn) : " + args[3]);
                    System.out.println("Actual data file path (adf) : " + args[4]);
                    //Netsim actual data
                    setActual_data_netsim_path(args[3]);

                    Stringdata = loadStringData(args[3]);

                    //Fresim actual data
                    setActual_data_fresim_path(args[4]);

                    Stringdata1 = loadStringData(args[4]);

                    list_actual_data.add(Stringdata);
                    list_actual_data.add(Stringdata1);

                    actual_data_netsim = (String[][]) list_actual_data.get(0);
                    actual_data_fresim = (String[][]) list_actual_data.get(1);
                }
            }

            //Simulation 
            if (check_7_arguments){
                algorithm=args[5];
            }else {
                algorithm = args[4];
            }

            try {
                if (check_7_arguments){
                    coeff_list = CoeffReader.readTextFile(args[6]);
                }else {
                    coeff_list = CoeffReader.readTextFile(args[5]);
                }
            } catch (IOException e) {
                System.out.println(e);
            }

            switch (algorithm) {
                case "GA":
                    setPop(CoeffReader.getFrom(coeff_list, "Pop"));
                    setGen(CoeffReader.getFrom(coeff_list, "Gen"));
                    setCrossPer(CoeffReader.getFrom(coeff_list, "CrossPer"));
                    setSelPer(CoeffReader.getFrom(coeff_list, "SelPer"));
                    setStep(CoeffReader.getFrom(coeff_list, "Step"));
                    setFat(CoeffReader.getFrom(coeff_list, "Fat"));
                    setMutPer(CoeffReader.getFrom(coeff_list, "MutPer"));
                    break;
                case "SPSA":
                    setA(CoeffReader.getFrom(coeff_list, "A"));
                    setAlpha(CoeffReader.getFrom(coeff_list, "Alpha"));
                    setGamma(CoeffReader.getFrom(coeff_list, "Gamma"));
                    seta(CoeffReader.getFrom(coeff_list, "a"));
                    setC(CoeffReader.getFrom(coeff_list, "C"));
                    break;
                case "GASA":
                    setPop(CoeffReader.getFrom(coeff_list, "Pop"));
                    setGen(CoeffReader.getFrom(coeff_list, "Gen"));
                    setCrossPer(CoeffReader.getFrom(coeff_list, "CrossPer"));
                    setSelPer(CoeffReader.getFrom(coeff_list, "SelPer"));
                    setStep(CoeffReader.getFrom(coeff_list, "Step"));
                    setFat(CoeffReader.getFrom(coeff_list, "Fat"));
                    setMutPer(CoeffReader.getFrom(coeff_list, "MutPer"));
                    setIniTemp(CoeffReader.getFrom(coeff_list, "IniTemp"));
                    setCoolRate(CoeffReader.getFrom(coeff_list, "CoolRate"));
                    break;
                case "GATS":
                    setPop(CoeffReader.getFrom(coeff_list, "Pop"));
                    setGen(CoeffReader.getFrom(coeff_list, "Gen"));
                    setCrossPer(CoeffReader.getFrom(coeff_list, "CrossPer"));
                    setSelPer(CoeffReader.getFrom(coeff_list, "SelPer"));
                    setStep(CoeffReader.getFrom(coeff_list, "Step"));
                    setFat(CoeffReader.getFrom(coeff_list, "Fat"));
                    setMutPer(CoeffReader.getFrom(coeff_list, "MutPer"));
                    setTenure(CoeffReader.getFrom(coeff_list, "Tenure"));
                    setNeighSize(CoeffReader.getFrom(coeff_list, "NeighSize"));
                    break;
                default:
                    System.out.println("Invalid algorithm argument");
                    break;
            }
            setSimulatorRuns((int) CoeffReader.getFrom(coeff_list, "simulatorRuns"));

            facade.setSPSACoefficients(A, a, c, Alpha, Gamma);
            facade.setGAParameters(Pop, Gen, CrossPer, SelPer, Step, Fat, MutPer);
            facade.setSAParameters(IniTemp, CoolRate);
            facade.setTSParameters(Tenure, NeighSize);
            facade.setSimulatorRuns(simulatorRuns);

            facade.setPercentageVolume(Volweight);
            facade.setPercentageSpeed(Speedweight);
            facade.newActualDataNetsim(actual_data_netsim);
            facade.newActualDataFresim(actual_data_fresim);

            Process proc = null;
            this.filelog = UTIL.Tools.actualTimestamp("yyyyMMdd_hhmmss");
            String LogPath = UTIL.Log.setLogSettings(filelog + "_" + UTIL.Log.getNametrf() + "_" + this.algorithm);
            UTIL.Tools.createFolder(UTIL.Log.getFolder() + File.separator + "Graphs");
            try {
                proc = Runtime.getRuntime().exec("cmd /C java -jar CShell.jar \"" + LogPath + "\"");
                System.out.println("java -jar CShell.jar \"" + LogPath + "\"");
            } catch (IOException ex) {
                Logger.getLogger(GUI.CalibrationTool.class.getName()).log(Level.SEVERE, null, ex);
            }

            facade.runCalibration(this.algorithm);

            System.out.println("Calibration done\n");
            proc.destroy();
            UTIL.Tools.executeCommand("clean.bat");

            
            //Graphics
            int maxGraph2 = GuiTools.maxRangeGraph(facade.getInitialSpeedsGraph());
            Graphics demo2 = new Graphics("Speeds before calibration graph", "Speeds before calibration", "Actual speeds", "Model speeds", maxGraph2, facade.getInitialSpeedsGraph());
            demo2.pack();
            //demo2.setLocationRelativeTo(null);
            demo2.setVisible(true);

            int maxGraph4 = GuiTools.maxRangeGraph(facade.getCalibratedSpeedsGraph());
            Graphics demo4 = new Graphics("Speeds after calibration graph", "Speeds after calibration", "Actual speeds", "Model speeds", maxGraph4, facade.getCalibratedSpeedsGraph());
            demo4.pack();
            //demo4.setLocationRelativeTo(null);
            demo4.setVisible(true);

            //Average multiple runs
            int maxGraph6 = GuiTools.maxRangeGraph(facade.getCalibratedSpeedsGraph());
            Graphics demo6 = new Graphics("Speeds after calibration graph", "Speeds multiple runs averaged results", "Actual speeds", "Model speeds", maxGraph6, facade.getAVGSpeedsGraph());
            demo6.pack();
            demo6.setVisible(true);

            int maxGraph1 = GuiTools.maxRangeGraph(facade.getInitialVolumesGraph());
            Graphics demo1 = new Graphics("Volumes before calibration", "Volumes before calibration", "Actual counts", "Model counts", maxGraph1, facade.getInitialVolumesGraph());
            demo1.pack();
            //demo1.setLocationRelativeTo(null);
            demo1.setVisible(true);

            int maxGraph3 = GuiTools.maxRangeGraph(facade.getCalibratedVolumesGraph());
            Graphics demo3 = new Graphics("Volumes after calibration graph", "Volumes after calibration", "Actual counts", "Model counts", maxGraph3, facade.getCalibratedVolumesGraph());
            demo3.pack();
            //demo3.setLocationRelativeTo(null);
            demo3.setVisible(true);

            int maxGraph8 = GuiTools.maxRangeGraph(facade.getCalibratedVolumesGraph());
            Graphics demo8 = new Graphics("Volumes after calibration graph", "Volumes multiple runs averaged results", "Actual counts", "Model counts", maxGraph8, facade.getAVGVolumesGraph());
            demo8.pack();
            demo8.setVisible(true);

            double maxX;
            double maxY;

            double maxX1 = GuiTools.getMax(facade.getInitialGEHGraph(), "x");
            double maxX2 = GuiTools.getMax(facade.getFinalGEHGraph(), "x");
            if (maxX1 > maxX2) {
                maxX = maxX1;
            } else {
                maxX = maxX2;
            }
            maxX = (int) maxX + 1;

            double maxY1 = GuiTools.getMax(facade.getInitialGEHGraph(), "y");
            double maxY2 = GuiTools.getMax(facade.getFinalGEHGraph(), "y");
            if (maxY1 > maxY2) {
                maxY = maxY1;
            } else {
                maxY = maxY2;
            }
            maxY = (int) maxY + 1;

            LineGraphics lG = new LineGraphics("GEH Graph", "GEH Graph",
                    "Link                Red: Before Calibration- Blue: After Calibration", "GEH", (int) maxX, (int) maxY,
                    facade.getInitialGEHGraph(), facade.getFinalGEHGraph());
            LineGraphics lGAVG = new LineGraphics("GEH Graph", "GEH Graph multiple runs averaged results",
                    "Link                Red: Before Calibration- Blue: After Calibration", "GEH", (int) maxX, (int) maxY,
                    facade.getInitialGEHGraph(), facade.getAVGGEHGraph());
            lG.pack();
            lG.setVisible(true);
            //Average multiple runs
            lGAVG.pack();
            lGAVG.setVisible(true);

        } else {
            System.out.println("Error : this program needs between 6 and 7 arguments (1 or 2 actualData_file)");
        }
    }
    public static void main(String args[]) {
        if (args.length != 0) {
            System.out.println(args.length + " arguments given.");
            CalibrationTool tool = new CalibrationTool(args);
        } else {
            System.out.println("No command line arguments");
        }
    }

    /**
     * Load the string data from an ActualData file (located in the actualData_path)
     * @param actualData_path
     * @return 
     */
    private String[][] loadStringData(String actualData_path) {
        String[][] loaded_data = PERSISTENCE.InputPersistence.loadInput(actualData_path);
        double[][] data_loaded = GuiTools.stringBidArrToDoubleArray(loaded_data);

        int count_time_periods = GUI.GuiTools.countTimePeriods(data_loaded);
        ArrayList<double[][]> BidArrays = GuiTools.returnBidArrays(data_loaded);

        ArrayList<JTable> jTableList = new ArrayList();
        ArrayList<DefaultTableModel> DTM_netsim = new ArrayList();

        String[] nameCols = {"node 1", "node 2", "volume(vph)", "speeds(mph)"};

        for (int i = 0; i < count_time_periods; i++) {
            DTM_netsim.add(new DefaultTableModel(GuiTools.doubleArrToStringArr(BidArrays.get(i)), nameCols));
            jTableList.add(new JTable(DTM_netsim.get(i)));
        }

        double[][] data = GuiTools.jTableToDoubleBidArr(jTableList);

        return GuiTools.doubleArrToStringArr(data);
    }

    /**
     * @return the facade
     */
    public Facade getFacade() {
        return facade;
    }

    /**
     * @return the path_trf
     */
    public String getPath_trf() {
        return path_trf;
    }

    /**
     * @return the check_path_trf
     */
    public boolean isCheck_path_trf() {
        return check_path_trf;
    }

    /**
     * @return the check_vol_weight
     */
    public boolean isCheck_vol_weight() {
        return check_vol_weight;
    }

    /**
     * @return the check_speed_weight
     */
    public boolean isCheck_speed_weight() {
        return check_speed_weight;
    }

    /**
     * @return the check_parameters_set
     */
    public boolean isCheck_parameters_set() {
        return check_parameters_set;
    }

    /**
     * @return the check_links_selected
     */
    public boolean isCheck_links_selected() {
        return check_links_selected;
    }

    /**
     * @return the path_cps
     */
    public String getPath_cps() {
        return path_cps;
    }

    /**
     * @return the path_adn
     */
    public String getPath_adn() {
        return path_adn;
    }

    /**
     * @return the path_adf
     */
    public String getPath_adf() {
        return path_adf;
    }

    /**
     * @return the path_links
     */
    public String getPath_links() {
        return path_links;
    }

    /**
     * @return the Speedweight
     */
    public String getSpeedweight() {
        return Speedweight;
    }

    /**
     * @return the Volweight
     */
    public String getVolweight() {
        return Volweight;
    }

    /**
     * @return the actual_data_netsim_path
     */
    public String getActual_data_netsim_path() {
        return actual_data_netsim_path;
    }

    /**
     * @return the actual_data_fresim_path
     */
    public String getActual_data_fresim_path() {
        return actual_data_fresim_path;
    }

    /**
     * @return the actual_data_netsim
     */
    public String[][] getActual_data_netsim() {
        return actual_data_netsim;
    }

    /**
     * @return the actual_data_fresim
     */
    public String[][] getActual_data_fresim() {
        return actual_data_fresim;
    }

    /**
     * @return the Stringdata
     */
    public String[][] getStringdata() {
        return Stringdata;
    }

    /**
     * @return the Stringdata1
     */
    public String[][] getStringdata1() {
        return Stringdata1;
    }

    /**
     * @return the parameters_set
     */
    public String[][] getParameters_set() {
        return parameters_set;
    }

    /**
     * @return the Coeff_list
     */
    public List getCoeff_list() {
        return coeff_list;
    }

    /**
     * @return the algorithm
     */
    public String getAlgorithm() {
        return algorithm;
    }

    /**
     * @return the A
     */
    public double getA() {
        return A;
    }

    /**
     * @return the Alpha
     */
    public double getAlpha() {
        return Alpha;
    }

    /**
     * @return the Gamma
     */
    public double getGamma() {
        return Gamma;
    }

    /**
     * @return the a
     */
    public double geta() {
        return a;
    }

    /**
     * @return the c
     */
    public double getC() {
        return c;
    }

    /**
     * @return the Pop
     */
    public double getPop() {
        return Pop;
    }

    /**
     * @return the Gen
     */
    public double getGen() {
        return Gen;
    }

    /**
     * @return the CrossPer
     */
    public double getCrossPer() {
        return CrossPer;
    }

    /**
     * @return the SelPer
     */
    public double getSelPer() {
        return SelPer;
    }

    /**
     * @return the Step
     */
    public double getStep() {
        return Step;
    }

    /**
     * @return the Fat
     */
    public double getFat() {
        return Fat;
    }

    /**
     * @return the MutPer
     */
    public double getMutPer() {
        return MutPer;
    }

    /**
     * @return the IniTemp
     */
    public double getIniTemp() {
        return IniTemp;
    }

    /**
     * @return the CoolRate
     */
    public double getCoolRate() {
        return CoolRate;
    }

    /**
     * @return the Tenure
     */
    public double getTenure() {
        return Tenure;
    }

    /**
     * @return the NeighSize
     */
    public double getNeighSize() {
        return NeighSize;
    }

    /**
     * @return the simulatorRuns
     */
    public int getSimulatorRuns() {
        return simulatorRuns;
    }

    /**
     * @return the filelog
     */
    public String getFilelog() {
        return filelog;
    }

    /**
     * @param path_trf the path_trf to set
     */
    public void setPath_trf(String path_trf) {
        this.path_trf = path_trf;
    }

    /**
     * @param check_path_trf the check_path_trf to set
     */
    public void setCheck_path_trf(boolean check_path_trf) {
        this.check_path_trf = check_path_trf;
    }

    /**
     * @param check_vol_weight the check_vol_weight to set
     */
    public void setCheck_vol_weight(boolean check_vol_weight) {
        this.check_vol_weight = check_vol_weight;
    }

    /**
     * @param check_speed_weight the check_speed_weight to set
     */
    public void setCheck_speed_weight(boolean check_speed_weight) {
        this.check_speed_weight = check_speed_weight;
    }

    /**
     * @param check_parameters_set the check_parameters_set to set
     */
    public void setCheck_parameters_set(boolean check_parameters_set) {
        this.check_parameters_set = check_parameters_set;
    }

    /**
     * @param check_links_selected the check_links_selected to set
     */
    public void setCheck_links_selected(boolean check_links_selected) {
        this.check_links_selected = check_links_selected;
    }

    /**
     * @param path_cps the path_cps to set
     */
    public void setPath_cps(String path_cps) {
        this.path_cps = path_cps;
    }

    /**
     * @param path_adn the path_adn to set
     */
    public void setPath_adn(String path_adn) {
        this.path_adn = path_adn;
    }

    /**
     * @param path_adf the path_adf to set
     */
    public void setPath_adf(String path_adf) {
        this.path_adf = path_adf;
    }

    /**
     * @param path_links the path_links to set
     */
    public void setPath_links(String path_links) {
        this.path_links = path_links;
    }

    /**
     * @param Speedweight the Speedweight to set
     */
    public void setSpeedweight(String Speedweight) {
        this.Speedweight = Speedweight;
    }

    /**
     * @param Volweight the Volweight to set
     */
    public void setVolweight(String Volweight) {
        this.Volweight = Volweight;
    }

    /**
     * @param actual_data_netsim_path the actual_data_netsim_path to set
     */
    public void setActual_data_netsim_path(String actual_data_netsim_path) {
        this.actual_data_netsim_path = actual_data_netsim_path;
    }

    /**
     * @param actual_data_fresim_path the actual_data_fresim_path to set
     */
    public void setActual_data_fresim_path(String actual_data_fresim_path) {
        this.actual_data_fresim_path = actual_data_fresim_path;
    }

    /**
     * @param actual_data_netsim the actual_data_netsim to set
     */
    public void setActual_data_netsim(String[][] actual_data_netsim) {
        this.actual_data_netsim = actual_data_netsim;
    }

    /**
     * @param actual_data_fresim the actual_data_fresim to set
     */
    public void setActual_data_fresim(String[][] actual_data_fresim) {
        this.actual_data_fresim = actual_data_fresim;
    }

    /**
     * @param parameters_set the parameters_set to set
     */
    public void setParameters_set(String[][] parameters_set) {
        this.parameters_set = parameters_set;
    }

    /**
     * @param Coeff_list the Coeff_list to set
     */
    public void setCoeff_list(List Coeff_list) {
        this.coeff_list = Coeff_list;
    }

    /**
     * @param algorithm the algorithm to set
     */
    public void setAlgorithm(String algorithm) {
        this.algorithm = algorithm;
    }

    /**
     * @param A the A to set
     */
    public void setA(double A) {
        this.A = A;
    }

    /**
     * @param Alpha the Alpha to set
     */
    public void setAlpha(double Alpha) {
        this.Alpha = Alpha;
    }

    /**
     * @param Gamma the Gamma to set
     */
    public void setGamma(double Gamma) {
        this.Gamma = Gamma;
    }

    /**
     * @param a the a to set
     */
    public void seta(double a) {
        this.a = a;
    }

    /**
     * @param c the c to set
     */
    public void setC(double c) {
        this.c = c;
    }

    /**
     * @param Pop the Pop to set
     */
    public void setPop(double Pop) {
        this.Pop = Pop;
    }

    /**
     * @param Gen the Gen to set
     */
    public void setGen(double Gen) {
        this.Gen = Gen;
    }

    /**
     * @param CrossPer the CrossPer to set
     */
    public void setCrossPer(double CrossPer) {
        this.CrossPer = CrossPer;
    }

    /**
     * @param SelPer the SelPer to set
     */
    public void setSelPer(double SelPer) {
        this.SelPer = SelPer;
    }

    /**
     * @param Step the Step to set
     */
    public void setStep(double Step) {
        this.Step = Step;
    }

    /**
     * @param Fat the Fat to set
     */
    public void setFat(double Fat) {
        this.Fat = Fat;
    }

    /**
     * @param MutPer the MutPer to set
     */
    public void setMutPer(double MutPer) {
        this.MutPer = MutPer;
    }

    /**
     * @param IniTemp the IniTemp to set
     */
    public void setIniTemp(double IniTemp) {
        this.IniTemp = IniTemp;
    }

    /**
     * @param CoolRate the CoolRate to set
     */
    public void setCoolRate(double CoolRate) {
        this.CoolRate = CoolRate;
    }

    /**
     * @param Tenure the Tenure to set
     */
    public void setTenure(double Tenure) {
        this.Tenure = Tenure;
    }

    /**
     * @param NeighSize the NeighSize to set
     */
    public void setNeighSize(double NeighSize) {
        this.NeighSize = NeighSize;
    }

    /**
     * @param simulatorRuns the simulatorRuns to set
     */
    public void setSimulatorRuns(int simulatorRuns) {
        this.simulatorRuns = simulatorRuns;
    }

}
