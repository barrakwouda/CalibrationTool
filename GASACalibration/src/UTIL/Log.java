/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package UTIL;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;

/**
 * Class to log all the processes of the system, it is not a good practice to
 * write directly into console, it is better to use this log class
 *
 * @author cristian
 */
public class Log {

    private static String filepath = "CalibrationToolLog" + ".txt";
    private static String filesufix = "";
    private static String fileoutvolspeed = "";
    private static String fileinitialendparameters = "";
    private static String fileoutgeh = "";
    private static String folder = "";
    private static String pathtrf = "";
    private static boolean tofile = true;
    private static boolean toconsole = true;
    private static boolean linetimestamp = true;
    //Tools.actualTimestamp("dd_MM_yy_h_mm_ss_S") +

    /**
     * Log an info event
     * @param msg
     */
    public static void info(String msg) {
        if (linetimestamp) {
            msg = Tools.actualTimestamp("[dd-MMM h:mm:ss] ") + msg;
        }
        if (tofile) {
            fileWritter(msg);
        }
        if (toconsole) {
            System.out.println(msg);
        }

    }

    /**
     * Log an error event
     * @param msg
     */
    public static void error(String msg) {
        msg = "ERROR: " + msg;
        if (linetimestamp) {
            msg = Tools.actualTimestamp("[dd-MMM h:mm:ss] ") + msg;
        }
        if (tofile) {
            fileWritter(msg);
        }
        if (toconsole) {
            System.out.println(msg);
        }

    }

    private static void fileWritter(String line) {
        try {
            File file = new File(getFilepath());

            FileWriter fileWriter = new FileWriter(file, true);
            BufferedWriter bufferFileWriter = new BufferedWriter(fileWriter);
            fileWriter.append(line);
            fileWriter.append("\n");
            bufferFileWriter.close();
        } catch (Exception ex) {
            System.out.println(ex);
        }
    }

    public static String getFilepath() {
        return filepath;
    }

    public static void setFilepath(String aFilepath) {
        filepath = aFilepath;
    }

    public static String getFilesufix() {
        return filesufix;
    }

    /**
     * Charge log settings
     * @param aFilesufix the filesufix to set
     * @return
     */
    public static String setLogSettings(String aFilesufix) {
        try {
            filesufix = aFilesufix;
            String separator = File.separator;
            setFolder("output_" + filesufix);
            File outdir = new File(getFolder());
            outdir.mkdir();
            filepath = getFolder() + separator + "Log.txt";
            fileoutvolspeed = getFolder() + separator + "VolumeSpeed.txt";
            fileoutgeh = getFolder() + separator + "GEH.txt";
            fileinitialendparameters = getFolder() + separator + "InitialEndParameters.csv";
            return filepath;
        } catch (SecurityException se) {
            UTIL.Log.error(se.getMessage());
            return "";
        }
    }

    public static String getFileoutvolspeed() {
        return fileoutvolspeed;
    }

    public static void setFileoutvolspeed(String aFileoutvolspeed) {
        fileoutvolspeed = aFileoutvolspeed;
    }

    public static String getFileoutgeh() {
        return fileoutgeh;
    }

    public static void setFileoutgeh(String aFileoutgeh) {
        fileoutgeh = aFileoutgeh;
    }

    public static String getFolder() {
        return folder;
    }

    public static void setFolder(String aFolder) {
        folder = aFolder;
    }

    public static String getPathtrf() {
        return pathtrf;
    }

    public static void setPathtrf(String aPathtrf) {
        pathtrf = aPathtrf;
    }

    public static String getNametrf() {
        File f = new File(pathtrf);
        String name = (f.getName());
        return name.substring(0, name.lastIndexOf('.'));

    }

    public static String getFileinitialendparameters() {
        return fileinitialendparameters;
    }

    public static void setFileinitialendparameters(String aFileinitialendparameters) {
        fileinitialendparameters = aFileinitialendparameters;
    }
}
