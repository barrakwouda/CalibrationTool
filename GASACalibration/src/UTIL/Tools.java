/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package UTIL;

import DOMAIN.Model.CorsimModel;
import DOMAIN.Model.Entry;
import DOMAIN.Model.Link;
import DOMAIN.Model.RecordType;
import java.io.BufferedInputStream;
import org.apache.commons.net.ntp.NTPUDPClient;
import org.apache.commons.net.ntp.TimeInfo;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.net.InetAddress;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.StringTokenizer;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Important class with utilities methods used through all the system
 * @author cristian
 */
public class Tools {

    private static String[][] entriesdesc;

    public static String[][] getEntriesdesc() {
        return entriesdesc;
    }
    
    /**
     * Read the file with the entries description and charges it to memory
     */
    public static void initializeEntriesDesc() {
        try {
            int lines = countLines("entries_list_RT_names.csv");
            entriesdesc = new String[lines][10];
            BufferedReader br = null;
            int i = 0;
            try {
                String sCurrentLine;
                br = new BufferedReader(new FileReader("entries_list_RT_names.csv"));

                while ((sCurrentLine = br.readLine()) != null) {
                    entriesdesc[i] = sCurrentLine.split(";");
                    
                    i++;
                }

            } catch (IOException e) {
                e.printStackTrace();
            } finally {
                try {
                    if (br != null) {
                        br.close();
                    }
                } catch (IOException ex) {
                    ex.printStackTrace();
                }
            }
        } catch (IOException ex) {
            Logger.getLogger(Tools.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Get the entry description with the parameters given
     * @param recordtype
     * @param startcolumn
     * @param endcolumn
     * @return
     */
    public static String[] getEntryDescription(String recordtype, long startcolumn, long endcolumn) {
        String ar[] = {"", "", "", "", "", "", "", "", "", "", "", "", ""};
        for (int i = 0; i < entriesdesc.length; i++) {
            if (entriesdesc[i][0].equals(recordtype) && entriesdesc[i][2].equals(String.valueOf(startcolumn)) && entriesdesc[i][3].equals(String.valueOf(endcolumn))) {
                return entriesdesc[i];
            }
        }
        return ar;
    }
    
    public static String[] getSpecificEntryDescription(String recordtype,int pos) {
        String ar[] = {"", "", "", "", "", "", "", "", "", "", "", "", ""};
        for (int i = 0; i < entriesdesc.length; i++) {
            if (entriesdesc[i][0].equals(recordtype) && entriesdesc[i][1].equals(String.valueOf(pos))) {
                return entriesdesc[i];
            }
        }
        return ar;
    }
    public static void setFlag(String recordtype,int pos, String flag){
        for(int i=0; i< entriesdesc.length;i++){
            if (entriesdesc[i][0].equals(recordtype) && entriesdesc[i][1].equals(String.valueOf(pos))) {
                entriesdesc[i][9] = flag;
            }
        }
    }
    public static String getFlag(String recordtype,int pos){

        String[] entry = getSpecificEntryDescription(recordtype, pos+1);
        if( entry[9].equals("false")){
            return "false";
        }else{
            return "true";
        }
    }
    public static int getRTnumber(){
        int number=0;
        for(int i=0; i<entriesdesc.length;i++){
            if(entriesdesc[i][2].equals(String.valueOf(78)) && entriesdesc[i][3].equals(String.valueOf(80))){
                number++;
            }
            
        }
        return number;
    }
    public static String[][] getRTdesc() {
        int number = getRTnumber();
        String ar[][]= new String[number][];
        int j=0;
        while(j<number){
            for(int i=0; i < entriesdesc.length ; i++){
                if (entriesdesc[i][2].equals(String.valueOf(78)) && entriesdesc[i][3].equals(String.valueOf(80))) {
                    ar[j]=entriesdesc[i]; 
                    j++;
                }
            }
        }
        return ar;     
    }
    public static String[][] getEntrydescOfSpecRT(String rt) {
        String[] RTdesc = getEntryDescription(rt, 78, 80);
        int max = stringToIntegerAux(RTdesc[1]);
        String ar[][]= new String[max][];
        int j=0;
        while(j<max){
            for(int i=0; i < entriesdesc.length ; i++){
                if (entriesdesc[i][0].equals(rt)&& entriesdesc[i][1].equals(78)==false && entriesdesc[i][1].equals(80)==false) {
                    ar[j]=entriesdesc[i]; 
                    j++;
                }
            }
        }
        return ar;     
    }


    private static int countLines(String filename) throws IOException {
        InputStream is = new BufferedInputStream(new FileInputStream(filename));
        try {
            byte[] c = new byte[1024];
            int count = 0;
            int readChars = 0;
            boolean empty = true;
            while ((readChars = is.read(c)) != -1) {
                empty = false;
                for (int i = 0; i < readChars; ++i) {
                    if (c[i] == '\n') {
                        ++count;
                    }
                }
            }
            return (count == 0 && !empty) ? 1 : count;
        } finally {
            is.close();
        }
    }

    /**
     * Extracts the system current date
     *
     * @param format
     * @return cadena con la fecha y hora actual
     */
    public static String actualTimestamp(String format) {
        Date date = new Date();
        SimpleDateFormat sdf = new SimpleDateFormat(format);
        String formattedDate = sdf.format(date);
        return formattedDate;
    }

    /**
     * Makes a copy of the given file in "pathin" into "pathout"
     *
     * @param sourceFile
     * @param destinationFile
     * @return true if successfull with the copy of file, false if error
     * @author Carlos Gaviria and Cristian Arteaga
     */
    public static boolean copyFile(String sourceFile, String destinationFile) {
        try {
            File inFile = new File(sourceFile);
            File outFile = new File(destinationFile);
            FileInputStream in = new FileInputStream(inFile);
            FileOutputStream out = new FileOutputStream(outFile);
            int c;
            while ((c = in.read()) != -1) {
                out.write(c);
            }

            in.close();
            out.close();
            return true;
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
    }

    /**
     * Delete the given file in "path"
     *
     * @param path
     * @return true if successfull with the delete of file, fasle if error
     * @author Carlos Gaviria and Cristian Arteaga
     */
    public static boolean deleteFile(String path) {
        try {
            String execute = "del " + "\"" + path + "\"";
            Process proc = Runtime.getRuntime().exec("cmd /C start/wait " + execute);
            proc.waitFor();
            proc.destroy();
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    /**
     * Execute commands from windows console
     *
     * @param command
     * @return true for successfull execution of CORSIM, output file generated,
     * false if error
     * @author Carlos Gaviria and Cristian Arteaga
     */
    public static boolean executeCommand(String command) {
        try {
//            Process proc = Runtime.getRuntime().exec("cmd /C start/wait "+execute);
            Process proc = Runtime.getRuntime().exec("cmd /C start/wait " + command);
            proc.waitFor();
            proc.destroy();
            return true;
        } catch (Exception e) {
            //the execution of dos executables sometimes generates Synchronisation problems, but
            //for this case it works well even if launch exception at this point
            return true;
        }
    }

    /**
     * Execute "RunCORSIM.exe" over the given file "pathtrf" and generates a
     * .out file
     *
     * @param pathtrf
     * @return true for successfull execution of CORSIM, output file generated,
     * false if error
     * @author Carlos Gaviria and Cristian Arteaga
     */
    public static boolean executeCorsim(String pathtrf) {
        try {
            String execute;
            try {
                File corsim_file = new File("RunCORSIM.exe");
                String dir_corsim = corsim_file.getPath();
                execute = dir_corsim + " \"" + pathtrf + "\" " + "out";
//                System.out.println("DIRCOrSIM"+dir_corsim);
            } catch (Exception ex) {
                ex.printStackTrace();
                return false;
            }

//            System.out.println(execute);
//            Process proc = Runtime.getRuntime().exec("cmd /C start/wait "+execute);
            Process proc = Runtime.getRuntime().exec("cmd /C start/wait /min " + execute);
            proc.waitFor();
            proc.destroy();
            return true;
        } catch (Exception e) {
            //the execution of dos executables sometimes generates Synchronisation problems, but
            //for this case it works well even if launch exception at this point
            return true;
        }
    }

    /**
     * Execute corsim n times for the given path
     * @param pathtrf
     * @param folder
     * @param n
     * @return
     */
    public static boolean executeCorsimMultiple(String pathtrf, String folder, int n) {
        try {
            String execute;
            try {
                File corsim_file = new File("RunCORSIM.exe");
                String dir_corsim = corsim_file.getPath();
                execute = dir_corsim + " \"" + pathtrf + "\" " + "runs=" + n + " out";
//                System.out.println("DIRCOrSIM"+dir_corsim);
            } catch (Exception ex) {
                ex.printStackTrace();
                return false;
            }

//            System.out.println(execute);
//            Process proc = Runtime.getRuntime().exec("cmd /C start/wait "+execute);
            Process proc = Runtime.getRuntime().exec("cmd /C start/wait /min " + execute);
            proc.waitFor();
            proc.destroy();
            return true;
        } catch (Exception e) {
            //the execution of dos executables sometimes generates Synchronisation problems, but
            //for this case it works well even if launch exception at this point
            return true;
        }
    }

    /**
     * Create a folder in the given path parameter
     * @param namefolder
     * @return
     */
    public static String createFolder(String namefolder) {
        try {
            File dir = new File(namefolder);
            dir.mkdir();
            return namefolder;
        } catch (SecurityException se) {
            UTIL.Log.error(se.getMessage());
            return null;
        }
    }

    /**
     * Delete the folder given in the path parameter
     * @param path
     */
    public static void deleteFolder(String path) {
        File file = new File(path);
        if (file.isDirectory()) {
            //directory is empty, then delete it
            if (file.list().length == 0) {
                file.delete();
            } else {
                //list all the directory contents
                String files[] = file.list();
                for (String temp : files) {
                    //construct the file structure
                    File fileDelete = new File(file, temp);
                    //recursive delete
                    deleteFolder(fileDelete.getAbsolutePath());
                }

                //check the directory again, if empty then delete it
                if (file.list().length == 0) {
                    file.delete();
                }
            }

        } else {//if file, then delete it
            file.delete();
        }
    }

    /**
     * Load into buffer each line in the file located into path
     *
     * @param path
     * @return buffer, array list of lines in the file
     * @author Carlos Gaviria and Cristian Arteaga
     */
    public static String[] fileToBuffer(String path) {
        try {
            //Count the number of lines of the file
            int number_line = 0;
            BufferedReader counter_line = new BufferedReader(new FileReader(path));
            String linea = counter_line.readLine();
            while (linea != null) {
                number_line++;
                linea = counter_line.readLine();
            }
            counter_line.close();

            String[] buffer = new String[number_line];
            int counter = 0;
            //Load in Buffer the lines of file
            BufferedReader reader = new BufferedReader(new FileReader(path));
            linea = reader.readLine();
            while (linea != null) {
                buffer[counter] = linea;
                linea = reader.readLine();
                counter++;
            }
            reader.close();
            return buffer;
        } catch (Exception e) {
            return null;
        }
    }

    /**
     * Save into file the lines contains in buffer
     *
     * @param buffer
     * @param path
     * @return boolean, false if error
     * @author Carlos Gaviria and Cristian Arteaga
     */
    public static boolean bufferToFile(String[] buffer, String path) {
        try {
            File f = new File(path);
            f.setWritable(true);
            FileWriter wt = new FileWriter(f);
            PrintWriter pw = new PrintWriter(wt);

            for (int i = 0; i < buffer.length; i++) {
                pw.println(buffer[i]);
            }
            wt.close();
            pw.close();
            return true;
        } catch (Exception e) {
            UTIL.Log.error("El archivo no pudo escribirse");
            return false;
        }

    }

    /**
     * Aux function, it converts a string into an integer, making validations.
     *
     * @param st
     * @return int number, -1 if is not a valid numeric string.
     * @author Carlos Gaviria and Cristian Arteaga
     */
    public static int stringToIntegerAux(String st) {
        String line_aux = "";
        int number;
        StringTokenizer clean_text = new StringTokenizer(st);
        while (clean_text.hasMoreElements()) {
            line_aux += clean_text.nextElement();
        }
        try {
            number = Integer.parseInt(line_aux);
            return number;
        } catch (NumberFormatException nfe) {
            return -1;
        }
    }

    /**
     * Aux function, it converts a string into an float, making validations.
     *
     * @param st
     * @return float number, -1 if is not a valid numeric string.
     * @author Carlos Gaviria and Cristian Arteaga
     */
    public static double stringToDoubleAux(String st) {
        String line_aux = "";
        double number;
        StringTokenizer clean_text = new StringTokenizer(st);
        while (clean_text.hasMoreElements()) {
            line_aux += clean_text.nextElement();
        }
        try {
            number = Double.parseDouble(line_aux);
            return number;
        } catch (NumberFormatException nfe) {
            return -1;
        }
    }

    /**
     * Aux function, it returns the value of a String from start to end
     *
     * @param line
     * @param start
     * @param end
     * @return string response, null on error
     * @author Carlos Gaviria and Cristian Arteaga
     */
    public static String getValue(String line, int start, int end) {
        String line_aux = "";
        if (line != null) {
            if (line.length() >= end && !line.equals("")) {
                for (int j = start; j <= end; j++) {
                    line_aux += line.charAt(j);
                }
                return line_aux;
            }
        }
        return "";
    }

    /**
     * Aux function, set value into String line, from start to end,
     * validate especial conditions
     *
     * @param line
     * @param value
     * @param start
     * @param end
     * @return string response, null on error
     * @author Carlos Gaviria and Cristian Arteaga
     */
    public static String setValue(String line, int value, int start, int end) {
        String string_value = String.valueOf(value);
        int length_value = string_value.length();

        if ((end - start) <= 0) {
            return line;
        }

        if (length_value > ((end - start) + 1)) {
            return null;
        }

        if (line.length() < start) { //is added the lenght neccessary into line
            for (int i = line.length(); i < end; i++) {
                line = line + " ";
            }
        }

        if (length_value < ((end - start) + 1)) {
            for (int i = length_value; i < ((end - start) + 1); i++) {
                string_value = " " + string_value;
            }
        }

        String trim_start = line.substring(0, start - 1);
        String trim_end = line.substring(end, line.length());

        return trim_start + string_value + trim_end;
    }

    /**
     *
     * @param matrix
     * @param path
     * @param tittle
     * @param pos
     */
    public static void printMatrixToFile(double[][] matrix, String path, String tittle, int pos) {
        try {
            if (matrix == null) {
                return;
            }
            FileWriter wt = new FileWriter(path, true);
            PrintWriter pw = new PrintWriter(wt);
            pw.println(tittle);
            for (int i = 0; i < matrix.length; i++) {
                pw.println(matrix[i][pos]);
            }
            wt.close();
            pw.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     *
     * @param matrix
     * @param tittle
     */
    public static void printArrayToConsoleInteger(int[] matrix, String tittle) {
        try {
            if (matrix == null) {
                return;
            }

            String cad = "";
            UTIL.Log.info(tittle);
            System.out.print("[");
            for (int i = 0; i < matrix.length; i++) {
                System.out.print(matrix[i] + ",");
            }
            System.out.print("]");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     *
     * @param matrix
     * @return
     */
    public static double[][] copyMatrixDouble(double[][] matrix) {
        if (matrix == null) {
            return null;
        }
        if (matrix.length == 0) {
            return null;
        }
        double[][] copy = new double[matrix.length][matrix[0].length];
        for (int i = 0; i < matrix.length; i++) {
            for (int j = 0; j < matrix[i].length; j++) {
                copy[i][j] = matrix[i][j];
            }
        }
        return copy;
    }

    /**
     *
     * @param matrix
     * @return
     */
    public static double[][] copyMatrixIntDouble(int[][] matrix) {
        if (matrix == null) {
            return null;
        }
        if (matrix.length == 0) {
            return null;
        }
        double[][] copy = new double[matrix.length][matrix[0].length];
        for (int i = 0; i < matrix.length; i++) {
            for (int j = 0; j < matrix[i].length; j++) {
                copy[i][j] = matrix[i][j];
            }
        }
        return copy;
    }

    /**
     *
     * @param matrix
     * @return
     */
    public static int[][] copyMatrixInt(int[][] matrix) {
        int[][] matrix2 = new int[matrix.length][matrix[0].length];
        if (matrix.length > 0) {
            for (int i = 0; i < matrix.length; i++) {
                for (int j = 0; j < matrix[0].length; j++) {
                    matrix2[i][j] = matrix[i][j];
                }

            }
        } else {
            UTIL.Log.error("Void matrix to copy");
        }
        return matrix2;
    }

    /**
     * Copy of an array
     * @param matrix
     * @return
     */
    public static int[] copyArrayInt(int[] matrix) {
        int[] matrix2 = new int[matrix.length];
        for (int i = 0; i < matrix.length; i++) {
            matrix2[i] = matrix[i];
        }
        return matrix2;
    }

    /**
     * Sort an array list
     * @param list
     * @return
     */
    public static ArrayList<int[]> sort(ArrayList<int[]> list) {

        for (int i = 0; i < list.size(); i++) {
            for (int j = 0; j < list.size() - 1; j++) {
                int[] array_j = list.get(j);
                int[] array_j1 = list.get(j + 1);
                if (array_j[0] > array_j1[0]) {
                    list.set(j, array_j1);
                    list.set(j + 1, array_j);
                }
            }
        }
        return list;
    }

    /**
     * Sum of two arrays
     * @param temp
     * @param index
     * @return
     */
    public static int arraySum(ArrayList<int[]> temp, int index) {
        int sum = 0;
        for (int i = 0; i < temp.size(); i++) {
            sum = sum + temp.get(i)[index];
        }
        return sum;
    }

    /**
     * Integer random between lower and upper
     * @param lower
     * @param upper
     * @return
     */
    public static int random(int lower, int upper) {
        return (int) (Math.random() * (upper - lower)) + lower;
    }

    /**
     * Double random between lower and upper
     * @param lower
     * @param upper
     * @return
     */
    public static double doublerandom(int lower, int upper) {
        return (Math.random() * (upper - lower)) + lower;
    }

    /**
     * Print a GEH graphic to console
     * @param output_real
     * @param output_model
     * @return
     */
    public static double[][] setGEHGraphPrintConsole(double[][] output_real, double[][] output_model) {
        int count = 0;
        int ln = 0;
        int cmp = 0;

        if (output_real == null || output_model == null) {
            UTIL.Log.info("Void Matrix");
            return null;
        }

        for (int i = 0; i < output_real.length; i++) {
            if (output_real[i][2] != -1) {
                ln++;
            }
        }
        double[][] data = new double[ln][3];
        for (int i = 0; i < output_real.length; i++) {

            if (output_real[i][2] != -1) {
                data[count][0] = count + 1;
                if (output_model[i][2] + output_real[i][2] == 0) {
                    data[count][1] = 0;
                } else {
                    data[count][1] = Math.sqrt(((output_model[i][2] - output_real[i][2]) * ((output_model[i][2] - output_real[i][2]))) / ((output_model[i][2] + output_real[i][2]) / 2));
                    if(data[count][1] <= 5){
                        cmp++;
                    }
                }
                count++;
            }
        }
        float res = cmp*100/count;
        UTIL.Log.info("GEH is less than 5.0 for "+res+"% of the links");

        return data;
    }

    /**
     *
     * @param output_real
     * @param output_model
     * @param type
     * @return
     */
    public static double[][] setGraphics(double[][] output_real, double[][] output_model, String type) {
        int pos = 0;
        double[][] data = new double[output_real.length][4];
        if (type.equals("Volume")) {
            pos = 2;
        }
        if (type.equals("Speed")) {
            pos = 3;
        }
        for (int i = 0; i < data.length; i++) {
            data[i][0] = output_real[i][0];//Initial node
            data[i][1] = output_model[i][1];//End node
            data[i][2] = output_real[i][pos];//X
            data[i][3] = output_model[i][pos];//Y
        }
        return data;
    }

    /**
     * Calculates the Normalized Root Mean Square for Volumes and Speeds for all
     * available data
     *
     * @param output_real
     * @param output_model
     * @param percentage
     * @return double[] theta_minus
     * @author Carlos Gaviria, Victor Molano and Cristian Arteaga
     */
    public static double NRMS(double[][] output_real, double[][] output_model, double percentage) {
//        UTIL.Tools.printMatrixToConsole(output_real, "OUTPUT REAL");
 //       UTIL.Tools.printMatrixToConsole(output_model, "OUTPUT MODEL");
        double aux_volume[] = new double[output_model.length];
        double aux_speed[] = new double[output_model.length];
        double percentage_volume = percentage / 100;
        int n_links_volume = 0;
        int n_links_speed = 0;

        double sum_errors_volume = 0;
        double sum_errors_speed = 0;
        double rms = 0;

        for (int i = 0; i < output_model.length; i++) {
            if (output_real[i][2] == 0) {
                aux_volume[i] = 1;
                n_links_volume++;
            } else if (output_real[i][2] == -1) {
                aux_volume[i] = 0;
            } else if (output_real[i][2] == 0 && output_model[i][2] == 0) {
                aux_volume[i] = 0;
                n_links_volume++;
            } else {
                aux_volume[i] = Math.pow((output_model[i][2] - output_real[i][2]) / output_real[i][2], 2);
                n_links_volume++;
            }

            if (output_real[i][3] == 0) {
                aux_speed[i] = 1;
                n_links_speed++;
            } else if (output_real[i][3] == -1) {
                aux_speed[i] = 0;
            } else if (output_real[i][3] == 0 && output_model[i][3] == 0) {
                aux_speed[i] = 0;
                n_links_speed++;
            } else {
                aux_speed[i] = Math.pow((output_model[i][3] - output_real[i][3]) / output_real[i][3], 2);
                n_links_speed++;
            }
        }
//        System.err.println("j= "+j);
        
        for (int i = 0; i < aux_volume.length; i++) {
            sum_errors_volume = sum_errors_volume + aux_volume[i];
        }
        for (int i = 0; i < aux_speed.length; i++) {
            sum_errors_speed = sum_errors_speed + aux_speed[i];
        }

//        System.out.println("n_links_volume="+n_links_volume);
//        System.out.println("n_links_speed="+n_links_speed);
//        System.out.println("sum_errors_volume="+sum_errors_volume);
//        System.out.println("sum_errors_speed="+sum_errors_speed);
        double prevVolume;
        double prevSpeed;
        if (n_links_volume != 0) {
            prevVolume = (percentage_volume * Math.sqrt(sum_errors_volume / n_links_volume));
        } else {
            prevVolume = 0;
        }

        if (n_links_speed != 0) {
            prevSpeed = (1 - percentage_volume) * Math.sqrt(sum_errors_speed / n_links_speed);
        } else {
            prevSpeed = 0;
        }

        rms = prevVolume + prevSpeed;
        return rms;
    }

    /**
     * Apply the corsim normal constraints for the theta matrix given as parameter
     * @param theta
     * @return
     */
    public static int[][] applyConstraints(double[][] theta) {
        int[][] new_theta = new int[theta.length][7];

        for (int i = 0; i < theta.length; i++) {
            new_theta[i][0] = (int) theta[i][0];
            new_theta[i][1] = (int) theta[i][1];
            new_theta[i][2] = (int) theta[i][2];
            new_theta[i][3] = (int) theta[i][3];
            new_theta[i][4] = (int) theta[i][4];
            new_theta[i][5] = (int) Math.round(theta[i][5]);
            new_theta[i][6] = (int) theta[i][6];
//            new_theta[i][7] = (int) theta[i][7];
//            new_theta[i][8] = (int) theta[i][8];
//            new_theta[i][9] = (int) theta[i][9];

            if (new_theta[i][5] < new_theta[i][3]) {
                new_theta[i][5] = new_theta[i][3];
            }
            if (new_theta[i][5] > new_theta[i][4]) {
                new_theta[i][5] = new_theta[i][4];
            }
        }

        return new_theta;
    }

    /**
     * Apply the record type special constraints for the theta matrix given as
     * parameter
     *
     * @param theta
     * @return
     */
    public static int[][] applyRTContraints(int[][] theta) {
        //Theta Positions [0]RT [1]SC [2]EC [3]MinV [4]MaxV [5]VAL [6]LN
        boolean cons_rt_147 = false, cons_rt_149 = false, cons_rt_153 = false, cons_rt_70 = false, cons_rt_71 =false;
        CorsimModel model = new CorsimModel();        
        int[] max_dec_type = {150,150,150,150,150,150,150,150,150};
        int[] sc_70 ={25,29,33,37,41,45,49,53,57} ;
        for (int i = 0; i < theta.length; i++) {
            Entry entry1 = model.getEntry(1, 4, theta[i][0], Long.valueOf(theta[i][1]) + "-" + Long.valueOf(theta[i][2]));
            int vehicle_type = (int)entry1.getValue();
            if (theta[i][0] == 147) {
                cons_rt_147 = true;
            }
            if (theta[i][0] == 149) {
                cons_rt_149 = true;
            }
            if (theta[i][0] == 153) {
                cons_rt_153 = true;
            }
            if (theta[i][0] == 70 ) {
                cons_rt_70 = true; 
            }
            if (entry1 != null ) {

                Entry entry2 = model.getEntry(13, 16, theta[i][0], Long.valueOf(theta[i][1]) + "-" + Long.valueOf(theta[i][2]));
                switch (vehicle_type){
                    case 1: max_dec_type[1] = (int)entry2.getValue();
                        break;
                    case 2: max_dec_type[2] = (int)entry2.getValue();
                        break;
                    case 3: max_dec_type[3] = (int)entry2.getValue();
                        break;
                    case 4: max_dec_type[4] = (int)entry2.getValue();
                        break;
                    case 5: max_dec_type[5] = (int)entry2.getValue();
                        break;
                    case 6: max_dec_type[6] = (int)entry2.getValue();
                        break;
                    case 7: max_dec_type[7] = (int)entry2.getValue();
                        break;
                    case 8: max_dec_type[8] = (int)entry2.getValue();
                        break;
                    case 9: max_dec_type[9] = (int)entry2.getValue();
                        break;    
                    default : break;
                }
            }
        }

        if (cons_rt_70){
            for(int k = 0; k < sc_70.length; k++){
                for (int i = 0; i < theta.length; i++) {
                    if (theta[i][0] == 70 && theta[i][1] == sc_70[i] ) {
                        if((int) theta[i][5] >max_dec_type[i])
                            theta[i][5] = max_dec_type[i];
                    }
                }
            }
        }
        if (cons_rt_153) {
            int value1 = 0;
            int value2 = 0;
            int dif = 0;
            double rand = 0;
            int one = 0;
            int Min = 0;
            int Max = 0;

            ArrayList<int[]> entry_vals = new ArrayList<>();

            for (int i = 0; i < theta.length; i++) {
                if (theta[i][0] == 153) {
                    int[] entry = new int[3];
                    entry[0] = (int) theta[i][6];   // LN
                    entry[1] = i;                   //ARRAY POS
                    entry[2] = (int) theta[i][5];   //VALUE
                    Min = (int) theta[i][3];
                    Max = (int) theta[i][4];
                    entry_vals.add(entry);
                }
            }

            entry_vals = sort(entry_vals);

            for (int i = 0; i < entry_vals.size(); i = i + 2) {

                value1 = entry_vals.get(i)[2];
                value2 = entry_vals.get(i + 1)[2];

                if ((value1 + value2) != 100) {
                    dif = 100 - (value1 + value2);

                    one = dif / Math.abs(dif);
                    dif = Math.abs(dif);
                    for (int j = 0; j < dif; j++) {
                        rand = Math.random();
                        int temp;
                        if (rand <= 0.5) {
                            temp = value1 + one;
                            if (temp < Min || temp > Max) {
                                j--;
                            } else {
                                value1 = value1 + one;
                            }
                        } else {
                            temp = value2 + one;
                            if (temp < Min || temp > Max) {
                                j--;
                            } else {
                                value2 = value2 + one;
                            }
                        }
                    }
                    theta[entry_vals.get(i)[1]][5] = value1;
                    theta[entry_vals.get(i + 1)[1]][5] = value2;
                }
            }
        }

        if (cons_rt_147) {
            int[] values = new int[10];
            int dif = 0;
            double rand = 0;
            int one = 0;
            int Min = 0;
            int Max = 0;

            ArrayList<int[]> entry_vals = new ArrayList<>();

            for (int i = 0; i < theta.length; i++) {
                if (theta[i][0] == 147) {
                    int[] entry = new int[3];
                    entry[0] = (int) theta[i][6];
                    entry[1] = i;
                    entry[2] = (int) theta[i][5];
                    Min = (int) theta[i][3];
                    Max = (int) theta[i][4];
                    entry_vals.add(entry);
                }
            }

            entry_vals = sort(entry_vals);

            for (int i = 0; i < entry_vals.size(); i = i + 10) {

                //value1 = entry_vals.get(i)[2];
                //value2 = entry_vals.get(i + 1)[2];
                for (int j = 0; j < 10; j++) {
                    values[j] = entry_vals.get(i + j)[2];
                }
                int add = 0;
                for (int j = 0; j < 10; j++) {
                    add = add + values[j];
                }
                if (add != 1000) {
                    dif = 1000 - (add);

                    one = dif / Math.abs(dif);
                    dif = Math.abs(dif);
                    for (int j = 0; j < dif; j++) {
                        int pos = randomPosition0to10();
                        int temp = values[pos] + one;
                        if (temp < Min || temp > Max) {
                            j--;
                        } else {
                            values[pos] = values[pos] + one;
                        }
                    }
                    for (int j = 0; j < 10; j++) {
                        theta[entry_vals.get(i + j)[1]][5] = values[j];
                    }
                }
            }

        }

        if (cons_rt_149) {
            int[] values = new int[10];
            int dif = 0;
            double rand = 0;
            int one = 0;
            int Min = 0;
            int Max = 0;

            ArrayList<int[]> entry_vals = new ArrayList<>();

            for (int i = 0; i < theta.length; i++) {
                if (theta[i][0] == 149) {
                    int[] entry = new int[3];
                    entry[0] = (int) theta[i][6];
                    entry[1] = i;
                    entry[2] = (int) theta[i][5];
                    Min = (int) theta[i][3];
                    Max = (int) theta[i][4];
                    entry_vals.add(entry);
                }
            }

            entry_vals = sort(entry_vals);

            for (int i = 0; i < entry_vals.size(); i = i + 10) {
                for (int j = 0; j < 10; j++) {
                    values[j] = entry_vals.get(i + j)[2];
                }
                int add = 0;
                for (int j = 0; j < 10; j++) {
                    add = add + values[j];
                }
                if (add != 1000) {
                    dif = 1000 - (add);
//                    System.out.println("add=" + add + ",dif=" + dif);
                    one = dif / Math.abs(dif);
                    dif = Math.abs(dif);
                    for (int j = 0; j < dif; j++) {
                        int pos = randomPosition0to10();
                        int temp = values[pos] + one;
                        if (temp < Min || temp > Max) {
                            j--;
                        } else {
                            values[pos] = values[pos] + one;
                        }
                    }
                    for (int j = 0; j < 10; j++) {
                        theta[entry_vals.get(i + j)[1]][5] = values[j];
                    }
                }
            }
        }

        return theta;
    }

    /**
     * Get a random number between 0 and 9 to be used as position into an array
     *
     * @return
     */
    public static int randomPosition0to10() {
        double rand = Math.random();
        if (rand >= 0 && rand < 0.1) {
            return 0;
        }
        if (rand >= 0.1 && rand < 0.2) {
            return 1;
        }
        if (rand >= 0.2 && rand < 0.3) {
            return 2;
        }
        if (rand >= 0.3 && rand < 0.4) {
            return 3;
        }
        if (rand >= 0.4 && rand < 0.5) {
            return 4;
        }
        if (rand >= 0.5 && rand < 0.6) {
            return 5;
        }
        if (rand >= 0.6 && rand < 0.7) {
            return 6;
        }
        if (rand >= 0.7 && rand < 0.8) {
            return 7;
        }
        if (rand >= 0.8 && rand < 0.9) {
            return 8;
        }
        if (rand >= 0.9 && rand <= 1.0) {
            return 9;
        }
        return 0;
    }

    /**
     *
     * @return
     */
    public static int randomPosition1to2() {
        double rand = Math.random();
        if (rand < 0.5) {
            return 0;
        } else {
            return 1;
        }
    }

    /**
     * Returns path without extension of 3 values
     *
     * @param path
     * @return int[][] Output, null if error.
     * @author Carlos Gaviria and Cristian Arteaga
     */
    public static String removeExtension(String path) {
        String path_out = path.substring(0, path.length() - 4);
        return path_out;
    }

    /**
     * Return a new bidimensional array when Matrix2 is added to Matrix1
     *
     * @param matrix1
     * @param matrix2
     * @param size1
     * @param size2
     * @return double[][] added
     * @author Carlos Gaviria and Cristian Arteaga
     */
    public static double[][] addMatrix(double[][] matrix1, double[][] matrix2, int size1, int size2) {
        if (matrix1 == null) {
            if (matrix2 != null) {
                return matrix2;
            }
        }
        if (matrix2 == null) {
            return matrix1;
        }

        if (size1 == 0 && size2 > 0) {
            return matrix2;
        }

        if (size1 > 0 && size2 == 0) {
            return matrix1;
        }

        double[][] added = new double[matrix1.length + matrix2.length][7];
        for (int i = 0; i < matrix1.length; i++) {
            added[i] = matrix1[i];
        }
        for (int i = matrix1.length; i < matrix1.length + matrix2.length; i++) {
            added[i] = matrix2[i - matrix1.length];
        }
        return added;
    }

    /**
     * returns a matrix of integers from the input String matrix (casting)...
     *
     * @param input
     * @return int[][] Output, null if error.
     * @author Carlos Gaviria and Cristian Arteaga
     */
    public static int[][] stringMatrixToIntMatrix(String[][] input) {
        if (input == null || input.length == 0) {
            return null;
        }
        int cols = input[0].length;

        int[][] output = new int[input.length][cols];

        for (int i = 0; i < input.length; i++) {
            for (int j = 0; j < cols; j++) {
                output[i][j] = Integer.parseInt(input[i][j]);
            }
        }
        return output;
    }

    /**
     * Returns a matrix of doubles from the input String matrix (casting)
     *
     * @param input
     * @return int[][] Output, null if error.
     * @author Carlos Gaviria and Cristian Arteaga
     */
    public static double[][] stringMatrixToDoubleMatrix(String[][] input) {
        if (input == null || input.length == 0) {
            return null;
        }
        int cols = input[0].length;

        double[][] output = new double[input.length][cols];

        for (int i = 0; i < input.length; i++) {
            for (int j = 0; j < cols; j++) {
                output[i][j] = Double.parseDouble(input[i][j]);
            }
        }
        return output;
    }

    /**
     * Returns a matrix of strings from the input int matrix (casting)...
     *
     * @param input
     * @return int[][] Output, null if error.
     * @author Carlos Gaviria and Cristian Arteaga
     */
    public static String[][] intMatrixToStringMatrix(int[][] input) {
        if (input == null || input.length == 0) {
            return null;
        }
        int cols = input[0].length;

        String[][] output = new String[input.length][cols];

        for (int i = 0; i < input.length; i++) {
            for (int j = 0; j < cols; j++) {
                output[i][j] = String.valueOf(input[i][j]);
            }
        }
        return output;
    }

    /**
     * Save a double matrix to file
     *
     * @param matrix
     * @param path
     * @param tittle
     */
    public static void printMatrixToFile(double[][] matrix, String path, String tittle) {
        try {
            if (matrix == null) {
                return;
            }
            FileWriter wt = new FileWriter(path, true);
            PrintWriter pw = new PrintWriter(wt);
            pw.println("-----------------------");
            pw.println(tittle);
            for (int i = 0; i < matrix.length; i++) {
                String line = "";
                for (int j = 0; j < matrix[i].length; j++) {
                    line = line + matrix[i][j] + ";";
                }
                pw.println(line);
            }
            pw.println("-----------------------");
            wt.close();
            pw.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Save an integer matrix to file
     *
     * @param matrix
     * @param path
     * @param tittle
     * @param subtitle
     */
    public static void printMatrixToFileInt(int[][] matrix, String path, String tittle, String subtitle) {
        try {
            if (matrix == null) {
                return;
            }
            FileWriter wt = new FileWriter(path, true);
            PrintWriter pw = new PrintWriter(wt);

            pw.println(tittle);
            pw.println(subtitle);
            for (int i = 0; i < matrix.length; i++) {
                String line = "";
                for (int j = 0; j < matrix[i].length; j++) {
                    line = line + matrix[i][j] + ",";
                }
                pw.println(line);
            }
            pw.println("");
            pw.println("");
            wt.close();
            pw.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Print a double matrix to console
     *
     * @param matrix
     * @param tittle
     */
    public static void printMatrixToConsole(double[][] matrix, String tittle) {
        try {
            if (matrix == null) {
                return;
            }

            String cad = "";
            UTIL.Log.info(tittle);
            for (int i = 0; i < matrix.length; i++) {
                for (int j = 0; j < matrix[i].length; j++) {
                    cad = cad + matrix[i][j] + "\t";
                }
                UTIL.Log.info(cad);
                cad = "";
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Print an integer matrix to console
     *
     * @param matrix
     * @param tittle
     */
    public static void printMatrixToConsoleInteger(int[][] matrix, String tittle) {
        try {
            if (matrix == null) {
                return;
            }

            String cad = "";
            UTIL.Log.info(tittle);
            for (int i = 0; i < matrix.length; i++) {
                for (int j = 0; j < matrix[i].length; j++) {
                    cad = cad + matrix[i][j] + "\t";
                }
                UTIL.Log.info(cad);
                cad = "";
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Print a the matrix given with its respective tittle
     *
     * @param matrix
     * @param tittle
     */
    public static void printMatrixToConsoleLine(double[][] matrix, String tittle) {
        try {
            if (matrix == null) {
                return;
            }

            String cad = "";
            UTIL.Log.info(tittle);
            for (int i = 0; i < matrix.length; i++) {
                for (int j = 0; j < matrix[i].length; j++) {
                    cad = cad + matrix[i][j] + ",";
                }
                cad = "[" + cad + "]";
                UTIL.Log.info(cad);
                cad = "";
            }
            UTIL.Log.info("");
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    /**
     * Important function used to calculate the numbers of
     *
     * @param min
     * @param max
     * @param changepercentage
     * @return
     */
    public static int calculateAlteration(int min, int max, float changepercentage) {
        int alteration = 0;
        int range = max - min;
        if (range > 0) {
            if (range <= 10) {
                alteration = 1;
            } else {
                alteration = (int) Math.round((float) range * (float) changepercentage / 100.0);
            }

        } else {
            UTIL.Log.error("Wrong max and min values");
        }
        return alteration;
    }

    /**
     * Returns a NTP date from a list of servers, it is used to validate license
     *
     * @return
     */
    public static Date getNTPDate() {

        String[] hosts = new String[]{
            "ntp02.oal.ul.pt", "ntp04.oal.ul.pt",
            "ntp.xs4all.nl"};

        NTPUDPClient client = new NTPUDPClient();
        // We want to timeout if a response takes longer than 5 seconds
        client.setDefaultTimeout(5000);

        for (String host : hosts) {

            try {
                InetAddress hostAddr = InetAddress.getByName(host);
                TimeInfo info = client.getTime(hostAddr);
                Date date = new Date(info.getReturnTime());
                return date;

            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        client.close();

        return null;

    }

    /**
     * Check if the software has expired
     *
     * @return
     */
    public static boolean expirated() {
        try {
            Date initial = new SimpleDateFormat("dd/MM/yyyy").parse("24/10/2014");
            Date end = UTIL.Tools.getNTPDate();
            long days = TimeUnit.DAYS.convert(end.getTime() - initial.getTime(), TimeUnit.MILLISECONDS);
            return days > 365;
        } catch (ParseException ex) {
            Logger.getLogger(Test.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }

    }

    /**
     * Check if the software has been used a lot of times, considering that 100
     * is the maximum
     *
     * @param maxuses
     * @return
     */
    public static boolean expiratedUsesNumber(int maxuses) {
        JWindowsReg regman = new JWindowsReg("org.username");
        if (!regman.isInitialized()) {
            regman.initialize();
        }
        regman.incrementIntegerHKCU();
        int iteration = regman.readIntegerHKCU();
        if (iteration <= maxuses) {
            return true;
        } else {
            return false;
        }

    }

    /**
     * Converts a list of integer arrays to a matrix
     *
     * @param list
     * @return
     */
    public static int[][] vectorsListToMatrixTheta(List<int[]> list) {
        int[][] matrix = new int[list.size()][9];
        int i = 0;
        for (int[] vector : list) {
            System.arraycopy(vector, 0, matrix[i], 0, 9);
            i++;
        }
        return matrix;
    }
    /* Sara*/
public static String[][] vectorsListToMatrixNames(List<String[]> list) {
        String[][] matrix = new String[list.size()][3];
        int i = 0;
        for (String[] vector : list) {
            System.arraycopy(vector, 0, matrix[i], 0, 3);
            i++;
        }
        return matrix;
    }
    /**
     * Convert a list to a matrix filter
     *
     * @param list
     * @return
     * @deprecated
     */
    public static int[][] vectorsListToMatrixFilter(List<int[]> list) {
        int[][] matrix = new int[list.size()][4];
        int i = 0;
        for (int[] vector : list) {
            System.arraycopy(vector, 0, matrix[i], 0, 4);
            i++;
        }
        return matrix;
    }
    
    

    /**
     * Get the columns from a filter
     *
     * @param theta
     * @return
     * @deprecated
     */
    public static String[] getImprovedFilterColumns(int[][] theta) {

        List<String> names = new ArrayList();
        for (int i = 0; i < theta.length; i++) {
            if (!names.contains(String.valueOf(theta[i][0]))) {
                names.add(String.valueOf(theta[i][0]));
            }
        }

        String[] columns = new String[names.size()];
        java.util.Collections.sort(names);
        for (int i = 0; i < names.size(); i++) {
            columns[i] = names.get(i);
        }

        return columns;
    }

    /**
     * Get the rows from a filter
     *
     * @param theta
     * @return
     * @deprecated
     */
    public static String[] getImprovedFilterRows(int[][] theta) {

        List<String> names = new ArrayList();
        for (int i = 0; i < theta.length; i++) {
            String rt = String.valueOf(theta[i][7]) + "-" + String.valueOf(theta[i][8]);
            if (!names.contains(rt)) {
                names.add(rt);
            }
        }

        String[] rows = new String[names.size()];
        java.util.Collections.sort(names);
        for (int i = 0; i < names.size(); i++) {
            rows[i] = names.get(i);
        }

        return rows;
    }

    /**
     * Given the filter matrix return the theta matrix after apply a filter
     *
     * @param theta
     * @param filter
     * @deprecated
     * @return
     */
    public static int[][] applyThetaFilter(int[][] theta, int[][] filter) {
        //Scheme of theta   [0] Rt,     [1] SC,      [2] EC, [3] MinV,      [4] MaxV,  [5] Value, [6] lineNumber,[7] InNode, [8]EndNode
        //Scheme of filter  [0] InNode, [1] EndNode, [2] RT, [3] calibrate?

        List<int[]> list_matrix = new ArrayList<int[]>();
        for (int i = 0; i < theta.length; i++) {
            if (calibrateThetaEntry(theta[i], filter)) {
                list_matrix.add(theta[i]);
            }
        }
        int[][] newtheta = Tools.vectorsListToMatrixTheta(list_matrix);
        return newtheta;
    }

    /**
     * Check in the filter matrix if a entry is selected to be calibrated
     *
     * @param entry
     * @param filter
     * @deprecated
     *
     * @return
     */
    public static boolean calibrateThetaEntry(int[] entry, int[][] filter) {
        boolean calibrate = false;
        for (int i = 0; i < filter.length; i++) {
            if (filter[i][2] == entry[0] && filter[i][0] == entry[7] && filter[i][1] == entry[8]) {
                return filter[i][3] == 1;
            }
        }
        return calibrate;
    }

    /**
     * Delete repeated values in a list array
     *
     * @param oldmatrix
     * @return
     */
    public static List<int[]> deleteRepeatedListArrayInt(List<int[]> oldmatrix) {
        List<int[]> newlist = new ArrayList<>();
        for (int[] element : oldmatrix) {
            boolean exist = false;
            for (int[] elementnew : newlist) {
                if (Arrays.equals(element, elementnew)) {
                    exist = true;
                }
            }
            if (!exist) {
                newlist.add(element);
            }
        }
        return newlist;
    }

    /**
     * Method used to check if a filter is setted.
     *
     * @param oldfilter
     * @param newfilter
     * @return
     * @deprecated
     */
    public static int[][] checkFiltersExistence(int[][] oldfilter, int[][] newfilter) {
        //Scheme of filter  [0] InNode, [1] EndNode, [2] RT, [3] calibrate?
        for (int i = 0; i < newfilter.length; i++) {
            int[] actual = newfilter[i];
            for (int j = 0; j < 10; j++) {
                if (oldfilter[j][0] == newfilter[i][0]
                        && oldfilter[j][1] == newfilter[i][1]
                        && oldfilter[j][2] == newfilter[i][2]) {
                    newfilter[i][3] = oldfilter[j][3];
                }
            }

        }
        return newfilter;
    }

    /**
     * Verify if the theta matrix has the link and record type parameters
     *
     * @param theta
     * @param link
     * @param rt
     * @return
     */
    public static int checkLinkRecordTypeExistece(int[][] theta, String link, String rt) {
        //Scheme of theta   [0] Rt,     [1] SC,      [2] EC, [3] MinV,      [4] MaxV,  [5] Value, [6] lineNumber,[7] InNode, [8]EndNode
        //Scheme of filter  [0] InNode, [1] EndNode, [2] RT, [3] calibrate?

        for (int i = 0; i < theta.length; i++) {
            if (rt.equals(String.valueOf(theta[i][0])) && link.equals(String.valueOf(theta[i][7]) + "-" + String.valueOf(theta[i][8]))) {
                return 1;
            }
        }
        return -1;
    }

    /**
     *
     * @param theta
     */
    public static void convertThetaMatrixToCorsimModel(int[][] theta) {
        //Scheme of theta   [0] Rt, [1] SC, [2] EC, [3] MinV, [4] MaxV, [5] Value,  [6] lineNumber,  [7] InNode, [8] EndNode,[9] entry
        CorsimModel model = new CorsimModel();
        model.setEntriesnumber(theta.length);
        for (int i = 0; i < theta.length; i++) {
        }

        for (int i = 0; i < theta.length; i++) {
            Entry entry = new Entry();
            entry.setId(String.valueOf(theta[i][9]));
            entry.setStartcolumn(Long.valueOf(theta[i][1]));
            entry.setEndcolumn(Long.valueOf(theta[i][2]));
            entry.setMinvalue(Integer.valueOf(theta[i][3]));
            entry.setMaxvalue(Integer.valueOf(theta[i][4]));
            entry.setValue(Double.valueOf(theta[i][5]));
            entry.setLinenumber(Long.valueOf(theta[i][6]));
            Link link = model.getLink(Long.valueOf(theta[i][7]), Long.valueOf(theta[i][8]));
            if (link != null) {
                RecordType recordtype = link.getRecordType(String.valueOf(theta[i][0]));
                if (recordtype != null) {
                    recordtype.addToEntries(entry);
                } else {
                    RecordType newrecordtype = new RecordType();
                }
            }
        }
    }
//    public static int[][] convertCorsimModelToThetaMatrix(CorsimModel model) {
//        //Scheme of theta   [0] Rt, [1] SC, [2] EC, [3] MinV, [4] MaxV, [5] Value,  [6] lineNumber,  [7] InNode, [8] EndNode,[9] entry
//    }

    /**
     * Print a matrix in a format that can be used for static initialize a java
     * Matrix
     *
     * @param theta
     */
    public static void tempPrintMatrix(int[][] theta) {
        for (int i = 0; i < theta.length; i++) {
            System.out.print("{,");
            for (int j = 0; j < theta[i].length; j++) {
                System.out.print("[" + theta[i][j] + "],");
            }
            System.out.print("},");
            System.out.println("");
        }
    }
}