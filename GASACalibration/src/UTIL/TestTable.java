/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package UTIL;

import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.*;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author das
 */
public class TestTable {

    boolean flag = false;

    /**
     *
     */
    public void create() {
        JTable table = new JTable(2, 2) {
            public boolean isCellEditable(int row, int column) {
                if (flag) {
                    return false;
                }
                return true;
            }
        };
        Object[][] dataJTable = {{new Boolean(false),new Boolean(true)},{new Boolean(false),new Boolean(true)}};
        String[] columns = {"X","Y"};
        DefaultTableModel model = new DefaultTableModel(dataJTable, columns) {
            @Override
            public Class getColumnClass(int columnIndex) {
                return Boolean.class;
            }
        };
        table.setModel(model);
        
        JFrame f = new JFrame();
        JButton button = new JButton("Disable");
        button.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                flag = true;
            }
        });
        f.setLayout(new FlowLayout());
        f.add(new JScrollPane(table));
        f.add(button);
        f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        f.setVisible(true);
        
    }

    /**
     *
     * @param args
     */
    public static void main(String[] args) {
        SwingUtilities.invokeLater(new Runnable() {
            public void run() {
                new TestTable().create();
            }
        });
    }
}