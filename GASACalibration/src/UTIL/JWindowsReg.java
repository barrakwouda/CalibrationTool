/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package UTIL;

import java.util.prefs.Preferences;

/**
 * Allows communication with the microsoft windows registry
 * @author cristian
 */
public class JWindowsReg {

    public JWindowsReg(String key) {
        this.PREF_KEY = key;
    }

    public JWindowsReg() {
        this.PREF_KEY = "org.username";
    }

    private String PREF_KEY = "";

    public void writeHKCU(String valor) {
        Preferences userPref = Preferences.userRoot();
        userPref.put(PREF_KEY, valor);
    }

    public void initialize() {
        Preferences userPref = Preferences.userRoot();
        userPref.put(PREF_KEY, "0");
    }

    public void incrementIntegerHKCU() {
        int execNum = readIntegerHKCU() + 1;
        writeHKCU(execNum + "");
    }

    public String readHKCU() {
        Preferences userPref = Preferences.userRoot();
        return userPref.get(PREF_KEY, PREF_KEY + " was not found.");
    }

    public boolean isInitialized() {
        return !readHKCU().equals(PREF_KEY + " was not found.");
    }

    public int readIntegerHKCU() {
        return Integer.parseInt(readHKCU());
    }
}