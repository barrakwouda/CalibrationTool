package DOMAIN.Model;

import java.io.Serializable;

/**
 * Abstracts all the information contained in a Corsim Entry
 * @author Carlos Gaviria, Cristian Arteaga and Victor Molano
 */
public class Entry implements Serializable{

    private String id;
    private String description;
    private double value;
    private int minvalue;
    private int maxvalue;
    private long startcolumn;
    private long endcolumn;
    private long linenumber;
    private boolean calibrate;

    public void setId(String id) {
        this.id = id;
    }

    public String getId() {
        return this.id;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDescription() {
        return this.description;
    }

    public void setValue(double value) {
        this.value = value;
    }

    public double getValue() {
        return this.value;
    }

    public void setMinvalue(int minvalue) {
        this.minvalue = minvalue;
    }

    public int getMinvalue() {
        return this.minvalue;
    }

    public void setMaxvalue(int maxvalue) {
        this.maxvalue = maxvalue;
    }

    public int getMaxvalue() {
        return this.maxvalue;
    }

    public void setStartcolumn(long startcolumn) {
        this.startcolumn = startcolumn;
    }

    public long getStartcolumn() {
        return this.startcolumn;
    }

    public void setEndcolumn(long endcolumn) {
        this.endcolumn = endcolumn;
    }

    public long getEndcolumn() {
        return this.endcolumn;
    }

    public void setLinenumber(long linenumber) {
        this.linenumber = linenumber;
    }

    public long getLinenumber() {
        return this.linenumber;
    }

    public void setCalibrate(boolean calibrate) {
        this.calibrate = calibrate;
    }

    public boolean getCalibrate() {
        return this.calibrate;
    }

    @Override
    public String toString() {
        return "\t\t\tENTRY[id=" + id +",description ="+description+",value=" + value +",minvalue=" + minvalue +",maxvalue=" + maxvalue +",startcolumn=" + startcolumn +",endcolumn=" + endcolumn +",linenumber=" + linenumber +",calibrate=" + calibrate+"]";
    }
}
