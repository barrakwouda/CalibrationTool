package DOMAIN.Model;

import java.io.Serializable;
import java.util.*;

/**
 * Abstracts all the necessary information for a Corsim Link
 * @author Carlos Gaviria, Cristian Arteaga and Victor Molano
 */
public class Link implements Serializable{

    String id;
    String description;
    long startnode;
    long endnode;
    List<RecordType> recordtypes;

    public Link() {
        this.recordtypes = new ArrayList();
    }

    public void setCalibrate(boolean active) {
        for (RecordType recordtype : recordtypes) {
            recordtype.setCalibrate(active);
        }
    }

    public boolean getCalibrate(){
        for (RecordType recordtype : recordtypes){
            if(recordtype.getCalibrate()){
                return true;
            }
        }
        return false; 
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setId(long startnode,long endnode) {
        this.startnode = startnode;
        this.endnode = endnode;;
    }

    public String getId() {
        return this.startnode + "-" + this.endnode;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDescription() {
        return this.description;
    }

    public void setStartnode(long startnode) {
        this.startnode = startnode;
    }

    public long getStartnode() {
        return this.startnode;
    }

    public void setEndnode(long endnode) {
        this.endnode = endnode;
    }

    public long getEndnode() {
        return this.endnode;
    }

    public void setRecordtypes(List<RecordType> recordtypes) {
        this.recordtypes = recordtypes;
    }

    public List<RecordType> getRecordtypes() {
        return this.recordtypes;
    }

    public void addToRecordtypes(RecordType recordtype) {
        this.recordtypes.add(recordtype);
    }

    public void removeFromRecordtypes(RecordType recordtype) {
        this.recordtypes.remove(recordtype);
    }

    public RecordType getRecordType(String recordtypeid) {
        for (RecordType recordtype : recordtypes) {
            if (recordtype.getId().equals(recordtypeid)) {
                return recordtype;
            }
        }
        return null;
    }

    public String toStringCascade() {
        String cascade = this.toString();
        for (RecordType recordType : recordtypes) {
            cascade += "\n" + recordType.toStringCascade();
        }
        return cascade;
    }

    @Override
    public String toString() {
        return "\tLINK[id=" + startnode + "," + endnode + ",description =" + description + "]";
    }
}
