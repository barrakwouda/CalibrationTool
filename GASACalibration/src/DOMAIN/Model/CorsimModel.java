package DOMAIN.Model;

import UTIL.Tools;
import java.io.Serializable;
import java.util.*;

/**
 * Abstracts all the information contained in a corsim model
 *
 * @author Carlos Gaviria, Cristian Arteaga and Victor Molano
 */
public class CorsimModel implements Serializable {

    private String id;
    private String description;
    private String filename;
    private List<Link> links;
    private long entriesnumber;

    public long getEntriesnumber() {
        return entriesnumber;
    }

    public void setEntriesnumber(long entriesnumber) {
        this.entriesnumber = entriesnumber;
    }

    public CorsimModel() {
        this.links = new ArrayList();
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getId() {
        return this.id;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDescription() {
        return this.description;
    }

    public void setFilename(String filename) {
        this.filename = filename;
    }

    public String getFilename() {
        return this.filename;
    }

    public void setLinks(List<Link> links) {
        this.links = links;
    }

    public List<Link> getLinks() {
        return this.links;
    }

    public void addToLinks(Link link) {
        this.links.add(link);
    }

    public void removeFromLinks(Link link) {
        this.links.remove(link);
    }

    public void setCalibrate(boolean value) {
        for (Link link : links) {
            link.setCalibrate(value);
        }
    }

    /**
     * Returns the requested link with start and end node given as parameters.
     *
     * @param startnode
     * @param endnode
     * @return
     */
    public Link getLink(long startnode, long endnode) {
        for (Link link : links) {
            if (link.startnode == startnode && link.endnode == endnode) {
                return link;
            }
        }
        return null;
    }

    /**
     * Return a String object with his attributes and the attributes of his
     * contained classes
     *
     * @return
     */
    public String toStringCascade() {
        String cascade = this.toString();
        for (Link link : links) {
            cascade += "\n" + link.toStringCascade();
        }
        return cascade;
    }

    /**
     * Returns the theta matrix after apply the filter based in the flag of
     * calibration on each entry
     *
     * @param theta
     * @return
     */
    public int[][] getFilteredTheta(int[][] theta) {
        //Scheme of theta   [0] Rt, [1] SC, [2] EC, [3] MinV, [4] MaxV, [5] Value,  [6] lineNumber,  [7] InNode, [8] EndNode,[9] entry
        List<int[]> list_matrix = new ArrayList<int[]>();
        for (int i = 0; i < theta.length; i++) {
            Entry entry = getEntry(theta[i][7], theta[i][8], theta[i][0], Long.valueOf(theta[i][1]) + "-" + Long.valueOf(theta[i][2]));
            if (entry == null) {
                UTIL.Tools.printArrayToConsoleInteger(theta[i], "Entry");
                UTIL.Tools.printMatrixToConsoleInteger(theta, "Whole matrix");
            }
            if (entry.getCalibrate()) {
                theta[i][3] = entry.getMinvalue();
                theta[i][4] = entry.getMaxvalue();
                list_matrix.add(theta[i]);
            }
        }
        int[][] newtheta = Tools.vectorsListToMatrixTheta(list_matrix);
        return newtheta;
    }

    /**
     * Returns an entry of the model with the parameters requested
     *
     * @param theta
     * @return
     */
    public Entry getEntry(long startnode, long endnode, long precordtype, String pentry) {
        for (Link link : links) {
            if (link.startnode == startnode && link.endnode == endnode) {
                for (RecordType recordtype : link.getRecordtypes()) {
                    if (recordtype.getId().equals(String.valueOf(precordtype))) {
                        for (Entry entry : recordtype.getEntries()) {
                            if (entry.getId().equals(pentry)) {
                                return entry;
                            }
                        }
                    }
                }
            }
        }
        return null;
    }

    @Override
    public String toString() {
        return "CORSIM_MODEL[id=" + id + ",description =" + description + ",entriesnumber=" + entriesnumber + "]";
    }

    /**
     * Using the theta matrix given as parameter initializes the Corsim Model
     * @param theta
     */
    public void initializeWithThetaMatrix(int[][] theta,String [][]link_names) {
        //Scheme of theta   [0] Rt, [1] SC, [2] EC, [3] MinV, [4] MaxV, [5] Value,  [6] lineNumber,  [7] InNode, [8] EndNode,[9] entry
        this.setEntriesnumber(theta.length);
        for (int i = 0; i < theta.length; i++) {
            Entry entry = new Entry();
            String[] description = UTIL.Tools.getEntryDescription(String.valueOf(theta[i][0]), Long.valueOf(theta[i][1]), Long.valueOf(theta[i][2]));
            entry.setId(Long.valueOf(theta[i][1]) + "-" + Long.valueOf(theta[i][2]));
            entry.setDescription(description[1] + ": " + description[4]);
            entry.setStartcolumn(Long.valueOf(theta[i][1]));
            entry.setEndcolumn(Long.valueOf(theta[i][2]));
            entry.setMinvalue(Integer.valueOf(theta[i][3]));
            entry.setMaxvalue(Integer.valueOf(theta[i][4]));
            entry.setValue(Double.valueOf(theta[i][5]));
            entry.setLinenumber(Long.valueOf(theta[i][6]));
            Link link = this.getLink(Long.valueOf(theta[i][7]), Long.valueOf(theta[i][8]));
            if (link != null) {
                RecordType recordtype = link.getRecordType(String.valueOf(theta[i][0]));
                if (recordtype != null) {
                    recordtype.addToEntries(entry);
                } else {
                    RecordType newrecordtype = new RecordType();
                    String[] RT_desc = UTIL.Tools.getEntryDescription(String.valueOf(theta[i][0]),78,80);
                    newrecordtype.setCalibrate(true);
                    newrecordtype.setDescription(RT_desc[4]);
                    newrecordtype.setId(String.valueOf(theta[i][0]));
                    newrecordtype.setSpecial(true);
                    newrecordtype.setSpecialtype("DEF");
                    newrecordtype.addToEntries(entry);

                    link.addToRecordtypes(newrecordtype);
                }
            } else {
                //Create the link
                Link newlink = new Link();
                newlink.setCalibrate(true);
                newlink.setStartnode(Long.valueOf(theta[i][7]));
                newlink.setEndnode(Long.valueOf(theta[i][8]));
                newlink.setDescription(theta[i][7]+"-"+theta[i][8]);
                for(int j =0; j<link_names.length;j++){
                    if((UTIL.Tools.stringToIntegerAux(link_names[j][0])==theta[i][7]||UTIL.Tools.stringToIntegerAux(link_names[j][1])==theta[i][7])&& (UTIL.Tools.stringToIntegerAux(link_names[j][0])==theta[i][8]||UTIL.Tools.stringToIntegerAux(link_names[j][1])==theta[i][8])){
                        newlink.setDescription(theta[i][7]+"-"+theta[i][8]+" : "+link_names[j][2]);
                    }
                }
                if(newlink.getStartnode() == -1 && newlink.getEndnode() == -1){
                    newlink.setDescription("GLOBAL PARAMETERS");
                }

                //Create the record type
                RecordType newrecordtype = new RecordType();
                newrecordtype.setCalibrate(true);
                String[] RT_desc = UTIL.Tools.getEntryDescription(String.valueOf(theta[i][0]), Long.valueOf(78), Long.valueOf(80));
                newrecordtype.setDescription(RT_desc[4]);
                newrecordtype.setId(String.valueOf(theta[i][0]));
                newrecordtype.setSpecial(true);
                newrecordtype.setSpecialtype("DEF");
                newrecordtype.addToEntries(entry);
                newlink.addToRecordtypes(newrecordtype);

                this.addToLinks(newlink);
            }
        }
    }
}
