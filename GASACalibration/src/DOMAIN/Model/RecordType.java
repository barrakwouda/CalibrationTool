package DOMAIN.Model;

import java.io.Serializable;
import java.util.*;

/**
 * Abstracts all the necessary information for a Corsim Record Type
 * @author Carlos Gaviria, Cristian Arteaga and Victor Molano
 */
public class RecordType implements Serializable{

    private String id;
    private String description;
    private boolean special;
    private String specialtype;
    private List<Entry> entries;

    public RecordType() {
        this.entries = new ArrayList();
    }

    public void setCalibrate(boolean active) {
        for (Entry entry : entries) {
            entry.setCalibrate(active);
        }
    }

    public boolean getCalibrate() {
        for (Entry entry : entries) {
            if (entry.getCalibrate()) {
                return true;
            }
        }
        return false;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getId() {
        return this.id;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDescription() {
        return this.description;
    }

    public void setSpecial(boolean special) {
        this.special = special;
    }

    public boolean getSpecial() {
        return this.special;
    }

    public void setSpecialtype(String specialtype) {
        this.specialtype = specialtype;
    }

    public String getSpecialtype() {
        return this.specialtype;
    }

    public void setEntries(List<Entry> entries) {
        this.entries = entries;
    }

    public List<Entry> getEntries() {
        return this.entries;
    }

    public void addToEntries(Entry entry) {
        this.entries.add(entry);
    }

    public void removeFromEntries(Entry entry) {
        this.entries.remove(entry);
    }

    public String toStringCascade() {
        String cascade = this.toString();
        for (Entry entry : entries) {
            cascade += "\n" + entry.toString();
        }
        return cascade;
    }

    @Override
    public String toString() {
        return "\t\tRECORDTYPE[id=" + id + ",description =" + description + ",special=" + special + ",specialtype=" + specialtype + "]";
    }
}
