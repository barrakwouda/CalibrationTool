package DOMAIN;

/**
 * Allows to store the Actual Data Netsim values
 * @author Carlos Gaviria, Cristian Arteaga and Victor Molano
 * @layer DOMAIN
 */
public class ActualDataFresim {
    
    private String[][] ADF_Data;
    
     /**
     * Default Constructor
     */
    public ActualDataFresim(){
        
    }
    /**
     * Initialize the object
     * @param data
     */
    public ActualDataFresim(String[][] data){
        this.ADF_Data = data;
    }
    
    /**
     * Return the ADF_Data attribute
     * @return this.ADF_Data
     */
    public String[][] getADF_Data() {
        return this.ADF_Data;
    }

    /**
     * Set the ADF_Data attribute with the data bidimensional array
     * @param data
     */
    public void setADF_Data(String[][] data) {
        this.ADF_Data = data;
    }

}
