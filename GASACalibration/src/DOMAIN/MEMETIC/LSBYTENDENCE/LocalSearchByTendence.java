package DOMAIN.MEMETIC.LSBYTENDENCE;

import PERSISTENCE.Persistence;
import PERSISTENCE.ThreadCORSIM;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Implements all the necessary logic for the algorithm of local search by
 * tendency.
 *
 * @author Carlos Gaviria, Cristian Arteaga and Victor Molano
 */
public class LocalSearchByTendence {

    int q;
    IndividualLocalSearch initial;
    Persistence persistence;
    private ArrayList<IndividualLocalSearch> bestindividuals;

    /**
     * Initialize the object with the necessary data
     *
     * @param persistence
     * @param q
     * @param step
     * @param howmuchpercentaje
     * @param addsubpercentaje
     * @param fatherevaluation
     */
    public LocalSearchByTendence(Persistence persistence, int q, int step, int howmuchpercentaje, int addsubpercentaje, double fatherevaluation) {
        int[][] data = persistence.getDataMatrix();
        this.q = q;
        int[] pattern = this.generatePattern(data.length, howmuchpercentaje, addsubpercentaje, step, 1);
        initial = new IndividualLocalSearch(data, step, howmuchpercentaje, pattern, addsubpercentaje, 1, 0);
        initial.setEvaluation(fatherevaluation);
        this.persistence = persistence;
        bestindividuals = new ArrayList<IndividualLocalSearch>();
    }

    /**
     * Starts the algorithm considering that data has been previously charged
     *
     * @return
     */
    public IndividualLocalSearch run() {
        bestindividuals.add(initial);
        IndividualLocalSearch father = null;
        UTIL.Log.info("Llegue  con RMS=" + initial.getEvaluation());
        IndividualLocalSearch best = new IndividualLocalSearch(initial);
        // printMatrixToConsoleIntegerLine(initial.getData(),"Initial DATA");
        while (bestindividuals.size() > 0) {
            UTIL.Log.info("Best Individuals Size=" + bestindividuals.size());
            father = new IndividualLocalSearch(bestindividuals.get(0));
            int changeadd = 1, changesub = 1;
            boolean betteradd = false, bettersub = false;
            IndividualLocalSearch sonadd = null, sonsub = null;
            do {
                sonadd = applyAlteration(father, changeadd, 1);
                betteradd = sonadd.getEvaluation() < father.getEvaluation();
                changeadd++;
            } while (!betteradd && changeadd <= 4);

            do {
                sonsub = applyAlteration(father, changesub, -1);
                bettersub = sonsub.getEvaluation() < father.getEvaluation();
                changesub++;
            } while (!bettersub && changesub <= 4);

            if (betteradd) {
                bestindividuals.add(sonadd);
            }
            if (bettersub) {
                bestindividuals.add(sonsub);
            }
            for (IndividualLocalSearch individual : bestindividuals) {
                if (individual.getEvaluation() < best.getEvaluation()) {
                    best = new IndividualLocalSearch(individual);
                }
            }
            bestindividuals.remove(0);
        }
        // printMatrixToConsoleIntegerLine(initial.getData(),"Initial End DATA");
        //printMatrixToConsoleIntegerLine(initial.getData(),"Best End DATA");
        UTIL.Log.info("Me voy con RMS=" + best.getEvaluation());
        return best;
    }

    /**
     *
     * @param father
     * @param change
     * @param sign
     * @return
     */
    public IndividualLocalSearch applyAlteration(IndividualLocalSearch father, int change, int sign) {
        ArrayList<IndividualLocalSearch> tempindividuals = new ArrayList<IndividualLocalSearch>();
        System.out.print("\tAlter " + change + " and sign " + sign);
        for (int i = 0; i < q; i++) {
//            UTIL.Log.info("Copy "+i);
            IndividualLocalSearch copy = new IndividualLocalSearch(father);
            if (i != 0) {
//                
                switch (change) {
                    case 1: {
//                        UTIL.Log.info("Alterate who");
                        copy = alterateWho(copy, sign);
                        break;
                    }
                    case 2: {
//                        UTIL.Log.info("Alterate how much");
                        copy = alterateHowMuch(copy, sign);
                        break;
                    }
                    case 3: {
//                        UTIL.Log.info("Alterate step");
                        copy = alterateStep(copy, sign);
                        break;
                    }
                    case 4: {
//                        UTIL.Log.info("Alterate addsub");
                        copy = alterateStep(copy, sign);
                        copy = alterateAddSub(copy, sign);
                        break;
                    }

                }

            } else {
                copy.applyPattern();
            }
            copy.applyAllConstraints();
            //printMatrixToConsoleIntegerLine(copy.getData(),"AfterConstraints");
            tempindividuals.add(copy);

        }
        evaluateAll(tempindividuals);
        double bestvalue = (tempindividuals.get(0)).getEvaluation();
        int bestposition = 0;
        for (int i = 0; i < tempindividuals.size(); i++) {
            if (tempindividuals.get(i).getEvaluation() < bestvalue) {
                bestvalue = tempindividuals.get(i).getEvaluation();
                bestposition = i;
            }
        }
        UTIL.Log.info("\tLS RMS: " + bestvalue);
        return tempindividuals.get(bestposition);
    }

    /**
     * Generates an array to be used as pattern of addition and subtraction for the individuals
     * @param father
     * @param sign
     * @return
     */
    private int[] generatePattern(int n, int howmuchpercentaje, int addsubpercentaje, int step, int sign) {
        //TYPE: 0        
        int[] pattern = new int[n];
        //Generating howmuch and who
        for (int i = 0; i < n; i++) {
            double tem = Math.random();
            double tem2 = ((double) howmuchpercentaje / (double) 100);
            if (tem < tem2) {
                double tem3 = Math.random();
                double tem4 = ((double) addsubpercentaje / (double) 100);
                if (tem3 < tem4) {
                    pattern[i] = step * sign;
                } else {
                    pattern[i] = -(step * sign);
                }
            } else {
                pattern[i] = 0;
            }
        }
        return pattern;
    }

    /**
     * Decides the individuals to alterate
     * @param father
     * @param sign
     * @return
     */
    public IndividualLocalSearch alterateWho(IndividualLocalSearch father, int sign) {
        int[] tempwho = this.generatePattern(father.getData().length, father.getHowmuchpercentaje(), father.getAddsubpercentaje(), father.getStep(), sign);
        father.setWho(tempwho);
        father.applyPattern();
        return father;
    }

    /**
     * Decides how many individuals alterate
     * @param father
     * @param sign
     * @return
     */
    public IndividualLocalSearch alterateHowMuch(IndividualLocalSearch father, int sign) {
        int datalenght = father.getData().length;
        int percentajechange = 10;
        int quantitychange = ((datalenght) / 100) * percentajechange;
        int toadd = father.getHowmuchpercentaje() + quantitychange;
        if (toadd > datalenght) {
            toadd = toadd - datalenght;
        }
        int[] tempwho = this.generatePattern(father.getData().length, toadd, father.getAddsubpercentaje(), father.getStep(), sign);
        father.setWho(tempwho);
        father.applyPattern();
        return father;
    }

    /**
     * Decides the step to alterate
     * @param father
     * @param sign
     * @return
     */
    public IndividualLocalSearch alterateStep(IndividualLocalSearch father, int sign) {
        int multiplier = 1;
        int toadd = father.getStep() + (1 * multiplier);
        if (toadd > 3 * multiplier) {
            toadd = toadd - (3 * multiplier);
        }
        int[] tempwho = this.generatePattern(father.getData().length, father.getHowmuchpercentaje(), father.getAddsubpercentaje(), toadd, sign);
        father.setWho(tempwho);
        father.applyPattern();
        return father;
    }

    /**
     * Decides if the alteration is an addition or subtraction
     * @param father
     * @param sign
     * @return
     */
    public IndividualLocalSearch alterateAddSub(IndividualLocalSearch father, int sign) {
        int percentajechange = 10;
        int toadd = father.getHowmuchpercentaje() + percentajechange;
        if (toadd > 80) {
            toadd = 20 + toadd - 80;
        }
        int[] tempwho = this.generatePattern(father.getData().length, father.getHowmuchpercentaje(), toadd, father.getStep(), sign);
        father.setWho(tempwho);
        father.applyPattern();
        return father;
    }

    /**
     * Evaluate all the individuals given as parameter
     * @param individuals
     */
    public void evaluateAll(ArrayList<IndividualLocalSearch> individuals) {

        //copies of the trf
        for (int i = 0; i < individuals.size(); i++) {
            UTIL.Tools.copyFile(persistence.getPathTrf(), "LSByT-" + i + ".trf");
        }
        //creation of the new trf's files from the new individuals
        for (int i = 0; i < individuals.size(); i++) {
            PERSISTENCE.ProcessPersistence.writeParametersSet(((IndividualLocalSearch) individuals.get(i)).getData(), "LSByT-" + i + ".trf");
        }
        //simulataneus execution of CORSIM with Threads       
        ThreadCORSIM[] Threads = new ThreadCORSIM[individuals.size()];
        for (int i = 0; i < individuals.size(); i++) {
            Threads[i] = new ThreadCORSIM("LSByT-" + i + ".trf");
        }
        for (int i = 0; i < individuals.size(); i++) {
            try {
                Threads[i].t.join();
            } catch (InterruptedException ex) {
                Logger.getLogger(LocalSearchByTendence.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        //evaluation of each new individual
        for (int i = 0; i < individuals.size(); i++) {
            int count_netsim = PERSISTENCE.ProcessPersistence.countDataNetsim("LSByT-" + i + ".out");
            int count_fresim = PERSISTENCE.ProcessPersistence.countDataFresim("LSByT-" + i + ".out");
            int count_real_netsim = (persistence.getActual_Data_Netsim().getADN_Data()).length;
            int count_real_fresim = (persistence.getActual_Data_Fresim().getADF_Data()).length;

            double[][] output_real = UTIL.Tools.addMatrix(UTIL.Tools.stringMatrixToDoubleMatrix(persistence.getActual_Data_Netsim().getADN_Data()), UTIL.Tools.stringMatrixToDoubleMatrix(persistence.getActual_Data_Fresim().getADF_Data()), count_real_netsim, count_real_fresim);
            double[][] output_model = UTIL.Tools.addMatrix(PERSISTENCE.ProcessPersistence.extractDataNetsim("LSByT-" + i + ".out", count_netsim), PERSISTENCE.ProcessPersistence.extractDataFresim("LSByT-" + i + ".out", count_fresim), count_real_netsim, count_real_fresim);

            double rms = NRMS(output_real, output_model, persistence.getPercentageVolume());
            ((IndividualLocalSearch) (individuals.get(i))).setEvaluation(rms);

        }

    }

    /**
     * Calculates the Normalized Root Mean Square for Volumes and Speeds for all
     * available data
     *
     * @param double[][] output_real, double[][] output_model,double percentage
     * @return double[] theta_minus
     * @author Carlos Gaviria, Victor Molano and Cristian Arteaga
     */
    private double NRMS(double[][] output_real, double[][] output_model, double percentage) {

        double aux_volume[] = new double[output_model.length];
        double aux_speed[] = new double[output_model.length];
        double percentage_volume = percentage / 100;
        int n_links_volume = 0;
        int n_links_speed = 0;

        double sum_errors_volume = 0;
        double sum_errors_speed = 0;
        double rms = 0;

        //UTIL.Log.info( "Model = " +output_model.length );
        //UTIL.Log.info("Real = " +output_real.length);
        for (int i = 0; i < output_model.length; i++) {

            if (output_real[i][2] == 0) {
                aux_volume[i] = 1;
                n_links_volume++;
            } else if (output_real[i][2] == -1) {
                aux_volume[i] = 0;
            } else if (output_real[i][2] == 0 && output_model[i][2] == 0) {
                aux_volume[i] = 0;
                n_links_volume++;
            } else {
                aux_volume[i] = Math.pow((output_model[i][2] - output_real[i][2]) / output_real[i][2], 2);
                n_links_volume++;
            }

            if (output_real[i][3] == 0) {
                aux_speed[i] = 1;
                n_links_speed++;
            } else if (output_real[i][3] == -1) {
                aux_speed[i] = 0;
            } else if (output_real[i][3] == 0 && output_model[i][3] == 0) {
                aux_speed[i] = 0;
                n_links_speed++;
            } else {
                aux_speed[i] = Math.pow((output_model[i][3] - output_real[i][3]) / output_real[i][3], 2);
                n_links_speed++;
            }
        }
        for (int i = 0; i < aux_volume.length; i++) {
            sum_errors_volume = sum_errors_volume + aux_volume[i];
        }

        for (int i = 0; i < aux_speed.length; i++) {
            sum_errors_speed = sum_errors_speed + aux_speed[i];
        }
        rms = (percentage_volume * Math.sqrt(sum_errors_volume / n_links_volume)) + (1 - percentage_volume) * Math.sqrt(sum_errors_speed / n_links_speed);

        //UTIL.Log.info("RMS=" + rms);
        return rms;
    }
}
