package DOMAIN.MEMETIC.LSBYTENDENCE;

/**
 * Abstracts and individual to be used exclusively with the algorithm
 * of local search by tendency
 *
 * @author Carlos Gaviria, Cristian Arteaga and Victor Molano
 */
public class IndividualLocalSearch {

    private int[][] data;
    private int step;
    private int howmuchpercentaje;
    private int[] who;
    private int addsubpercentaje;
    private int suboradd;
    private double evaluation;

    /**
     *
     * @param father
     */
    public IndividualLocalSearch(IndividualLocalSearch father) {
        this.data = UTIL.Tools.copyMatrixInt(father.getData());
        this.step = father.getStep();
        this.howmuchpercentaje = father.getHowmuchpercentaje();
        this.who = UTIL.Tools.copyArrayInt(father.getWho());
        this.addsubpercentaje = father.getAddsubpercentaje();
        this.suboradd = father.getSuboradd();
        this.evaluation = father.getEvaluation();
    }

    /**
     *
     * @param data
     * @param step
     * @param howmuchpercentaje
     * @param who
     * @param addsubpercentaje
     * @param suboradd
     * @param evaluation
     */
    public IndividualLocalSearch(int[][] data, int step, int howmuchpercentaje, int[] who, int addsubpercentaje, int suboradd, double evaluation) {
        this.data = data;
        this.step = step;
        this.howmuchpercentaje = howmuchpercentaje;
        this.who = who;
        this.addsubpercentaje = addsubpercentaje;
        this.suboradd = suboradd;
        this.evaluation = evaluation;
    }

    /**
     * @return the data
     */
    public int[][] getData() {
        return data;
    }

    /**
     * @param data the data to set
     */
    public void setData(int[][] data) {
        this.data = data;
    }

    /**
     * @return the step
     */
    public int getStep() {
        return step;
    }

    /**
     * @param step the step to set
     */
    public void setStep(int step) {
        this.step = step;
    }

    /**
     * @return the howmuchpercentaje
     */
    public int getHowmuchpercentaje() {
        return howmuchpercentaje;
    }

    /**
     * @param howmuchpercentaje the howmuchpercentaje to set
     */
    public void setHowmuchpercentaje(int howmuchpercentaje) {
        this.howmuchpercentaje = howmuchpercentaje;
    }

    /**
     * @return the who
     */
    public int[] getWho() {
        return who;
    }

    /**
     * @param who the who to set
     */
    public void setWho(int[] who) {
        this.who = who;
    }

    /**
     * @return the addsubpercentaje
     */
    public int getAddsubpercentaje() {
        return addsubpercentaje;
    }

    /**
     * @param addsubpercentaje the addsubpercentaje to set
     */
    public void setAddsubpercentaje(int addsubpercentaje) {
        this.addsubpercentaje = addsubpercentaje;
    }

    /**
     * @return the suboradd
     */
    public int getSuboradd() {
        return suboradd;
    }

    /**
     * @param suboradd the suboradd to set
     */
    public void setSuboradd(int suboradd) {
        this.suboradd = suboradd;
    }

    /**
     * @return the evaluation
     */
    public double getEvaluation() {
        return evaluation;
    }

    /**
     * @param evaluation the evaluation to set
     */
    public void setEvaluation(double evaluation) {
        this.evaluation = evaluation;
    }

    /**
     *
     */
    public void applyPattern() {
        for (int i = 0; i < data.length; i++) {
            data[i][5] = data[i][5] + who[i];
        }
        applyAllConstraints();
    }

    /**
     *
     */
    public void applyAllConstraints() {
        data = UTIL.Tools.applyConstraints(UTIL.Tools.copyMatrixIntDouble(data));
        data = UTIL.Tools.applyRTContraints(data);

    }
}
