package DOMAIN.MEMETIC;

import DOMAIN.Graph;
import DOMAIN.MEMETIC.GENETIC.GeneticAlgorithm;
import DOMAIN.MEMETIC.GENETIC.IndividualGA;
import PERSISTENCE.Persistence;
import UTIL.Tools;
import java.util.HashMap;
import java.util.Map;

/**
 * Implements all the necessary logic to make a calibration with a memetic algorithm
 * @author Carlos Gaviria, Cristian Arteaga and Victor Molano
 */
public class MemeticAlgorithm {

    Map<String, Graph> graphs;
    Persistence persistence;

    /**
     *
     * @param persistence
     */
    public MemeticAlgorithm(Persistence persistence) {
        graphs = new HashMap<String, Graph>();
        this.persistence = persistence;
    }

    /**
     * Execute the calibration using a Genetic algorithm with a Simulated
     * Annealing algorithm
     *
     * @param Pop
     * @param Gen
     * @param CrossPer
     * @param SelPer
     * @param Step
     * @param Fat
     * @param MutPer
     * @param IniTemp
     * @param CoolRate
     */
    public void runGASA(int Pop, int Gen, int CrossPer, int SelPer, int Step, int Fat, int MutPer, double IniTemp, double CoolRate) {
        IndividualGA best = new IndividualGA();
        IndividualGA initial = new IndividualGA();
        initial.setGenes(persistence.getDataMatrix());
        int npoprestart = 1;
        for (int i = 0; i < npoprestart; i++) {
            persistence.writeDataMatrix(initial.getGenes());
            UTIL.Log.info("Start time " + Tools.actualTimestamp("dd-MMM-yyyy h:mm:ss"));
            GeneticAlgorithm genetic = new GeneticAlgorithm(persistence, Gen, Pop, Fat, SelPer, CrossPer, MutPer, Step);
            IndividualGA bestgenetic = genetic.runWithSA(IniTemp, CoolRate);

            UTIL.Log.info("End time " + Tools.actualTimestamp("dd-MMM-yyyy h:mm:ss"));
            if (bestgenetic.getEvaluation() < best.getEvaluation()) {
                best = new IndividualGA(bestgenetic);
            }
        }
        UTIL.Log.info("Best RMS=" + best.getEvaluation());
        persistence.writeDataMatrix(best.getGenes());
    }

    /**
     * Execute the calibration using a Genetic algorithm with a Tabu search
     * algorithm
     *
     * @param Pop
     * @param Gen
     * @param CrossPer
     * @param SelPer
     * @param Step
     * @param Fat
     * @param MutPer
     * @param Tenure
     * @param NeighSize
     */
    public void runGATS(int Pop, int Gen, int CrossPer, int SelPer, int Step, int Fat, int MutPer, int Tenure, int NeighSize) {
        IndividualGA best = new IndividualGA();
        IndividualGA initial = new IndividualGA();
        initial.setGenes(persistence.getDataMatrix());
        int npoprestart = 1;
        for (int i = 0; i < npoprestart; i++) {
            persistence.writeDataMatrix(initial.getGenes());
            UTIL.Log.info("Start time " + Tools.actualTimestamp("dd-MMM-yyyy h:mm:ss"));
            GeneticAlgorithm genetic = new GeneticAlgorithm(persistence, Gen, Pop, Fat, SelPer, CrossPer, MutPer, Step);
            IndividualGA bestgenetic = genetic.runWithTS(Tenure, NeighSize);

            UTIL.Log.info("End time " + Tools.actualTimestamp("dd-MMM-yyyy h:mm:ss"));
            if (bestgenetic.getEvaluation() < best.getEvaluation()) {
                best = new IndividualGA(bestgenetic);
            }
        }
        UTIL.Log.info("Best RMS=" + best.getEvaluation());
        persistence.writeDataMatrix(best.getGenes());
    }

    /**
     * Execute the calibration using a genetic algorithm
     * @param Pop
     * @param Gen
     * @param CrossPer
     * @param SelPer
     * @param Step
     * @param Fat
     * @param MutPer
     */
    public void runGA(int Pop, int Gen, int CrossPer, int SelPer, int Step, int Fat, int MutPer) {
        IndividualGA best = new IndividualGA();
        IndividualGA initial = new IndividualGA();
        initial.setGenes(persistence.getDataMatrix());
        int npoprestart = 1;
        for (int i = 0; i < npoprestart; i++) {
            persistence.writeDataMatrix(initial.getGenes());
            UTIL.Log.info("Start time " + Tools.actualTimestamp("dd-MMM-yyyy h:mm:ss"));
            GeneticAlgorithm genetic = new GeneticAlgorithm(persistence, (int) Gen, (int) Pop, (int) Fat, (int) SelPer, (int) CrossPer, (int) MutPer, (int) Step);
            IndividualGA bestgenetic = genetic.runGA();
//            persistence.writeDataMatrix(bestgenetic.getGenes());
            UTIL.Log.info("End time " + Tools.actualTimestamp("dd-MMM-yyyy h:mm:ss"));
            if (bestgenetic.getEvaluation() < best.getEvaluation()) {
                best = new IndividualGA(bestgenetic);
            }
        }
        UTIL.Log.info("Best RMS=" + best.getEvaluation());
        persistence.writeDataMatrix(best.getGenes());
    }
}
