package DOMAIN.MEMETIC.GENETIC;

import DOMAIN.MEMETIC.SIMULATEDANNEALING.IndividualSA;
import DOMAIN.MEMETIC.SIMULATEDANNEALING.SimulatedAnnealing;
import DOMAIN.MEMETIC.TABUSEARCH.IndividualTS;
import DOMAIN.MEMETIC.TABUSEARCH.TabuSearch;
import PERSISTENCE.Persistence;
import PERSISTENCE.ThreadCORSIM;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Contains all the necessary logic to implement an genetic algorithm
 * for the calibration problem, each of the attributes are the parameters used
 * in a traditional algorithm of this type
 *
 * @author Carlos Gaviria, Cristian Arteaga and Victor Molano
 */
public class GeneticAlgorithm {

    private int population_size;//multiplier of (fathersnum * 2)
    private int ngenerations;
    private int selectionpercentaje;
    private int fathersnum;
    private int crosspercentaje;
    private int mutationpercentaje;
    private int step;
    private IndividualGA initial;
    private ArrayList population;
    private Persistence persistence;

    /**
     * Starts the calibration using the Tabu search algorithm as exploitation
     * algorithm
     *
     * @param Tenure
     * @param NeighSize
     * @return
     */
    public IndividualGA runWithTS(int Tenure, int NeighSize) {
        int count = 0, previoussize, k = 0, kmax = 200, i = 0;
        UTIL.Log.info("GENETIC ALGORITHM SETTINGS: ");
        UTIL.Log.info("Individual size       = " + initial.size());
        UTIL.Log.info("Population size       = " + population_size);
        UTIL.Log.info("Number of generations = " + ngenerations);
        UTIL.Log.info("Number of parents     = " + fathersnum);
        UTIL.Log.info("Selection percentaje  = " + selectionpercentaje);
        UTIL.Log.info("Cross percentaje      = " + crosspercentaje);
        UTIL.Log.info("Mutation percentaje   = " + mutationpercentaje);
        UTIL.Log.info("Step                  = " + step);
        UTIL.Log.info("");
        UTIL.Log.info("TABU SEARCH ALGORITHM SETTINGS: ");
        UTIL.Log.info("Tenure                = " + Tenure);
        UTIL.Log.info("Neighborhood size     = " + NeighSize);
        IndividualGA best = new IndividualGA(initial);
        generateGEHData(best);
        best.setEvaluation(evaluate(best));
        UTIL.Log.info("GA-RMS=" + best.getEvaluation());
        generateInitialPopulation();
        evaluation(0, population_size - 1);
//        Collections.reverse(population);
        population.add(best);
        sort();
        population.remove(population.size() - 1);
        while (i < ngenerations && k < kmax) {
            ArrayList selecteds = selection();
            ArrayList crosseds = crossing(selecteds);
            ArrayList mutateds = mutation(crosseds);
            previoussize = population.size();
            population.addAll(mutateds);
            evaluation(previoussize, population.size() - 1);
            sort();
            remove();
            //If a new improvement was found, print the new rms
            if (((IndividualGA) (population.get(0))).getEvaluation() < best.getEvaluation()) {
//                generateGEHData((IndividualGA) (population.get(0)));
                best = new IndividualGA((IndividualGA) (population.get(0)));
                k = 0;

            } else {
                k++;
            }
            UTIL.Log.info("GA-RMS=" + ((IndividualGA) (population.get(0))).getEvaluation());
            best = new IndividualGA((IndividualGA) (population.get(0)));
            TabuSearch ts = new TabuSearch(best.getGenes(), persistence, Tenure, 70, NeighSize);
            IndividualTS bestTS = new IndividualTS(ts.run());
            best.setGenes(bestTS.getData());
            best.setEvaluation(bestTS.getEvaluation());
            if (population.size() > 0) {
                population.remove(0);
            }
            population.add(0, best);
            i++;
        }
        generateGEHData(best);
        return best;
    }

    /**
     * Starts the calibration using the simulated annealing algorithm as
     * exploitation algorithm
     *
     * @param IniTemp
     * @param CoolRate
     * @return
     */
    public IndividualGA runWithSA(double IniTemp, double CoolRate) {
        int count = 0, previoussize, i = 0, k = 0, kmax = 200;
        UTIL.Log.info("GENETIC ALGORITHM SETTINGS: ");
        UTIL.Log.info("Individual size       = " + initial.size());
        UTIL.Log.info("Population size       = " + population_size);
        UTIL.Log.info("Number of generations = " + ngenerations);
        UTIL.Log.info("Number of parents     = " + fathersnum);
        UTIL.Log.info("Selection percentaje  = " + selectionpercentaje);
        UTIL.Log.info("Cross percentaje      = " + crosspercentaje);
        UTIL.Log.info("Mutation percentaje   = " + mutationpercentaje);
        UTIL.Log.info("Step                  = " + step);
        UTIL.Log.info("");
        UTIL.Log.info("SIMULATED ANNEALING ALGORITHM SETTINGS: ");
        UTIL.Log.info("Initial temperature   = " + IniTemp);
        UTIL.Log.info("Cooling rate          = " + CoolRate);
        UTIL.Log.info("");
        IndividualGA best = new IndividualGA(initial);
        generateGEHData(best);
        
        best.setEvaluation(evaluate(best));
        
        UTIL.Log.info("GA-RMS=" + best.getEvaluation());
        generateInitialPopulation();
        evaluation(0, population_size - 1);
//        Collections.reverse(population);
        population.add(best);
        sort();
        population.remove(population.size() - 1);
        while (i < ngenerations && k < kmax) {
            ArrayList selecteds = selection();
            ArrayList crosseds = crossing(selecteds);
            ArrayList mutateds = mutation(crosseds);
            previoussize = population.size();
            population.addAll(mutateds);
            evaluation(previoussize, population.size() - 1);
            sort();
            remove();
            if (((IndividualGA) (population.get(0))).getEvaluation() < best.getEvaluation()) {
                best = new IndividualGA((IndividualGA) (population.get(0)));
                k = 0;
            } else {
                k++;
            }
            UTIL.Log.info("GA-RMS=" + ((IndividualGA) (population.get(0))).getEvaluation());
            best = new IndividualGA((IndividualGA) (population.get(0)));
            SimulatedAnnealing sa = new SimulatedAnnealing(best.getGenes(), persistence, IniTemp, CoolRate);
            IndividualSA bestSA = new IndividualSA(sa.run());
            best.setGenes(bestSA.getData());
            best.setEvaluation(bestSA.getEvaluation());
            if (population.size() > 0) {
                population.remove(0);
            }
            population.add(0, best);
            i++;
        }
        generateGEHData(best);
        return best;
    }

    /**
     * Starts the calibration without exploitation algorithm
     *
     * @return
     */
    public IndividualGA runGA() {
        int count = 0, previoussize, i = 0, k = 0, kmax = 200;
        UTIL.Log.info("GENETIC ALGORITHM SETTINGS: ");
        UTIL.Log.info("Individual size       = " + initial.size());
        UTIL.Log.info("Population size       = " + population_size);
        UTIL.Log.info("Number of generations = " + ngenerations);
        UTIL.Log.info("Number of parents     = " + fathersnum);
        UTIL.Log.info("Selection percentaje  = " + selectionpercentaje);
        UTIL.Log.info("Cross percentaje      = " + crosspercentaje);
        UTIL.Log.info("Mutation percentaje   = " + mutationpercentaje);
        UTIL.Log.info("Step                  = " + step);
        UTIL.Log.info("");
        IndividualGA best = new IndividualGA(initial);
        generateGEHData(best);
        best.setEvaluation(evaluate(best));
        UTIL.Log.info("GA-RMS=" + best.getEvaluation());
        generateInitialPopulation();
        evaluation(0, population_size - 1);
//        Collections.reverse(population);
        population.add(best);
        sort();
        population.remove(population.size() - 1);
        while (i < ngenerations && k < kmax) {
            ArrayList selecteds = selection();
            ArrayList crosseds = crossing(selecteds);
            ArrayList mutateds = mutation(crosseds);
            previoussize = population.size();
            population.addAll(mutateds);
            evaluation(previoussize, population.size() - 1);
            sort();
            remove();
            if (((IndividualGA) (population.get(0))).getEvaluation() < best.getEvaluation()) {
                best = new IndividualGA((IndividualGA) (population.get(0)));
                k = 0;
            } else {
                k++;
            }
            UTIL.Log.info("GA-RMS=" + ((IndividualGA) (population.get(0))).getEvaluation());
            best = new IndividualGA((IndividualGA) (population.get(0)));
            if (population.size() > 0) {
                population.remove(0);
            }
            population.add(0, best);
            i++;
        }
        generateGEHData(best);
        return best;
    }

    /**
     * This class randomly generates an initial population using the initial
     * individual attribute
     */
    public void generateInitialPopulation() {
        for (int i = 0; i < population_size; i++) {
            IndividualGA temp = initial.generateRandomInitial();
            temp.applyAllConstraints();
            population.add(temp);
        }
    }

    /**
     * Using a tournament selection technique this method sets into the
     * population attribute the selected individuals to process
     *
     * @return
     */
    public ArrayList selection() {
        ArrayList temp = new ArrayList();
        int acceptation = (int) (((double) population.size() * (double) selectionpercentaje) / 100.0);
        for (int i = 0; i < fathersnum; i++) {
            int newa = UTIL.Tools.random(0, acceptation - 1);
//            int newb = UTIL.Tools.random(0, acceptation);
//            int newInd = Math.max(newa, newb);
            IndividualGA In = new IndividualGA((IndividualGA) population.get(newa));
            IndividualGA In2 = new IndividualGA((IndividualGA) population.get(newa + 1));
            temp.add(In);
            temp.add(In2);
        }
        return temp;
    }

    /**
     * Cross the selected individuals in the input parameter list
     *
     * @param input
     * @return
     */
    public ArrayList crossing(ArrayList input) {
        ArrayList temporal = new ArrayList();
        IndividualGA newson;
        IndividualGA temp;
        for (int i = 0; i < input.size(); i = i + 2) {
            double tem = Math.random();
            double tem2 = ((double) crosspercentaje / (double) 100);
            if (tem < tem2) {
                temp = (IndividualGA) input.get(i);
                newson = temp.cross(((IndividualGA) input.get(i + 1)));
                temporal.add(temp);
                temporal.add(newson);
            } else {
                temp = (IndividualGA) input.get(i);
                newson = (IndividualGA) input.get(i + 1);
                temporal.add(temp);
                temporal.add(newson);
            }
        }
        return temporal;
    }

    /**
     * Mutate the individuals in the input parameter list.
     *
     * @param input
     * @return
     */
    public ArrayList mutation(ArrayList input) {
        ArrayList temp = new ArrayList();
        for (int i = 0; i < (input.size()); i++) {
            ((IndividualGA) (input.get(i))).mutate(step, mutationpercentaje);
            ((IndividualGA) (input.get(i))).applyAllConstraints();
            temp.add(input.get(i));

        }
        return temp;
    }

    /**
     * Evaluates the individuals in the population list between the start and
     * end individuals
     *
     * @param start
     * @param end
     */
    public void evaluation(int start, int end) {
        //Copies of the trf
        for (int i = start; i < end + 1; i++) {
            UTIL.Tools.copyFile(persistence.getPathTrf(), i + ".trf");
        }
        int count = 0;
        //Creation of the new trf's files from the new individuals
        for (int i = start; i < end + 1; i++) {
            PERSISTENCE.ProcessPersistence.writeParametersSet(((IndividualGA) population.get(i)).getGenes(), i + ".trf");
        }
        //Simulataneous execution of CORSIM with Threads       
        int total_individuals = end - start + 1;
        ThreadCORSIM[] Threads = new ThreadCORSIM[total_individuals];

        int counter = start;

        for (int i = 0; i < total_individuals; i++) {
            Threads[i] = new ThreadCORSIM(counter + ".trf");
            counter++;
        }

        for (int i = 0; i < total_individuals; i++) {
            try {
                Threads[i].t.join();
            } catch (InterruptedException ex) {
                Logger.getLogger(GeneticAlgorithm.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

        //Evaluation of each new individual
        for (int i = start; i < end + 1; i++) {
            int count_netsim = PERSISTENCE.ProcessPersistence.countDataNetsim(i + ".out");
            int count_fresim = PERSISTENCE.ProcessPersistence.countDataFresim(i + ".out");
            int count_real_netsim = (persistence.getActual_Data_Netsim().getADN_Data()).length;
            int count_real_fresim = (persistence.getActual_Data_Fresim().getADF_Data()).length;
            System.err.println("evaluation i = "+i);

            double[][] output_real = UTIL.Tools.addMatrix(UTIL.Tools.stringMatrixToDoubleMatrix(persistence.getActual_Data_Netsim().getADN_Data()), UTIL.Tools.stringMatrixToDoubleMatrix(persistence.getActual_Data_Fresim().getADF_Data()), count_real_netsim, count_real_fresim);
            double[][] output_model = UTIL.Tools.addMatrix(PERSISTENCE.ProcessPersistence.extractDataNetsim(i + ".out", count_netsim), PERSISTENCE.ProcessPersistence.extractDataFresim(i + ".out", count_fresim), count_real_netsim, count_real_fresim);
           
            double rms = UTIL.Tools.NRMS(output_real, output_model, persistence.getPercentageVolume());
            ((IndividualGA) (population.get(i))).setEvaluation(rms);
        }

    }

    /**
     * Generate the GEH data for the individual given as parameter
     * @param toevaluate
     */
    public void generateGEHData(IndividualGA toevaluate) {
        PERSISTENCE.ProcessPersistence.writeParametersSet(toevaluate.getGenes(), persistence.getPathTrf());
        UTIL.Tools.executeCorsim(persistence.getPathTrf());
        String path_out_model = UTIL.Tools.removeExtension(persistence.getPathTrf()) + ".out";
        int count_netsim = PERSISTENCE.ProcessPersistence.countDataNetsim(path_out_model);
        
        int count_fresim = PERSISTENCE.ProcessPersistence.countDataFresim(path_out_model);
       
        int count_real_netsim = (persistence.getActual_Data_Netsim().getADN_Data()).length;
        int count_real_fresim = (persistence.getActual_Data_Fresim().getADF_Data()).length;

        double[][] output_real = UTIL.Tools.addMatrix(UTIL.Tools.stringMatrixToDoubleMatrix(persistence.getActual_Data_Netsim().getADN_Data()), UTIL.Tools.stringMatrixToDoubleMatrix(persistence.getActual_Data_Fresim().getADF_Data()), count_real_netsim, count_real_fresim);
        double[][] output_model = UTIL.Tools.addMatrix(PERSISTENCE.ProcessPersistence.extractDataNetsim(path_out_model, count_netsim), PERSISTENCE.ProcessPersistence.extractDataFresim(path_out_model, count_fresim), count_real_netsim, count_real_fresim);
        
        double rms = UTIL.Tools.NRMS(output_real, output_model, persistence.getPercentageVolume());
        double[][] dataGEH = UTIL.Tools.setGEHGraphPrintConsole(output_real, output_model);
        UTIL.Tools.printMatrixToFile(dataGEH, UTIL.Log.getFileoutgeh(), String.valueOf(rms));
        double[][] dataVol = UTIL.Tools.setGraphics(output_real, output_model, "Volume");
        double[][] dataSpe = UTIL.Tools.setGraphics(output_real, output_model, "Speed");
        UTIL.Tools.printMatrixToFile(dataVol, UTIL.Log.getFileoutvolspeed(), "Volume - " + String.valueOf(rms));
        UTIL.Tools.printMatrixToFile(dataSpe, UTIL.Log.getFileoutvolspeed(), "Speed - " + String.valueOf(rms));

    }

    /**
     * In this version this function is not necessary
     * @return
     */
    public boolean convergenceCondition() {

        return true;
    }

    /**
     * Initializes the class with the necessary values.
     * @param persistence
     * @param ngenerations
     * @param population_size
     * @param fathersnum
     * @param selectionpercentaje
     * @param crosspercentaje
     * @param mutationpercentaje
     * @param step
     */
    public GeneticAlgorithm(Persistence persistence, int ngenerations, int population_size, int fathersnum, int selectionpercentaje, int crosspercentaje, int mutationpercentaje, int step) {
        this.population_size = population_size;
        initial = new IndividualGA();
        population = new ArrayList();
        this.fathersnum = fathersnum;
        this.ngenerations = ngenerations;
        this.selectionpercentaje = selectionpercentaje;
        this.crosspercentaje = crosspercentaje;
        this.mutationpercentaje = mutationpercentaje;
        this.persistence = persistence;
        this.step = step;
        initial.setGenes(persistence.getDataMatrix());

    }

    /**
     * Sort in an ascendent way the population list
     */
    public void sort() {
        ArrayList temp = new ArrayList(population);
        population.clear();
        double min;
        int minpos, tempsize = temp.size();
        for (int i = 0; i < tempsize; i++) {
            min = ((IndividualGA) (temp.get(0))).getEvaluation();
            minpos = 0;
            for (int j = 1; j < temp.size(); j++) {
                if (((IndividualGA) (temp.get(j))).getEvaluation() <= min ) {
                    min = ((IndividualGA) (temp.get(j))).getEvaluation();
                    minpos = j;
                }
            }
            population.add(temp.get(minpos));
            temp.remove(minpos);
        }
    }

    /**
     * Remove some elements of the population list
     */
    public void remove() {
        int min = population.size() - (fathersnum * 2);
        for (int i = population.size() - 1; i >= min; i--) {
            population.remove(i);
        }
    }

    /**
     * Evaluate individually an the given parameter
     * @param toevaluate
     * @return
     */
    public double evaluate(IndividualGA toevaluate) {
        PERSISTENCE.ProcessPersistence.writeParametersSet(toevaluate.getGenes(), persistence.getPathTrf());
        UTIL.Tools.executeCorsim(persistence.getPathTrf());
        String path_out_model = UTIL.Tools.removeExtension(persistence.getPathTrf()) + ".out";
        int count_netsim = PERSISTENCE.ProcessPersistence.countDataNetsim(path_out_model);
        int count_fresim = PERSISTENCE.ProcessPersistence.countDataFresim(path_out_model);
        int count_real_netsim = (persistence.getActual_Data_Netsim().getADN_Data()).length;
        int count_real_fresim = (persistence.getActual_Data_Fresim().getADF_Data()).length;

        double[][] output_real = UTIL.Tools.addMatrix(UTIL.Tools.stringMatrixToDoubleMatrix(persistence.getActual_Data_Netsim().getADN_Data()), UTIL.Tools.stringMatrixToDoubleMatrix(persistence.getActual_Data_Fresim().getADF_Data()), count_real_netsim, count_real_fresim);
        double[][] output_model = UTIL.Tools.addMatrix(PERSISTENCE.ProcessPersistence.extractDataNetsim(path_out_model, count_netsim), PERSISTENCE.ProcessPersistence.extractDataFresim(path_out_model, count_fresim), count_real_netsim, count_real_fresim);
        double rms = UTIL.Tools.NRMS(output_real, output_model, persistence.getPercentageVolume());
        return rms;
    }
}
