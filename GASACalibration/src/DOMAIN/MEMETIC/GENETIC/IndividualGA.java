package DOMAIN.MEMETIC.GENETIC;

/**
 * Abstracts an individual to be used exclusively with the genetic
 * algorithm
 *
 * @author Carlos Gaviria, Cristian Arteaga and Victor Molano
 */
public class IndividualGA {

    private double evaluation;
    private int[][] genes;

    public double evaluate() {
        return getEvaluation();
    }

    public IndividualGA(IndividualGA father) {
        this.genes = UTIL.Tools.copyMatrixInt(father.getGenes());
        this.evaluation = father.getEvaluation();
    }

    IndividualGA(double evaluation, int[][] genes) {
        this.evaluation = evaluation;
        this.genes = UTIL.Tools.copyMatrixInt(genes);

    }

    public IndividualGA() {
        this.evaluation = 1000; //Set the initial evaluation as high

    }

    /**
     * Mutate the data in the genes attribute
     *
     * @param changepercentage
     * @param mutationpercentaje
     */
    public void mutate(float changepercentage, int mutationpercentaje) {
        int one = 1;

        double some = (double) mutationpercentaje / 100.0;
        for (int i = 0; i < genes.length; i++) {
            if (Math.random() < some) {
                double sign = Math.random();
                if (sign < 0.5) {
                    one = -1;
                } else {
                    one = 1;
                }
                genes[i][5] = genes[i][5] + (one * UTIL.Tools.calculateAlteration(genes[i][3], genes[i][4], changepercentage));
            }
        }

    }

    /**
     * Cross the data in the genes attribute
     *
     * @param tocross
     * @return
     */
    public IndividualGA cross(IndividualGA tocross) {
        int[][] genes2 = tocross.getGenes();
        int num;
        int[] mask = this.generateMask(genes.length, 50);

        for (int i = 0; i < genes.length; i++) {
            if (mask[i] == 1) {
                num = genes[i][5];
                genes[i][5] = genes2[i][5];
                genes2[i][5] = num;
            }

        }
        tocross.setGenes(genes2);

        return tocross;

    }

    /**
     * Randomly generates a new individual based in the actual, this method
     * verify the max and min value
     *
     * @return
     */
    public IndividualGA generateRandomInitial() {
        int temp[][] = UTIL.Tools.copyMatrixInt(genes);
        for (int i = 0; i < getGenes().length; i++) {
            temp[i][5] = UTIL.Tools.random(getGenes()[i][3], getGenes()[i][4]);
        }
        IndividualGA random = new IndividualGA();
        random.setGenes(temp);
        return random;
    }

    public double getEvaluation() {
        return evaluation;
    }

    public void setEvaluation(double evaluation) {
        this.evaluation = evaluation;
    }

    public int[][] getGenes() {
        return genes;
    }

    public void setGenes(int[][] genes) {
        this.genes = UTIL.Tools.copyMatrixInt(genes);
    }

    public void applyAllConstraints() {
        genes = UTIL.Tools.applyConstraints(UTIL.Tools.copyMatrixIntDouble(genes));
        genes = UTIL.Tools.applyRTContraints(genes);
    }

    public long size() {
        return genes.length;
    }

    /**
     * Calculates the alteration for the mutation process
     *
     * @param min
     * @param max
     * @param changepercentage
     * @return
     */
    public int calculateAlteration(int min, int max, float changepercentage) {
        int alteration = 0;
        int range = max - min;
        if (range > 0) {
            if (range <= 10) {
                alteration = 1;
            } else {
                alteration = (int) Math.round((float) range * (float) changepercentage / 100.0);
            }

        } else {
            UTIL.Log.error("Wrong max and min values");
        }
        return alteration;
    }

    /**
     * Important method which generates an array with the values to be added or
     * subtracted in the mutation process
     *
     * @param n
     * @param howmuchpercentaje
     * @return
     */
    private int[] generateMask(int n, int howmuchpercentaje) {
        //TYPE: 0        
        int[] pattern = new int[n];
        //Generating howmuch and who
        for (int i = 0; i < n; i++) {
            double tem = Math.random();
            double tem2 = ((double) howmuchpercentaje / (double) 100);
            if (tem < tem2) {
                pattern[i] = 1;
            } else {
                pattern[i] = 0;
            }
        }
        return pattern;
    }
}
