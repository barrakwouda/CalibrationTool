package DOMAIN.MEMETIC.TABUSEARCH;

/**
 * Abstracts the individual exclusively used for tabu search
 * algorithm
 *
 * @author Carlos Gaviria, Cristian Arteaga and Victor Molano
 */
public class IndividualTS {

    private int[][] data;
    private int tenure;
    private double evaluation;

    public IndividualTS(int[][] data, double evaluation, int tenure) {
        this.data = UTIL.Tools.copyMatrixInt(data);
        this.tenure = tenure;
        this.evaluation = evaluation;
    }

    public IndividualTS(IndividualTS father) {
        this.data = UTIL.Tools.copyMatrixInt(father.getData());
        this.evaluation = father.getEvaluation();
        this.tenure = father.getTenure();
    }

    public IndividualTS() {
    }

    /**
     * Check the actual individual is equal to that given as parameter
     *
     * @param other
     * @return
     */
    public boolean isEqual(IndividualTS other) {
        boolean equal = false;
        int[][] other_data = other.getData();
        int counter = 0;
        for (int i = 0; i < data.length; i++) {
            if (data[i][5] == other_data[i][5]) {
                counter++;
            }
        }
        equal = (data.length == counter);
        return equal;
    }

    /**
     * Important and characteristic method of tabu search algorithm which
     * alterate the data attribute to generate a neighbor
     */
    public void alterate() {
        int howmuchpercentaje = 30;
        int addsubpercentaje = 50;
        int changepercentage = 10;
        int n = data.length;
        int[] pattern = new int[n];
        //Generating howmuch and who
        for (int i = 0; i < n; i++) {
            int step = (UTIL.Tools.calculateAlteration(data[i][3], data[i][4], changepercentage));
            double tem = Math.random();
            double tem2 = ((double) howmuchpercentaje / (double) 100);
            if (tem < tem2) {
                double tem3 = Math.random();
                double tem4 = ((double) addsubpercentaje / (double) 100);
                if (tem3 < tem4) {
                    pattern[i] = step * 1;
                } else {
                    pattern[i] = -(step * 1);
                }
            } else {
                pattern[i] = 0;
            }
        }

        for (int i = 0; i < data.length; i++) {
            data[i][5] = data[i][5] + pattern[i];
        }
        this.applyAllConstraints();
    }

    /**
     * Apply the CORSIM constraints for the data attribute
     */
    public void applyAllConstraints() {
        data = UTIL.Tools.applyConstraints(UTIL.Tools.copyMatrixIntDouble(data));
        data = UTIL.Tools.applyRTContraints(data);
    }

    /**
     * Clone the information of the father parameter
     * @param father
     */
    public void copy(IndividualTS father) {
        this.data = UTIL.Tools.copyMatrixInt(father.getData());
        this.evaluation = father.getEvaluation();
        this.tenure = father.getTenure();
    }

    public int[][] getData() {
        return data;
    }

    public void setData(int[][] data) {
        this.data = data;
    }

    public int getTenure() {
        return tenure;
    }

    public void setTenure(int tenure) {
        this.tenure = tenure;
    }

    public double getEvaluation() {
        return evaluation;
    }

    public void setEvaluation(double evaluation) {
        this.evaluation = evaluation;
    }

    public long size() {
        return data.length;
    }
}
