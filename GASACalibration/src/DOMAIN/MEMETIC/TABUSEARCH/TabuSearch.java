package DOMAIN.MEMETIC.TABUSEARCH;

import PERSISTENCE.Persistence;
import PERSISTENCE.ThreadCORSIM;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Implements all the necessary logic for tabu search algorithm
 * @author Carlos Gaviria, Cristian Arteaga and Victor Molano
 */
public class TabuSearch {

    //Algorithm parameteres
    private int tenure;             //Tabu list lenght
    private int max_iterations;
    private int neighborhood_size;
    //Algorithm tools
    private IndividualTS initial;
    private Persistence persistence;
    private List<IndividualTS> tabu_list;

    public TabuSearch(int[][] initial_data, Persistence persistence, int tenure, int max_iterations, int neighborhood_size) {
        initial = new IndividualTS();
        this.initial.setData(UTIL.Tools.copyMatrixInt(initial_data));
        tabu_list = new ArrayList();
        this.tenure = tenure;
        this.max_iterations = max_iterations;
        this.neighborhood_size = neighborhood_size;
        this.persistence = persistence;
    }

    /**
     * Randomly creates a neighborhood and return the individual with best evaluation in the neighborhood
     * @param Pbest
     * @return
     */
    public IndividualTS getBestNeighbor(IndividualTS Pbest) {
        IndividualTS best = new IndividualTS(Pbest);
        List<IndividualTS> neighborhood = new ArrayList();
        boolean first_neighbor = true, found = false;
        for (int i = 0; i < neighborhood_size; i++) {
            IndividualTS copy = new IndividualTS(best);
            copy.alterate();
            neighborhood.add(copy);
        }

        neighborhood = evaluation(neighborhood);

        for (int i = 0; i < neighborhood_size; i++) {
            if ((neighborhood.get(i).getEvaluation() < best.getEvaluation() || first_neighbor) && !(isTabu(neighborhood.get(i)))) { //if better move found, store it
                first_neighbor = false;
                best.copy(neighborhood.get(i));// Best = newBest
                found = true;
            }
        }

        if (found) {
            this.decrementTabu();
            best.setTenure(tenure);//Penalize the best neighbor
            tabu_list.add(best);
        }
        return best;
    }

    /**
     * Evaluate the individuals given in the list parameter
     * @param neighborhood
     * @return
     */
    public List<IndividualTS> evaluation(List<IndividualTS> neighborhood) {
        int size = neighborhood.size();
        //Copies of the trf
        for (int i = 0; i < size; i++) {
            UTIL.Tools.copyFile(persistence.getPathTrf(), i + ".trf");
        }
        int count = 0;
        //Creation of the new trf's files from the new individuals
        for (int i = 0; i < size; i++) {
            PERSISTENCE.ProcessPersistence.writeParametersSet(((IndividualTS) neighborhood.get(i)).getData(), i + ".trf");
        }
        //Simulataneous execution of CORSIM with Threads       
        ThreadCORSIM[] Threads = new ThreadCORSIM[size];

        int counter = 0;

        for (int i = 0; i < size; i++) {
            Threads[i] = new ThreadCORSIM(counter + ".trf");
            counter++;
        }

        for (int i = 0; i < size; i++) {
            try {
                Threads[i].t.join();
            } catch (InterruptedException ex) {
                Logger.getLogger(TabuSearch.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

        //Evaluation of each new individual
        for (int i = 0; i < size; i++) {
            int count_netsim = PERSISTENCE.ProcessPersistence.countDataNetsim(i + ".out");
            int count_fresim = PERSISTENCE.ProcessPersistence.countDataFresim(i + ".out");
            int count_real_netsim = (persistence.getActual_Data_Netsim().getADN_Data()).length;
            int count_real_fresim = (persistence.getActual_Data_Fresim().getADF_Data()).length;

            double[][] output_real = UTIL.Tools.addMatrix(UTIL.Tools.stringMatrixToDoubleMatrix(persistence.getActual_Data_Netsim().getADN_Data()), UTIL.Tools.stringMatrixToDoubleMatrix(persistence.getActual_Data_Fresim().getADF_Data()), count_real_netsim, count_real_fresim);
            double[][] output_model = UTIL.Tools.addMatrix(PERSISTENCE.ProcessPersistence.extractDataNetsim(i + ".out", count_netsim), PERSISTENCE.ProcessPersistence.extractDataFresim(i + ".out", count_fresim), count_real_netsim, count_real_fresim);

            double rms = UTIL.Tools.NRMS(output_real, output_model, persistence.getPercentageVolume());
            ((IndividualTS) (neighborhood.get(i))).setEvaluation(rms);
        }
        return neighborhood;
    }

    /**
     * Evaluate the individual given as parameter
     * @param toevaluate
     * @return
     */
    public double evaluate(IndividualTS toevaluate) {
        PERSISTENCE.ProcessPersistence.writeParametersSet(toevaluate.getData(), persistence.getPathTrf());
        UTIL.Tools.executeCorsim(persistence.getPathTrf());
        String path_out_model = UTIL.Tools.removeExtension(persistence.getPathTrf()) + ".out";
        int count_netsim = PERSISTENCE.ProcessPersistence.countDataNetsim(path_out_model);
        int count_fresim = PERSISTENCE.ProcessPersistence.countDataFresim(path_out_model);
        int count_real_netsim = (persistence.getActual_Data_Netsim().getADN_Data()).length;
        int count_real_fresim = (persistence.getActual_Data_Fresim().getADF_Data()).length;

        double[][] output_real = UTIL.Tools.addMatrix(UTIL.Tools.stringMatrixToDoubleMatrix(persistence.getActual_Data_Netsim().getADN_Data()), UTIL.Tools.stringMatrixToDoubleMatrix(persistence.getActual_Data_Fresim().getADF_Data()), count_real_netsim, count_real_fresim);
        double[][] output_model = UTIL.Tools.addMatrix(PERSISTENCE.ProcessPersistence.extractDataNetsim(path_out_model, count_netsim), PERSISTENCE.ProcessPersistence.extractDataFresim(path_out_model, count_fresim), count_real_netsim, count_real_fresim);
        double rms = UTIL.Tools.NRMS(output_real, output_model, persistence.getPercentageVolume());
        return rms;
    }

    /**
     * Start the calibration considering that a initial individual has been received.
     * @return
     */
    public IndividualTS run() {
        int i = 0,k=0,kmax=30;
        IndividualTS best = new IndividualTS(initial);
        best.setEvaluation(evaluate(best));
//        this.generateGEHData(best); 
        while( i < max_iterations && k < kmax) {
            IndividualTS newbest = this.getBestNeighbor(best);
            if (newbest.getEvaluation() < best.getEvaluation()) {
                best.copy(newbest);            
                k=0;
            }else{
                k++;
            }
            UTIL.Log.info("TS-RMS=" + best.getEvaluation());
            i++;
        }
//        this.generateGEHData(best); 
        return best;
    }

    /**
     * Checks if an individuals is marked as tabu in the tabu list
     * @param test
     * @return
     */
    public boolean isTabu(IndividualTS test) {
        boolean is_tabu = false;
        for (int i = 0; i < tabu_list.size(); i++) {
            if (test.isEqual(tabu_list.get(i)) && tabu_list.get(i).getTenure() > 0) {
                is_tabu = true;
            }
        }
        return is_tabu;
    }

    /**
     * Decrement one time the tabu mark for all the elements in tabu list
     */
    public void decrementTabu() {
        Iterator<IndividualTS> iterator = tabu_list.iterator();
        while (iterator.hasNext()) {
            IndividualTS actual = iterator.next();
            int newtenure = actual.getTenure() - actual.getTenure() <= 0 ? 0 : 1;
            actual.setTenure(newtenure);
            if (newtenure == 0) {
                iterator.remove();
            }
        }
    }

    /**
     * Generate the GEH data for the given individual
     * @param toevaluate
     */
    public void generateGEHData(IndividualTS toevaluate) {
        PERSISTENCE.ProcessPersistence.writeParametersSet(toevaluate.getData(), persistence.getPathTrf());
        UTIL.Tools.executeCorsim(persistence.getPathTrf());
        String path_out_model = UTIL.Tools.removeExtension(persistence.getPathTrf()) + ".out";
        int count_netsim = PERSISTENCE.ProcessPersistence.countDataNetsim(path_out_model);
        int count_fresim = PERSISTENCE.ProcessPersistence.countDataFresim(path_out_model);
        int count_real_netsim = (persistence.getActual_Data_Netsim().getADN_Data()).length;
        int count_real_fresim = (persistence.getActual_Data_Fresim().getADF_Data()).length;

        double[][] output_real = UTIL.Tools.addMatrix(UTIL.Tools.stringMatrixToDoubleMatrix(persistence.getActual_Data_Netsim().getADN_Data()), UTIL.Tools.stringMatrixToDoubleMatrix(persistence.getActual_Data_Fresim().getADF_Data()), count_real_netsim, count_real_fresim);
        double[][] output_model = UTIL.Tools.addMatrix(PERSISTENCE.ProcessPersistence.extractDataNetsim(path_out_model, count_netsim), PERSISTENCE.ProcessPersistence.extractDataFresim(path_out_model, count_fresim), count_real_netsim, count_real_fresim);
        double rms = UTIL.Tools.NRMS(output_real, output_model, persistence.getPercentageVolume());
        double[][] dataGEH = UTIL.Tools.setGEHGraphPrintConsole(output_real, output_model);
        UTIL.Tools.printMatrixToFile(dataGEH, UTIL.Log.getFileoutgeh(), String.valueOf(rms));
        double[][] dataVol = UTIL.Tools.setGraphics(output_real, output_model, "Volume");
        double[][] dataSpe = UTIL.Tools.setGraphics(output_real, output_model, "Speed");
        UTIL.Tools.printMatrixToFile(dataVol, UTIL.Log.getFileoutvolspeed(), "Volume - " + String.valueOf(rms));
        UTIL.Tools.printMatrixToFile(dataSpe, UTIL.Log.getFileoutvolspeed(), "Speed - " + String.valueOf(rms));
    }
}
