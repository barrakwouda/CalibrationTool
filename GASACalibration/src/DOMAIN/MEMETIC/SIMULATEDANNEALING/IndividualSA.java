package DOMAIN.MEMETIC.SIMULATEDANNEALING;

/**
 * Abstracts an individual to be used exclusively with the simulated
 * annealing algorithm
 *
 * @author Carlos Gaviria, Cristian Arteaga and Victor Molano
 */
public class IndividualSA {

    private int[][] data;
    private double evaluation;

    /**
     *
     * @param data
     * @param evaluation
     */
    public IndividualSA(int[][] data, double evaluation) {
        this.data = data;
        this.evaluation = evaluation;
    }

    /**
     *
     * @param father
     */
    public IndividualSA(IndividualSA father) {
        this.data = UTIL.Tools.copyMatrixInt(father.getData());
        this.evaluation = father.getEvaluation();
    }

    IndividualSA() {
    }

    /**
     * Generates a new neighbor individual based in the actual
     *
     * @return
     */
    public IndividualSA generateNeighbor() {
        IndividualSA neighbor = new IndividualSA(this);
        neighbor.alterate();
        return neighbor;
    }

    public int[][] getData() {
        return data;
    }

    public void setData(int[][] data) {
        this.data = data;
    }

    public double getEvaluation() {
        return evaluation;
    }

    public void setEvaluation(double evaluation) {
        this.evaluation = evaluation;
    }

    /**
     * Important method, necessary in simulated annealing algorithm which
     * alterate the data generating a new neighbor
     */
    private void alterate() {
        int howmuchpercentaje = 30;
        int addsubpercentaje = 50;
        int changepercentage = 10;
        int n = data.length;
        int[] pattern = new int[n];
        //Generating howmuch and who
        for (int i = 0; i < n; i++) {
            int step = (UTIL.Tools.calculateAlteration(data[i][3], data[i][4], changepercentage));
            double tem = Math.random();
            double tem2 = ((double) howmuchpercentaje / (double) 100);
            if (tem < tem2) {
                double tem3 = Math.random();
                double tem4 = ((double) addsubpercentaje / (double) 100);
                if (tem3 < tem4) {
                    pattern[i] = step * 1;
                } else {
                    pattern[i] = -(step * 1);
                }
            } else {
                pattern[i] = 0;
            }
        }

        for (int i = 0; i < data.length; i++) {
            data[i][5] = data[i][5] + pattern[i];
        }
        this.applyAllConstraints();
    }

    /**
     * Apply constraints to data attribute
     */
    public void applyAllConstraints() {
        data = UTIL.Tools.applyConstraints(UTIL.Tools.copyMatrixIntDouble(data));
        data = UTIL.Tools.applyRTContraints(data);

    }

    public long size() {
        return data.length;
    }
}
