package DOMAIN.MEMETIC.SIMULATEDANNEALING;

import PERSISTENCE.Persistence;

/**
 * Implements all the necessary logic for the simulated annealing
 * algorithm
 *
 * @author Carlos Gaviria, Cristian Arteaga and Victor Molano
 */
public class SimulatedAnnealing {

    public double temperature;
    public double coolingrate;
    public IndividualSA initial;
    private Persistence persistence;

    public SimulatedAnnealing(int[][] data, Persistence persistence, double temperature, double coolingrate) {
        initial = new IndividualSA();
        this.initial.setData(UTIL.Tools.copyMatrixInt(data));
//        printMatrixToConsoleIntegerLine(initial.getData(), "SA Initial DATA");
        this.temperature = temperature;
        this.coolingrate = coolingrate;
        this.persistence = persistence;
    }

    /**
     * Starts the calibration process considering that an initial individual has
     * been received
     *
     * @return
     */
    public IndividualSA run() {
        double initialtemp = temperature;
        this.initial.setEvaluation(evaluate(initial));
        IndividualSA best = new IndividualSA(initial);
        int i = 0;
//        generateGEHData(best);
//        UTIL.Log.info("SA_PAR, temp = "+temperature+",coolrate="+coolingrate);
        int k = 0, kmax = 30;
        double alpha = 0.92;
        IndividualSA currentSolution = new IndividualSA(initial);
        while (this.temperature > 0 && k < kmax) {
            IndividualSA newSolution = null;
            double tem3 = Math.random();
            double tem4 = ((double) 50 / (double) 100);
            if (tem3 < tem4) {
                newSolution = currentSolution.generateNeighbor();
            } else {
                newSolution = best.generateNeighbor();
            }
            newSolution.setEvaluation(evaluate(newSolution));
            if (acceptanceProbability(currentSolution.getEvaluation(), newSolution.getEvaluation()) > Math.random()) {
                currentSolution = new IndividualSA(newSolution);     
            }
            if (currentSolution.getEvaluation() < best.getEvaluation()&&currentSolution.getEvaluation() != 0 &&  best.getEvaluation() ==0) {
                best = new IndividualSA(currentSolution);
                k = 0;
            } else {
                k++;
            }
            UTIL.Log.info("SA-RMS=" + best.getEvaluation());
            temperature = temperature * alpha;  
            i++;
//            temperature -= coolingrate;
        }
//        UTIL.Log.info("BEST SA CALIBRATION= " + best.getEvaluation());
//        printMatrixToConsoleIntegerLine(best.getData(), "SA End DATA");
//        generateGEHData(best);
        return best;
    }

    /**
     * Important and characteristic method of simulated annealing algorithms
     * which calculates the acceptance probability in a given moment using the
     * given parameters
     *
     * @return
     */
    private double acceptanceProbability(double energy, double newEnergy) {
        double ret = 0;
        if (newEnergy < energy) {
            ret = 1.0;
        } else {
            ret = Math.exp((-1) * (Math.abs(energy - newEnergy) / temperature));
        }
//        System.out.println();
//        System.out.println("pa ="+ ret+"\ttemp="+temperature+"oldE ="+ energy+"\tnewE="+newEnergy);
        return ret;
    }

    /**
     * Evaluate the individual given as parameter
     * @param toevaluate
     * @return
     */
    public double evaluate(IndividualSA toevaluate) {
        PERSISTENCE.ProcessPersistence.writeParametersSet(toevaluate.getData(), persistence.getPathTrf());
        UTIL.Tools.executeCorsim(persistence.getPathTrf());
        String path_out_model = UTIL.Tools.removeExtension(persistence.getPathTrf()) + ".out";
        int count_netsim = PERSISTENCE.ProcessPersistence.countDataNetsim(path_out_model);
        int count_fresim = PERSISTENCE.ProcessPersistence.countDataFresim(path_out_model);
        int count_real_netsim = (persistence.getActual_Data_Netsim().getADN_Data()).length;
        int count_real_fresim = (persistence.getActual_Data_Fresim().getADF_Data()).length;

        double[][] output_real = UTIL.Tools.addMatrix(UTIL.Tools.stringMatrixToDoubleMatrix(persistence.getActual_Data_Netsim().getADN_Data()), UTIL.Tools.stringMatrixToDoubleMatrix(persistence.getActual_Data_Fresim().getADF_Data()), count_real_netsim, count_real_fresim);
        double[][] output_model = UTIL.Tools.addMatrix(PERSISTENCE.ProcessPersistence.extractDataNetsim(path_out_model, count_netsim), PERSISTENCE.ProcessPersistence.extractDataFresim(path_out_model, count_fresim), count_real_netsim, count_real_fresim);
        double rms = UTIL.Tools.NRMS(output_real, output_model, persistence.getPercentageVolume());

        return rms;
    }

    /**
     * Generate the GEH data for the individual given as parametr
     * @param toevaluate
     */
    public void generateGEHData(IndividualSA toevaluate) {
        PERSISTENCE.ProcessPersistence.writeParametersSet(toevaluate.getData(), persistence.getPathTrf());
        UTIL.Tools.executeCorsim(persistence.getPathTrf());
        String path_out_model = UTIL.Tools.removeExtension(persistence.getPathTrf()) + ".out";
        int count_netsim = PERSISTENCE.ProcessPersistence.countDataNetsim(path_out_model);
        int count_fresim = PERSISTENCE.ProcessPersistence.countDataFresim(path_out_model);
        int count_real_netsim = (persistence.getActual_Data_Netsim().getADN_Data()).length;
        int count_real_fresim = (persistence.getActual_Data_Fresim().getADF_Data()).length;

        double[][] output_real = UTIL.Tools.addMatrix(UTIL.Tools.stringMatrixToDoubleMatrix(persistence.getActual_Data_Netsim().getADN_Data()), UTIL.Tools.stringMatrixToDoubleMatrix(persistence.getActual_Data_Fresim().getADF_Data()), count_real_netsim, count_real_fresim);
        double[][] output_model = UTIL.Tools.addMatrix(PERSISTENCE.ProcessPersistence.extractDataNetsim(path_out_model, count_netsim), PERSISTENCE.ProcessPersistence.extractDataFresim(path_out_model, count_fresim), count_real_netsim, count_real_fresim);
        double rms = UTIL.Tools.NRMS(output_real, output_model, persistence.getPercentageVolume());
        double[][] dataGEH = UTIL.Tools.setGEHGraphPrintConsole(output_real, output_model);
        UTIL.Tools.printMatrixToFile(dataGEH, UTIL.Log.getFileoutgeh(), String.valueOf(rms));
        double[][] dataVol = UTIL.Tools.setGraphics(output_real, output_model, "Volume");
        double[][] dataSpe = UTIL.Tools.setGraphics(output_real, output_model, "Speed");
        UTIL.Tools.printMatrixToFile(dataVol, UTIL.Log.getFileoutvolspeed(), "Volume - " + String.valueOf(rms));
        UTIL.Tools.printMatrixToFile(dataSpe, UTIL.Log.getFileoutvolspeed(), "Speed - " + String.valueOf(rms));
    }
}
