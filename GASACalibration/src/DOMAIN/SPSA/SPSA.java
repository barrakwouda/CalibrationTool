package DOMAIN.SPSA;

import DOMAIN.Graph;
import DOMAIN.MEMETIC.GENETIC.IndividualGA;
import PERSISTENCE.Persistence;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Implements all the necessary logic to calibrate with the SPSA
 * algorithm
 *
 * @author Carlos Gaviria, Cristian Arteaga and Victor Molano
 * @layer DOMAIN
 */
public class SPSA {

    int K;
    double A;
    double a;
    double c;
    double alpha;
    double gamma;
    int n;
    int[] MonteCarlo;
    double[] CkMonteCarlo;
    List Rms;
    Map<String, Graph> graphs;
    private Persistence persistence;

    /**
     * Set the SPSA data for the class
     *
     * @param persistence
     * @param K
     * @param a
     * @param A
     * @param c
     * @param alpha
     * @param gamma
     * @author Carlos Gaviria, Victor Molano and Cristian Arteaga
     */
    public SPSA(Persistence persistence, int K, double A, double a, double c, double alpha, double gamma) {
        this.persistence = persistence;
        n = persistence.countParametersSet();
        MonteCarlo = new int[n];
        CkMonteCarlo = new double[n];
        //this.K = K;
        this.A = A;
        this.a = a;
        this.c = c;
        this.alpha = alpha;
        this.gamma = gamma;
        this.K = K;
        Rms = new ArrayList<Double>();
        graphs = new HashMap<String, Graph>();
    }

    /**
     * Starts the calibration considering that an initial theta matrix has been received
     */
    public void run() {
        String path_trf_plus = UTIL.Tools.removeExtension(persistence.getPathTrf()) + "_plus.trf";
        String path_trf_minus = UTIL.Tools.removeExtension(persistence.getPathTrf()) + "_minus.trf";

        String path_out_plus = UTIL.Tools.removeExtension(path_trf_plus) + ".out";
        String path_out_minus = UTIL.Tools.removeExtension(path_trf_minus) + ".out";
        String path_out_model = UTIL.Tools.removeExtension(persistence.getPathTrf()) + ".out";

        //Load the theta bidimensional array from file
        int[][] parameters_set = UTIL.Tools.stringMatrixToIntMatrix(persistence.getParameters_Set().getPS_Parameters());
        int lenght_theta = PERSISTENCE.ProcessPersistence.countParametersSet(parameters_set, persistence.getPathTrf());
        double bestrms = 1000000.0;
        int[][] besttheta = UTIL.Tools.copyMatrixInt(PERSISTENCE.ProcessPersistence.exctractParametersSet(parameters_set, persistence.getPathTrf()));
        this.generateGEHData(besttheta);
        int counter = 0;


        UTIL.Log.info("SPSA SETTINGS: ");
        UTIL.Log.info("Coefficients: ");
        UTIL.Log.info("A     = " + A);
        UTIL.Log.info("a     = " + a);
        UTIL.Log.info("c     = " + c);
        UTIL.Log.info("alhpa = " + alpha);
        UTIL.Log.info("gamma = " + gamma);
        UTIL.Log.info("Vector size = " + lenght_theta);



        //&& !spsa.convergenceCondition()
        //while(counter < CounterK && !spsa.convergenceCondition())
        while (counter < K) {
            int[][] theta = PERSISTENCE.ProcessPersistence.exctractParametersSet(parameters_set, persistence.getPathTrf());

            int[][] theta_plus = this.generateThetaPlus(theta);
            int[][] theta_minus = this.generateThetaMinus(theta);

            UTIL.Tools.copyFile(persistence.getPathTrf(), path_trf_plus);
            UTIL.Tools.copyFile(persistence.getPathTrf(), path_trf_minus);

            theta_plus = UTIL.Tools.applyRTContraints(theta_plus);
            PERSISTENCE.ProcessPersistence.writeParametersSet(theta_plus, path_trf_plus);
            theta_minus = UTIL.Tools.applyRTContraints(theta_minus);
            PERSISTENCE.ProcessPersistence.writeParametersSet(theta_minus, path_trf_minus);

            UTIL.Tools.executeCorsim(path_trf_plus);
            UTIL.Tools.executeCorsim(path_trf_minus);
            UTIL.Tools.executeCorsim(persistence.getPathTrf());


            //Esto deberia estar fuera del 
            int count_netsim = PERSISTENCE.ProcessPersistence.countDataNetsim(path_out_model);
            int count_fresim = PERSISTENCE.ProcessPersistence.countDataFresim(path_out_model);

            int count_real_netsim = (persistence.getActual_Data_Netsim().getADN_Data()).length;
            int count_real_fresim = (persistence.getActual_Data_Fresim().getADF_Data()).length;

            double[][] output_plus = UTIL.Tools.addMatrix(PERSISTENCE.ProcessPersistence.extractDataNetsim(path_out_plus, count_netsim), PERSISTENCE.ProcessPersistence.extractDataFresim(path_out_plus, count_fresim), count_real_netsim, count_real_fresim);
            double[][] output_minus = UTIL.Tools.addMatrix(PERSISTENCE.ProcessPersistence.extractDataNetsim(path_out_minus, count_netsim), PERSISTENCE.ProcessPersistence.extractDataFresim(path_out_minus, count_fresim), count_real_netsim, count_real_fresim);
            double[][] output_real = UTIL.Tools.addMatrix(UTIL.Tools.stringMatrixToDoubleMatrix(persistence.getActual_Data_Netsim().getADN_Data()), UTIL.Tools.stringMatrixToDoubleMatrix(persistence.getActual_Data_Fresim().getADF_Data()), count_real_netsim, count_real_fresim);
            double[][] output_model = UTIL.Tools.addMatrix(PERSISTENCE.ProcessPersistence.extractDataNetsim(path_out_model, count_netsim), PERSISTENCE.ProcessPersistence.extractDataFresim(path_out_model, count_fresim), count_real_netsim, count_real_fresim);

            int[][] new_theta = this.parametersUpdate(theta, output_plus, output_minus, output_real, output_model, persistence.getPercentageVolume(), persistence.getPercentageSpeed());
            double rms = this.evaluate(theta, output_plus, output_minus, output_real, output_model, persistence.getPercentageVolume(), persistence.getPercentageSpeed());
            new_theta = UTIL.Tools.applyRTContraints(new_theta);
            PERSISTENCE.ProcessPersistence.writeParametersSet(new_theta, persistence.getPathTrf());
            if (rms < bestrms) {
                besttheta = UTIL.Tools.copyMatrixInt(new_theta);
                bestrms = rms;
            }
            counter++;
            //if(spsa.convergenceCondition() || counter == CounterK){
            if (counter == K) {
                PERSISTENCE.ProcessPersistence.writeParametersSet(besttheta, persistence.getPathTrf());
                UTIL.Tools.executeCorsim(persistence.getPathTrf());
                output_model = UTIL.Tools.addMatrix(PERSISTENCE.ProcessPersistence.extractDataNetsim(path_out_model, count_netsim), PERSISTENCE.ProcessPersistence.extractDataFresim(path_out_model, count_fresim), count_real_netsim, count_real_fresim);
                addGraph(output_real, output_model, "EndVolume", "Volume");
                addGraph(output_real, output_model, "EndSpeed", "Speed");
                addGraph(output_real, output_model, "EndGEH", "GEH");
                UTIL.Log.info("Best RMS = " + bestrms);
                this.generateGEHData(besttheta);
            }
        }

    }

    /**
     * Return a MonteCarlo vector with Bernoulli distribution
     *
     * @return int[] MonteCarlo
     * @author Carlos Gaviria, Victor Molano and Cristian Arteaga
     */
    public int[] generateMonteCarlo() {
        for (int i = 0; i < MonteCarlo.length; i++) {
            double sign = Math.random();
            if (sign < 0.5) {
                MonteCarlo[i] = -1;
            } else {
                MonteCarlo[i] = 1;
            }
        }
        this.generateCkMonteCarlo();
        return MonteCarlo;
    }

    /**
     * Return gain function Ak
     *
     * @return double ak
     * @author Carlos Gaviria, Victor Molano and Cristian Arteaga
     */
    public double Ak() {
        double ak = a / (Math.pow((K + 1 + A), alpha));
        return ak;
    }

    /**
     * Return gain function Ck
     *
     * @return double ck
     * @author Carlos Gaviria, Victor Molano and Cristian Arteaga
     */
    public double Ck() {
        double ck = c / (Math.pow((K + 1), gamma));
        return ck;
    }

    /**
     * Return the Multiplication between Ck and Montecarlo bidimensional array
     *
     * @author Carlos Gaviria, Victor Molano and Cristian Arteaga
     */
    public void generateCkMonteCarlo() {
        for (int i = 0; i < CkMonteCarlo.length; i++) {
            CkMonteCarlo[i] = this.Ck() * MonteCarlo[i];
            //System.out.print("_"+CkMonteCarlo[i]);
        }
    }

    /**
     * Generate the theta plus bi-dimensional array
     *
     * @param theta
     * @return double[] theta_plus
     * @author Carlos Gaviria, Victor Molano and Cristian Arteaga
     */
    public int[][] generateThetaPlus(int[][] theta) {
        generateMonteCarlo();
        double[][] theta_plus_aux = UTIL.Tools.copyMatrixIntDouble(theta);
        int[][] theta_plus = new int[theta.length][7];
        for (int i = 0; i < theta_plus_aux.length; i++) {
            theta_plus_aux[i][5] = theta[i][5] + CkMonteCarlo[i];
        }
        //FACADE.FacadeUTIL.Tools.printMatrixToConsole(theta_plus_aux, "--------------- THETA PLUS DOUBLE -----------");
        theta_plus = UTIL.Tools.applyConstraints(theta_plus_aux);
        //FACADE.FacadeUTIL.Tools.printMatrixToConsoleInteger(theta_plus, "--------------- THETA PLUS INTEGER -----------");

        return theta_plus;
    }

    /**
     * Generate the theta minus bidimensional array
     *
     * @param theta
     * @return double[] theta_minus
     * @author Carlos Gaviria, Victor Molano and Cristian Arteaga
     */
    public int[][] generateThetaMinus(int[][] theta) {
        double[][] theta_minus_aux = UTIL.Tools.copyMatrixIntDouble(theta);
        int[][] theta_minus = new int[theta.length][7];
        for (int i = 0; i < theta_minus_aux.length; i++) {
            theta_minus_aux[i][5] = theta[i][5] - CkMonteCarlo[i];
        }
        theta_minus = UTIL.Tools.applyConstraints(theta_minus_aux);

        return theta_minus;
    }

    /**
     * Generate the theta minus bidimensional array
     *
     * @param theta
     * @param output_plus
     * @param output_minus
     * @param output_real
     * @param percentage_speed
     * @param percentage_volume
     * @param output_model
     * @return double[] theta_minus
     * @author Carlos Gaviria, Victor Molano and Cristian Arteaga
     */
    public int[][] parametersUpdate(int[][] theta, double[][] output_plus, double[][] output_minus, double[][] output_real, double[][] output_model, double percentage_volume, double percentage_speed) {
        double rms_plus = UTIL.Tools.NRMS(output_real, output_plus, percentage_volume);
        double rms_minus = UTIL.Tools.NRMS(output_real, output_minus, percentage_volume);
        double rms_model = UTIL.Tools.NRMS(output_real, output_model, percentage_volume);

        double[] gk_thetak = this.generateGkThetak(theta.length, rms_plus, rms_minus);
        for (int i = 0; i < theta.length; i++) {
            theta[i][5] = (int) (theta[i][5] - Math.round(Ak() * gk_thetak[i]));

        }
        theta = UTIL.Tools.applyConstraints(UTIL.Tools.copyMatrixIntDouble(theta));
        Rms.add(rms_model);
        UTIL.Log.info("RMS = " + rms_model);
        return theta;
    }

    /**
     *
     * @param theta
     * @param output_plus
     * @param output_minus
     * @param output_real
     * @param output_model
     * @param percentage_volume
     * @param percentage_speed
     * @return
     */
    public double evaluate(int[][] theta, double[][] output_plus, double[][] output_minus, double[][] output_real, double[][] output_model, double percentage_volume, double percentage_speed) {
        double rms_model = UTIL.Tools.NRMS(output_real, output_model, percentage_volume);

        return rms_model;
    }

    /**
     * Calculates the Gradient approximation
     *
     * @param dint theta_length,double rms_plus, double rms_minus
     * @return double[] theta_minus
     * @author Carlos Gaviria, Victor Molano and Cristian Arteaga
     */
    private double[] generateGkThetak(int theta_length, double rms_plus, double rms_minus) {
        double[] gk_thetak = new double[theta_length];
        for (int i = 0; i < gk_thetak.length; i++) {
            gk_thetak[i] = ((rms_plus - rms_minus) / (2 * Ck())) * MonteCarlo[i];
        }
        return gk_thetak;
    }

    /**
     * Establishes the stop condition for optimization
     *
     * @return boolean, true if stop
     * @author Carlos Gaviria, Victor Molano and Cristian Arteaga
     */
    public boolean convergenceCondition() {
        double avg = 0;
        double rho = 0;
        if (Rms.size() < 10) {
            return false;
        }
        for (int i = (Rms.size() - 10); i < Rms.size(); i++) {
            avg = avg + (Double) Rms.get(i);
        }
        avg = avg / 10;
        rho = Math.abs(avg - (Double) Rms.get(Rms.size() - 1));
        if (rho < 0.002) {
            UTIL.Log.info("-------------------------------------------------------");
            UTIL.Log.info("----------------CONVERGENCE CONDITION TRUE-------------");
            UTIL.Log.info("-------------------------------------------------------");
            return true;

        } else {
            return false;
        }

    }

    /**
     *
     * @param x_data
     * @param y_data
     * @param id
     * @param type
     */
    public void addGraph(double[][] x_data, double[][] y_data, String id, String type) {
        Graph graph = new Graph();
        if (type.equals("Volume")) {
            graph.setVolSpeedGraphic(x_data, y_data, type);
        }
        if (type.equals("Speed")) {
            graph.setVolSpeedGraphic(x_data, y_data, type);
        }
        if (type.equals("GEH")) {
            graph.setGEHGraph(x_data, y_data);
        }
        graphs.put(id, graph);

    }

    /**
     *
     * @param theta
     */
    public void generateGEHData(int[][] theta) {
        PERSISTENCE.ProcessPersistence.writeParametersSet(theta, persistence.getPathTrf());
        UTIL.Tools.executeCorsim(persistence.getPathTrf());
        String path_out_model = UTIL.Tools.removeExtension(persistence.getPathTrf()) + ".out";
        int count_netsim = PERSISTENCE.ProcessPersistence.countDataNetsim(path_out_model);
        int count_fresim = PERSISTENCE.ProcessPersistence.countDataFresim(path_out_model);
        int count_real_netsim = (persistence.getActual_Data_Netsim().getADN_Data()).length;
        int count_real_fresim = (persistence.getActual_Data_Fresim().getADF_Data()).length;

        double[][] output_real = UTIL.Tools.addMatrix(UTIL.Tools.stringMatrixToDoubleMatrix(persistence.getActual_Data_Netsim().getADN_Data()), UTIL.Tools.stringMatrixToDoubleMatrix(persistence.getActual_Data_Fresim().getADF_Data()), count_real_netsim, count_real_fresim);
        double[][] output_model = UTIL.Tools.addMatrix(PERSISTENCE.ProcessPersistence.extractDataNetsim(path_out_model, count_netsim), PERSISTENCE.ProcessPersistence.extractDataFresim(path_out_model, count_fresim), count_real_netsim, count_real_fresim);
        double rms = UTIL.Tools.NRMS(output_real, output_model, persistence.getPercentageVolume());
        double[][] dataGEH = UTIL.Tools.setGEHGraphPrintConsole(output_real, output_model);
        UTIL.Tools.printMatrixToFile(dataGEH, UTIL.Log.getFileoutgeh(), String.valueOf(rms));
        double[][] dataVol = UTIL.Tools.setGraphics(output_real, output_model, "Volume");
        double[][] dataSpe = UTIL.Tools.setGraphics(output_real, output_model, "Speed");
        UTIL.Tools.printMatrixToFile(dataVol, UTIL.Log.getFileoutvolspeed(), "Volume - " + String.valueOf(rms));
        UTIL.Tools.printMatrixToFile(dataSpe, UTIL.Log.getFileoutvolspeed(), "Speed - " + String.valueOf(rms));

    }
}
