package DOMAIN;

/**
 * Contains a set of parameters to be extracted from the trf file.
 * @author Carlos Gaviria, Cristian Arteaga and Victor Molano
 * @layer Domain
 */
public class ParametersSet {
    
    /**
     * PS_Parameters each file contains the columns in format:
     * [0] Record Type, [1] Start Column, [2] End Column, [3] Min Value, [4] Max Value
     * Cannot be repeated files in the matrix
     */
    private String[][] PS_Parameters;
    

    public ParametersSet(String[][] data)
    {
        this.PS_Parameters = data;
    }
    
     public ParametersSet()
    {
    }

    public String[][] getPS_Parameters() {
        return PS_Parameters;
    }

    public void setPS_Parameters(String[][] data) {
        this.PS_Parameters = data;
    }
    
    

}
