package DOMAIN;

/**
 * Allows to store the Actual Data Netsim values
 * @author Carlos Gaviria, Cristian Arteaga and Victor Molano
 * @layer DOMAIN
 */
public class ActualDataNetsim {
    private String[][] ADN_Data;
    
     /**
     * Default Constructor
     */
    public ActualDataNetsim(){
        
    }
    /**
     * Initialize the object
     * @param data
     */
    public ActualDataNetsim(String[][] data){
        this.ADN_Data = data;
    }
    
    /**
     * Return the ADN_Data attribute
     * @return this.ADN_Data
     */
    public String[][] getADN_Data() {
        return this.ADN_Data;
    }

    /**
     * Set the ADN_Data attribute with the data bidimensional array
     * @param data
     */
    public void setADN_Data(String[][] data) {
        this.ADN_Data = data;
    }

}
