/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package PERSISTENCE;

import DOMAIN.Model.CorsimModel;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

/**
 * Managment reading and writing files related with input data.
 * @author Carlos Gaviria and Cristian Arteaga
 * @layer Persitence
 */
public class InputPersistence {
    
    
     /**
     * Saves into a file the bidimensional array data
     * @param data 
     * @param path 
     * @return boolean, false if error
     * @author Carlos Gaviria and Cristian Arteaga
     */
    public static boolean saveInput(String[][] data, String path)
    {
        Object to_save = (Object)data;
        try
        {
            ObjectOutputStream writer = new ObjectOutputStream(new FileOutputStream(path));
            writer.writeObject(to_save);
            writer.close();
            return true;
        }
        catch(Exception e)
        {
            e.printStackTrace();
            return false;
        }        
    }
    
    /**
     *
     * @param model
     * @param path
     * @return
     */
    public static boolean saveModelInput(CorsimModel model, String path)
    {
        Object to_save = (Object)model;
        try
        {
            ObjectOutputStream writer = new ObjectOutputStream(new FileOutputStream(path));
            writer.writeObject(to_save);
            writer.close();
            return true;
        }
        catch(Exception e)
        {
            e.printStackTrace();
            return false;
        }        
    }
    
    /**
     *
     * @param path
     * @return
     */
    public static CorsimModel loadModelInput(String path)
    {
        CorsimModel model;
        try
        {
            ObjectInputStream reader = new ObjectInputStream(new FileInputStream(path));
            model = (CorsimModel)reader.readObject();
            reader.close();
            return model;
        }
        catch(Exception e)
        {
            e.printStackTrace();
            return null;
        }        
    }
    
    
     /**
     * Return the data(String[][]) contained from a file
     * @param path 
     * @return String[][] data, null if error
     * @author Carlos Gaviria and Cristian Arteaga
     */
    public static String[][] loadInput(String path)
    {
        String data[][];
        try
        {
            ObjectInputStream reader = new ObjectInputStream(new FileInputStream(path));
            data = (String[][])reader.readObject();
            reader.close();
            return data;
        }
        catch(Exception e)
        {
            e.printStackTrace();
            return null;
        }        
    }
    
    /**
     *
     * @param data
     * @param path
     * @return
     */
    public static boolean saveInputInteger(int[][] data, String path)
    {
        Object to_save = (Object)data;
        try
        {
            ObjectOutputStream writer = new ObjectOutputStream(new FileOutputStream(path));
            writer.writeObject(to_save);
            writer.close();
            return true;
        }
        catch(Exception e)
        {
            e.printStackTrace();
            return false;
        }        
    }

    /**
     *
     * @param path
     * @return
     */
    public static int[][] loadInputInteger(String path)
    {
        int data[][];
        try
        {
            ObjectInputStream reader = new ObjectInputStream(new FileInputStream(path));
            data = (int[][])reader.readObject();
            reader.close();
            return data;
        }
        catch(Exception e)
        {
            e.printStackTrace();
            return null;
        }        
    }

}


class IP extends InputPersistence{

    public IP() {
        super();
    }

}