
package PERSISTENCE;

import java.io.File;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author das
 */
public class ThreadCORSIM implements Runnable{
    
    String path_Trf; 

    /**
     *
     */
    public Thread t;

    /**
     *
     * @param pathTrf
     */
    public ThreadCORSIM(String pathTrf) {
        path_Trf = pathTrf;
        t = new Thread(this, path_Trf);        
        t.start();
    }

    public void run() {
        try {
            String execute = "";
            File corsim_file = new File("RunCORSIM.exe");
            String dir_corsim = corsim_file.getPath();
            File trf_file = new File(path_Trf);
            String dir_trf_file = trf_file.getAbsolutePath();
            execute = dir_corsim+" \""+dir_trf_file+"\" "+"out > nul"; 
//            Process proc = Runtime.getRuntime().exec("cmd /C start/wait "+execute);
            Process proc = Runtime.getRuntime().exec("cmd /C start/wait /min "+execute);
            proc.waitFor();
            proc.destroy();
        } catch (IOException ex) {
            Logger.getLogger(ThreadCORSIM.class.getName()).log(Level.SEVERE, null, ex);
        } catch (InterruptedException ex) {
            Logger.getLogger(ThreadCORSIM.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
}
