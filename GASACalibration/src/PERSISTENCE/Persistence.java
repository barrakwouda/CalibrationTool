package PERSISTENCE;

import DOMAIN.ActualDataFresim;
import DOMAIN.ActualDataNetsim;
import DOMAIN.Model.CorsimModel;
import DOMAIN.ParametersSet;
import static PERSISTENCE.ProcessPersistence.writeParametersSet;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.List;

/**
 * Group all the necessary method to perform persistence actions, the extraction
 * of theta matrix is done dynamically due to the trf files changes trough the
 * iterations of the calibration algorithms
 *
 * @author das
 */
public class Persistence {

    private ParametersSet Parameters_Set;
    private ActualDataNetsim Actual_Data_Netsim;
    private ActualDataFresim Actual_Data_Fresim;
    private int PercentageVolume;
    private int PercentageSpeed;
    private String PathTrf;
    private int[][] filter;
    private CorsimModel corsimmodel;

    /**
     * Important function called by all the algorithms, it extracts from the trf
     * file the parameters selected to be calibrated
     *
     * @return Theta matrix with parameters to be calibrated
     */
    public int[][] getDataMatrix() {
        int[][] parameters_set = UTIL.Tools.stringMatrixToIntMatrix(Parameters_Set.getPS_Parameters());
        int[][] theta = PERSISTENCE.ProcessPersistence.exctractParametersSet(parameters_set, PathTrf);
        return corsimmodel.getFilteredTheta(theta);// UTIL.Tools.applyThetaFilter(theta, filter);
//        return theta;

    }

    /**
     * Works similarly to getDataMatrix, the difference is that this function
     * does not filter the links preselected by the user;
     *
     * @return Theta matrix with parameters to be calibrated
     */
    public int[][] getDataMatrixNoFilter() {
        int[][] parameters_set = UTIL.Tools.stringMatrixToIntMatrix(Parameters_Set.getPS_Parameters());
        int[][] theta = PERSISTENCE.ProcessPersistence.exctractParametersSet(parameters_set, PathTrf);
        return theta;

    }

    public void initializeCorsimModel() {
        int[][] parameters_set = UTIL.Tools.stringMatrixToIntMatrix(Parameters_Set.getPS_Parameters());
        int[][] theta = PERSISTENCE.ProcessPersistence.exctractParametersSet(parameters_set, PathTrf);
        String[][] link_names = PERSISTENCE.ProcessPersistence.extractLinkName(PathTrf);

        setCorsimmodel(new CorsimModel());
        getCorsimmodel().initializeWithThetaMatrix(theta,link_names);
        corsimmodel.setCalibrate(true);
        System.out.println(corsimmodel.toStringCascade());
    }

    /**
     *
     * @param improvedfilter
     * @deprecated
     */
    public void setImprovedFilter(int[][] improvedfilter) {
        //Scheme of theta   [0] Rt,     [1] SC,      [2] EC, [3] MinV,      [4] MaxV,  [5] Value, [6] lineNumber,[7] InNode, [8]EndNode
        //Scheme of filter  [0] InNode, [1] EndNode, [2] RT, [3] calibrate?
        int[][] parameters_set = UTIL.Tools.stringMatrixToIntMatrix(Parameters_Set.getPS_Parameters());
        int[][] theta = PERSISTENCE.ProcessPersistence.exctractParametersSet(parameters_set, PathTrf);
        UTIL.Tools.tempPrintMatrix(theta);
        String[] rts = UTIL.Tools.getImprovedFilterColumns(theta);
        String[] links = UTIL.Tools.getImprovedFilterRows(theta);
        List<int[]> filterlist = new ArrayList();
        for (int i = 0; i < links.length; i++) {
            for (int j = 0; j < rts.length; j++) {
                if (improvedfilter[i][j] == 1) {
                    int[] filteritem = new int[4];
                    String[] parts = links[i].split("-");
                    filteritem[0] = Integer.parseInt(parts[0]);
                    filteritem[1] = Integer.parseInt(parts[1]);
                    filteritem[2] = Integer.parseInt(rts[j]);
                    filteritem[3] = 1;
                    filterlist.add(filteritem);
                }
            }
        }
        this.filter = UTIL.Tools.vectorsListToMatrixFilter(filterlist);
    }

    /**
     *
     * @return @deprecated
     */
    public int[][] getImprovedFilterMatrix() {
        int[][] parameters_set = UTIL.Tools.stringMatrixToIntMatrix(Parameters_Set.getPS_Parameters());
        int[][] theta = PERSISTENCE.ProcessPersistence.exctractParametersSet(parameters_set, PathTrf);

        String[] rts = this.getImprovedFilterColumns();
        String[] links = this.getImprovedFilterRows();
        int[][] filtermatrix = new int[links.length][rts.length];
        for (int i = 0; i < links.length; i++) {
            for (int j = 0; j < rts.length; j++) {
                filtermatrix[i][j] = UTIL.Tools.checkLinkRecordTypeExistece(theta, links[i], rts[j]);
            }
        }

        return filtermatrix;
    }

    /**
     *
     * @param data
     * @return
     */
    public boolean writeDataMatrix(int[][] data) {
        return writeParametersSet(data, PathTrf);
    }

    /**
     *
     * @return @deprecated
     */
    public String[] getImprovedFilterColumns() {
        int[][] parameters_set = UTIL.Tools.stringMatrixToIntMatrix(Parameters_Set.getPS_Parameters());
        int[][] theta = PERSISTENCE.ProcessPersistence.exctractParametersSet(parameters_set, PathTrf);


        List<String> names = new ArrayList();
        for (int i = 0; i < theta.length; i++) {
            if (!names.contains(String.valueOf(theta[i][0]))) {
                names.add(String.valueOf(theta[i][0]));
            }
        }

        String[] columns = new String[names.size()];
        java.util.Collections.sort(names);
        for (int i = 0; i < names.size(); i++) {
            columns[i] = names.get(i);
        }

        return columns;
    }

    /**
     *
     * @return @deprecated
     */
    public String[] getImprovedFilterRows() {
        int[][] parameters_set = UTIL.Tools.stringMatrixToIntMatrix(Parameters_Set.getPS_Parameters());
        int[][] theta = PERSISTENCE.ProcessPersistence.exctractParametersSet(parameters_set, PathTrf);


        List<String> names = new ArrayList();
        for (int i = 0; i < theta.length; i++) {
            String rt = String.valueOf(theta[i][7]) + "-" + String.valueOf(theta[i][8]);
            if (!names.contains(rt)) {
                names.add(rt);
            }
        }

        String[] rows = new String[names.size()];
        java.util.Collections.sort(names);
        for (int i = 0; i < names.size(); i++) {
            rows[i] = names.get(i);
        }

        return rows;
    }

    /**
     *
     * @return @deprecated
     */
    public int[][] getInitialFilter() {

        int[][] parameters_set = UTIL.Tools.stringMatrixToIntMatrix(Parameters_Set.getPS_Parameters());
        int[][] theta = PERSISTENCE.ProcessPersistence.exctractParametersSet(parameters_set, PathTrf);
        setFilter(this.distinct(theta));
        return filter;
    }

    /**
     * Count all the parameters into the trf file that were selected to be
     * calibrated, this method is used to initialize the matrix in many parts of
     * the code. This method go over the whole file and using the parameters set
     * check the existence of the selected parameter and increments a counter if
     * the parameter is found
     *
     * @return Quantity of parameters available for calibration
     */
    public int countParametersSet() {
        int[][] parameters_set = UTIL.Tools.stringMatrixToIntMatrix(Parameters_Set.getPS_Parameters());
        String line;
        String line_aux = "";
        int rt;
        int value;
        int counter = 0;
        //int count=0;
        for (int i = 0; i < parameters_set.length; i++) {
            try {
                BufferedReader reader = new BufferedReader(new FileReader(new File(PathTrf)));
                while ((line = reader.readLine()) != null) {
                    //Check the record type located between columns 70 and 80
                    if (line.length() >= 79) {
                        for (int j = 77; j < 80; j++) {
                            line_aux += line.charAt(j);
                        }
                    }
                    rt = UTIL.Tools.stringToIntegerAux(line_aux);
                    
                    line_aux = "";
                    //Check the columns of the parameters set 
                    if (line.length() >= parameters_set[i][2] - 1) {
                        for (int j = parameters_set[i][1] - 1; j < parameters_set[i][2]; j++) {
                            line_aux += line.charAt(j);
                        }
                    }
                    value = UTIL.Tools.stringToIntegerAux(line_aux);
                    line_aux = "";
                    //Count when the values coincides with the parameters requested
                    if (rt >= 0 && value >= 0 && parameters_set[i][0] == rt) {
                        counter++;
                    }
                }
                reader.close();
            } catch (Exception e) {
                e.printStackTrace();
                return -1;
            }
        }
        return counter;
    }

    /**
     * Delete from the theta matrix the repeated rows
     *
     * @return Matrix with distinct rows
     */
    private int[][] distinct(int[][] theta) {
        //Scheme of theta   [0] Rt,     [1] SC,      [2] EC, [3] MinV,      [4] MaxV,  [5] Value, [6] lineNumber,[7] InNode, [8]EndNode

        List<int[]> listmatrix = new ArrayList<>();
        for (int i = 0; i < theta.length; i++) {
            int[] filter_i = new int[4];
            filter_i[0] = theta[i][7];
            filter_i[1] = theta[i][8];
            filter_i[2] = theta[i][0];
            filter_i[3] = 0;
            listmatrix.add(filter_i);
        }
        List<int[]> newlist = UTIL.Tools.deleteRepeatedListArrayInt(listmatrix);
        int[][] newfilter = UTIL.Tools.vectorsListToMatrixFilter(newlist);
        return newfilter;

    }

    public int[][] getFilter() {
        return filter;
    }

    public void setFilter(int[][] filter) {
        this.filter = filter;
    }

    public CorsimModel getCorsimmodel() {
        return corsimmodel;
    }

    public void setCorsimmodel(CorsimModel corsimmodel) {
        this.corsimmodel = corsimmodel;
    }

    public ParametersSet getParameters_Set() {
        return Parameters_Set;
    }

    public void setParameters_Set(ParametersSet Parameters_Set) {
        this.Parameters_Set = Parameters_Set;
    }

    public ActualDataNetsim getActual_Data_Netsim() {
        return Actual_Data_Netsim;
    }

    public void setActual_Data_Netsim(ActualDataNetsim Actual_Data_Netsim) {
        this.Actual_Data_Netsim = Actual_Data_Netsim;
    }

    public ActualDataFresim getActual_Data_Fresim() {
        return Actual_Data_Fresim;
    }

    public void setActual_Data_Fresim(ActualDataFresim Actual_Data_Fresim) {
        this.Actual_Data_Fresim = Actual_Data_Fresim;
    }

    public int getPercentageVolume() {
        return PercentageVolume;
    }

    public void setPercentageVolume(int PercentageVolume) {
        this.PercentageVolume = PercentageVolume;
    }

    public int getPercentageSpeed() {
        return PercentageSpeed;
    }

    public void setPercentageSpeed(int PercentageSpeed) {
        this.PercentageSpeed = PercentageSpeed;
    }

    public String getPathTrf() {
        return PathTrf;
    }

    public void setPathTrf(String PathTrf) {
        this.PathTrf = PathTrf;
    }
}
