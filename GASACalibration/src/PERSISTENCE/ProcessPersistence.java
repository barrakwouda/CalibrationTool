/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package PERSISTENCE;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.List;

/**
 * Manage the read and write processes for CORSIM files, the type of files
 * managed for this class is the TRF and OUT files
 *
 * @author Carlos Gaviria and Cristian Arteaga
 * @layer Persitence
 */
public class ProcessPersistence {

    /**
     * Read the netsim statistics from the output of the calibration, the method
     * go over the output file and search the specific parts where the
     * statistics are located
     *
     * @param pathtrf
     * @param size      
     * @return double[][] data_matrix, null if error.
     * @author Carlos Gaviria and Cristian Arteaga
     */
    public static double[][] extractDataNetsim(String pathtrf, int size) {
        double[][] data_matrix = new double[size][5];
        String line;
        String find = "CUMULATIVE NETSIM STATISTICS";
        boolean condition = false, flag = false;
        int counter = 0, time_period = 0;
        double value1 = 0, value2 = 0, value3 = 0;

        try {
            BufferedReader reader = new BufferedReader(new FileReader(new File(pathtrf)));
            while ((line = reader.readLine()) != null) {
                if (line.indexOf(find) != -1) {
                    reader.readLine();
                    line = reader.readLine();//Jump to neccessary place (Time Period)
                    time_period = UTIL.Tools.stringToIntegerAux(UTIL.Tools.getValue(line, 81, 81));
                    if (time_period != -1) //Verify existence of time period
                    {
                        reader.readLine();
                        reader.readLine();
                        reader.readLine();
                        reader.readLine();
                        line = reader.readLine();//Jump to neccessary place(VPH and MPH)
                        if (UTIL.Tools.getValue(line, 120, 122).equals("VPH") && UTIL.Tools.getValue(line, 126, 128).equals("MPH"))//Verify existence of titles
                        {
                            reader.readLine();
                            reader.readLine();
                            line = reader.readLine();
                            condition = UTIL.Tools.getValue(line, 1, 1).equals("("); //Condition of counter UTIL.Tools.getValue(line,118,121).equals("MPH");
                            while (condition) {
                                value1 = UTIL.Tools.stringToIntegerAux(UTIL.Tools.getValue(line, 2, 5));
                                value2 = UTIL.Tools.stringToIntegerAux(UTIL.Tools.getValue(line, 7, 10));
                                flag = (value1 != -1) && (value2 != -1);
                                if (flag) { // If the value exists in the trf file
                                    data_matrix[counter][0] = value1;
                                    data_matrix[counter][1] = value2;
                                    value3 = UTIL.Tools.stringToDoubleAux(UTIL.Tools.getValue(line, 118, 121));
                                    if (value3 != -1.0) {
                                        data_matrix[counter][2] = value3;
                                    } else {
                                        data_matrix[counter][2] = -1.0;
                                    }

                                    value3 = UTIL.Tools.stringToDoubleAux(UTIL.Tools.getValue(line, 124, 129));
                                    if (value3 != -1.0) {
                                        data_matrix[counter][3] = value3;
                                    } else {
                                        data_matrix[counter][3] = -1.0;
                                    }
                                    data_matrix[counter][4] = time_period;
                                }
                                counter++;
                                line = reader.readLine();
                                condition = UTIL.Tools.getValue(line, 1, 1).equals("(");
                            }

                        }
                    }
                }
            }
            reader.close();
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
        return data_matrix;
    }

    /**
     * Read the fresim statistics from the output of the calibration, the method
     * go over the output file and search the specific parts where the
     * statistics are located
     *
     * @param pathtrf
     * @param size
     * @return
     */
    public static double[][] extractDataFresim(String pathtrf, int size) {

        double[][] data_matrix = new double[size][5];
        String line;
        String line_aux = "";
        String find = "CUMULATIVE FRESIM STATISTICS";
        boolean condition = false, flag = false;
        int counter = 0, time_period = 0;
        double value1 = 0, value2 = 0, value3 = 0;
        try {
            BufferedReader reader = new BufferedReader(new FileReader(new File(pathtrf)));
            while ((line = reader.readLine()) != null) {
                if (line.indexOf(find) != -1) {
                    time_period = UTIL.Tools.stringToIntegerAux(UTIL.Tools.getValue(line, 80, 87));
                    reader.readLine();
                    reader.readLine();
                    reader.readLine();
                    reader.readLine();
                    reader.readLine();
                    reader.readLine();
                    reader.readLine();
                    reader.readLine();
                    line = reader.readLine();//Jump to neccessary place (Time Period)

                    if (time_period != -1) //Verify existence of time period
                    {
                        //reader.readLine();reader.readLine();reader.readLine();reader.readLine();line=reader.readLine();//Jump to neccessary place(VPH and MPH)
                        if (UTIL.Tools.getValue(line, 96, 101).equals("VOLUME") && UTIL.Tools.getValue(line, 117, 121).equals("SPEED"))//Verify existence of titles
                        {
                            reader.readLine();
                            reader.readLine();
                            reader.readLine();
                            line = reader.readLine();
                            condition = UTIL.Tools.getValue(line, 0, 0).equals("("); //Condition of counter UTIL.Tools.getValue(line,118,121).equals("MPH");
                            while (condition) {
                                value1 = UTIL.Tools.stringToIntegerAux(UTIL.Tools.getValue(line, 1, 4));
                                value2 = UTIL.Tools.stringToIntegerAux(UTIL.Tools.getValue(line, 6, 9));
                                flag = (value1 != -1) && (value2 != -1);
                                if (flag) {
                                    data_matrix[counter][0] = value1;
                                    data_matrix[counter][1] = value2;
                                    value3 = UTIL.Tools.stringToDoubleAux(UTIL.Tools.getValue(line, 95, 101));
                                    if (value3 != -1.0) {
                                        data_matrix[counter][2] = value3;
                                    } else {
                                        data_matrix[counter][2] = -1.0;
                                    }
                                    value3 = UTIL.Tools.stringToDoubleAux(UTIL.Tools.getValue(line, 116, 122));
                                    if (value3 != -1.0) {
                                        data_matrix[counter][3] = value3;
                                    } else {
                                        data_matrix[counter][3] = -1.0;
                                    }
                                    data_matrix[counter][4] = time_period;
                                }
                                counter++;
                                reader.readLine();
                                line = reader.readLine();
                                condition = UTIL.Tools.getValue(line, 0, 0).equals("(");
                            }

                        }
                    }
                }
            }
            reader.close();
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
        //Put the time period into values
        double previous = 1;
        int i = 0;
        int tp = 1;
        if (data_matrix.length > 0) {
            while (i < data_matrix.length) {
                previous = data_matrix[i][4];
                while (previous == data_matrix[i][4]) {
                    previous = data_matrix[i][4];
                    data_matrix[i][4] = tp;
                    i++;
                    if (i == data_matrix.length) {
                        return data_matrix;
                    }
                }
                tp++;
            }
        }
        return data_matrix;
    }

    /**
     * Using the parameters_set parameter extracts the specific information from
     * the trf file. This method go over the file and get the data previously
     * selected.
     *
     * @param parameters_set
     * @param pathtrf
     * @return
     */
    public static int[][] exctractParametersSet(int[][] parameters_set, String pathtrf) {
        int[][] set_of_parameters = null;
        List<int[]> parameters_matrix = new ArrayList<int[]>();
        String line, line_aux = "", nod1 = "", nod2 = "";
        int line_counter = 0, rt, value, node1, node2, file_num = 0;
        for (int i = 0; i < parameters_set.length; i++) {
            try {
                BufferedReader reader = new BufferedReader(new FileReader(new File(pathtrf)));
                //Go over the file
                while ((line = reader.readLine()) != null) {
                    line_counter++;
                    if (line.length() >= 79) {
                        for (int j = 77; j < 80; j++) {
                            line_aux += line.charAt(j);
                        }
                        nod1 = UTIL.Tools.getValue(line, 0, 3);
                        nod2 = UTIL.Tools.getValue(line, 4, 7);
                    }
                    node1 = UTIL.Tools.stringToIntegerAux(nod1);
                    node2 = UTIL.Tools.stringToIntegerAux(nod2);
                    nod1 = "";
                    nod2 = "";
                    rt = UTIL.Tools.stringToIntegerAux(line_aux);
                    if (rt == 52 || rt == 58 || rt == 68 || rt == 70 || rt == 81 || rt == 140 || rt == 141 || rt == 142 || rt == 143 || rt == 144 || rt == 145 || rt == 146 || rt == 147 || rt == 149 || rt == 153) {
                        node1 = -1;
                        node2 = -1;
                    }
                    line_aux = "";
                    if (line.length() >= parameters_set[i][2] - 1) {
                        for (int j = parameters_set[i][1] - 1; j < parameters_set[i][2]; j++) {
                            line_aux += line.charAt(j);
                        }
                    }
                    value = UTIL.Tools.stringToIntegerAux(line_aux);
                    line_aux = "";
                    if (rt >= 0 && value >= 0 && parameters_set[i][0] == rt) {
                        //Scheme of theta   [0] Rt, [1] SC, [2] EC, [3] MinV, [4] MaxV, [5] Value,  [6] lineNumber,  [7] InNode, [8] EndNode,[9] entry
                        int[] parameters_i = new int[9];
                        parameters_i[0] = parameters_set[i][0];
                        parameters_i[1] = parameters_set[i][1];
                        parameters_i[2] = parameters_set[i][2];
                        parameters_i[3] = parameters_set[i][3];
                        parameters_i[4] = parameters_set[i][4];
                        parameters_i[5] = value;
                        parameters_i[6] = line_counter;
                        parameters_i[7] = node1;
                        parameters_i[8] = node2;
                        parameters_matrix.add(parameters_i);
                        file_num++;
                    }   
                }
                reader.close();
                line_counter = 0;
            } catch (Exception e) {
                e.printStackTrace();
                return null;
            }
        }

        set_of_parameters = UTIL.Tools.vectorsListToMatrixTheta(parameters_matrix);
        return set_of_parameters;
    }
    /* Sara*/
    public static String [][] extractLinkName(String pathtrf){
        List<String[]> names_matrix = new ArrayList<String[]>();
        String[][] set_of_names = null;
        String line, line_aux = "", nod1 = "", nod2 = "";
        int rt;
        try {
                BufferedReader reader = new BufferedReader(new FileReader(new File(pathtrf)));
                //Go over the file
                while ((line = reader.readLine()) != null) {
                    if (line.length() >= 79) {
                        for (int j = 77; j < 80; j++) {
                            line_aux += line.charAt(j);
                        }
                    }
                    rt = UTIL.Tools.stringToIntegerAux(line_aux);
                    line_aux = "";
                    if (rt==10){
                        for (int j = 8; j < 20; j++) {
                            line_aux += line.charAt(j);
                        }
                        String [] names = new String[3];
                        names[0]=UTIL.Tools.getValue(line, 0, 3);
                        names[1]=UTIL.Tools.getValue(line, 4, 7);
                        names[2]=line_aux;
                        names_matrix.add(names);
                    }
                    line_aux = "";
                }
                reader.close();
            } catch (Exception e) {
                e.printStackTrace();
                return null;
            }
        set_of_names = UTIL.Tools.vectorsListToMatrixNames(names_matrix);
        return set_of_names;
    }

    /**
     *
     * @param parameters_set
     * @param pathtrf
     * @param size
     * @return
     */
    public static int[][] exctractParametersSetOld(int[][] parameters_set, String pathtrf, int size) {
        int[][] set_of_parameters = new int[size][9];
        String line, line_aux = "", nod1 = "", nod2 = "";
        int line_counter = 0, rt, value, node1, node2, file_num = 0;
        int[] parameters_i = new int[9];
        for (int i = 0; i < parameters_set.length; i++) {
            try {
                BufferedReader reader = new BufferedReader(new FileReader(new File(pathtrf)));
                while ((line = reader.readLine()) != null) {
                    line_counter++;
                    if (line.length() >= 79) {
                        for (int j = 77; j < 80; j++) {
                            line_aux += line.charAt(j);
                        }
                        for (int j = 0; j < 3; j++) {
                            nod1 += line.charAt(j);
                        }
                        for (int j = 4; j < 7; j++) {
                            nod2 += line.charAt(j);
                        }
                    }
                    node1 = UTIL.Tools.stringToIntegerAux(nod1);
                    node2 = UTIL.Tools.stringToIntegerAux(nod2);
                    rt = UTIL.Tools.stringToIntegerAux(line_aux);
                    nod1 = "";
                    nod2 = "";
                    line_aux = "";
                    if (line.length() >= parameters_set[i][2] - 1) {
                        for (int j = parameters_set[i][1] - 1; j < parameters_set[i][2]; j++) {
                            line_aux += line.charAt(j);
                        }
                    }
                    value = UTIL.Tools.stringToIntegerAux(line_aux);
                    line_aux = "";
                    if (rt >= 0 && value >= 0 && parameters_set[i][0] == rt) {
                        set_of_parameters[file_num][0] = parameters_set[i][0];
                        set_of_parameters[file_num][1] = parameters_set[i][1];
                        set_of_parameters[file_num][2] = parameters_set[i][2];
                        set_of_parameters[file_num][3] = parameters_set[i][3];
                        set_of_parameters[file_num][4] = parameters_set[i][4];
                        set_of_parameters[file_num][5] = value;
                        set_of_parameters[file_num][6] = line_counter;
                        set_of_parameters[file_num][7] = node1;
                        set_of_parameters[file_num][8] = node2;
                        file_num++;
                    }
                }
                reader.close();
                line_counter = 0;
            } catch (Exception e) {
                e.printStackTrace();
                return null;
            }
        }
        return set_of_parameters;
    }

    /**
     * Write the given set of parameters (data) into a file (pathtrf),
     * considering the information about line number, start and end column.
     *
     * @param data
     * @param pathtrf
     * @return true if successfull, false if occurs a fail
     * @author Carlos Gaviria and Cristian Arteaga
     */
    public static boolean writeParametersSet(int[][] data, String pathtrf) {
        try {
            String line = "";
            String[] buffer = UTIL.Tools.fileToBuffer(pathtrf);
            for (int i = 0; i < data.length; i++) {
                line = buffer[data[i][6] - 1];
//                 System.out.println(data[i][6]+" "+line+"_RT"+data[i][0]);
                String new_line = UTIL.Tools.setValue(line, data[i][5], data[i][1], data[i][2]);
                buffer[data[i][6] - 1] = new_line;
            }
            return UTIL.Tools.bufferToFile(buffer, pathtrf);

        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    /**
     * counts the number of parameters indicated by parameters_set in the .trf
     * file
     *
     * @param parameters_set
     * @param pathtrf
     * @return int counter, -1 if found some error
     * @author Carlos Gaviria and Cristian Arteaga
     */
    public static int countParametersSet(int[][] parameters_set, String pathtrf) {
        String line;
        String line_aux = "";
        int rt;
        int value;
        int counter = 0;
        //int count=0;
        for (int i = 0; i < parameters_set.length; i++) {
            try {
                BufferedReader reader = new BufferedReader(new FileReader(new File(pathtrf)));
                while ((line = reader.readLine()) != null) {
                    //count++;
                    if (line.length() >= 79) {
                        for (int j = 77; j < 80; j++) {
                            line_aux += line.charAt(j);
                        }
                    }
                    rt = UTIL.Tools.stringToIntegerAux(line_aux);
                    line_aux = "";
                    //System.out.println("Linea="+line);
                    if (line.length() >= parameters_set[i][2] - 1) {
                        for (int j = parameters_set[i][1] - 1; j < parameters_set[i][2]; j++) {
                            line_aux += line.charAt(j);
                        }
                    }
                    value = UTIL.Tools.stringToIntegerAux(line_aux);
                    line_aux = "";
                    if (rt >= 0 && value >= 0 && parameters_set[i][0] == rt) {
                        counter++;
                    }
                }
                reader.close();
            } catch (Exception e) {
                e.printStackTrace();
                return -1;
            }
        }
        return counter;
    }

    /**
     * Count the quantity of values availables to Netsim from the file located
     * in pathtrf
     *
     * @param pathtrf
     * @return int counter, -1 if error.
     * @author Carlos Gaviria and Cristian Arteaga
     */
    public static int countDataNetsim(String pathtrf) {
        String line;
        String line_aux = "";
        String find = "CUMULATIVE NETSIM STATISTICS";
        boolean condition = false;
        int counter = 0, time_period = 0;
        double value1 = 0, value2 = 0;

        try {
            BufferedReader reader = new BufferedReader(new FileReader(new File(pathtrf)));
            while ((line = reader.readLine()) != null) {
                if (line.indexOf(find) != -1) {
                    line = reader.readLine();
                    line = reader.readLine();//Jump to neccessary place (Time Period)
                    time_period = UTIL.Tools.stringToIntegerAux(UTIL.Tools.getValue(line, 81, 81));
                    if (time_period != -1) //Verify existence of time period
                    {
                        reader.readLine();
                        reader.readLine();
                        reader.readLine();
                        reader.readLine();
                        line = reader.readLine();//Jump to neccessary place(VPH and MPH)
                        if (UTIL.Tools.getValue(line, 120, 122).equals("VPH") && UTIL.Tools.getValue(line, 126, 128).equals("MPH"))//Verify existence of titles
                        {
                            reader.readLine();
                            reader.readLine();
                            line = reader.readLine();
                            value1 = UTIL.Tools.stringToIntegerAux(UTIL.Tools.getValue(line, 2, 5));
                            value2 = UTIL.Tools.stringToIntegerAux(UTIL.Tools.getValue(line, 7, 10));
                            condition = UTIL.Tools.getValue(line, 1, 1).equals("(") && (value1 != -1.0) && (value2 != -1.0); //Condition of counter UTIL.Tools.getValue(line,118,121).equals("MPH");
                           
                            while (condition) {
                                counter++;
                                line = reader.readLine();
                                value1 = UTIL.Tools.stringToIntegerAux(UTIL.Tools.getValue(line, 2, 5));
                                value2 = UTIL.Tools.stringToIntegerAux(UTIL.Tools.getValue(line, 7, 10));
                                condition = UTIL.Tools.getValue(line, 1, 1).equals("(") && (value1 != -1.0) && (value2 != -1.0);
                            }

                        }
                    }
                }
            }
            reader.close();
        } catch (Exception e) {
            e.printStackTrace();
            return -1;
        }
        return counter;
    }

    /**
     * Count the quantity of values availables to Fresim from the file located
     * in pathtrf
     *
     * @param pathtrf
     * @return int counter, null if error.
     * @author Carlos Gaviria and Cristian Arteaga
     */
    public static int countDataFresim(String pathtrf) {
        String line;
        String line_aux = "";
        String find = "CUMULATIVE FRESIM STATISTICS";
        boolean condition = false;
        int counter = 0, time_period = 0;
        float value1 = 0, value2 = 0;

        try {
            BufferedReader reader = new BufferedReader(new FileReader(new File(pathtrf)));
            while ((line = reader.readLine()) != null) {
               // System.err.println("line.indexOf(find) = "+line.indexOf(find));
                if (line.indexOf(find) != -1) {
                    reader.readLine();
                    reader.readLine();
                    reader.readLine();
                    reader.readLine();
                    reader.readLine();
                    reader.readLine();
                    reader.readLine();
                    reader.readLine();
                    line = reader.readLine();//Jump to neccessary place (Time Period)
                    time_period = 1;//UTIL.Tools.stringToIntegerAux(UTIL.Tools.getValue(line,81,81));
                    if (time_period != -1) //Verify existence of time period
                    {
                        //reader.readLine();reader.readLine();reader.readLine();reader.readLine();line=reader.readLine();//Jump to neccessary place(VPH and MPH)
                        if (UTIL.Tools.getValue(line, 96, 101).equals("VOLUME") && UTIL.Tools.getValue(line, 117, 121).equals("SPEED"))//Verify existence of titles
                        {
                            reader.readLine();
                            reader.readLine();
                            reader.readLine();
                            line = reader.readLine();
                            value1 = UTIL.Tools.stringToIntegerAux(UTIL.Tools.getValue(line, 1, 4));
                            value2 = UTIL.Tools.stringToIntegerAux(UTIL.Tools.getValue(line, 6, 9));
                            condition = UTIL.Tools.getValue(line, 0, 0).equals("(") && (value1 != -1.0) && (value2 != -1.0); //Condition of counter UTIL.Tools.getValue(line,118,121).equals("MPH");
                          
                            while (condition) {
                                counter++;
                                reader.readLine();
                                line = reader.readLine();
                                value1 = UTIL.Tools.stringToIntegerAux(UTIL.Tools.getValue(line, 1, 4));
                                value2 = UTIL.Tools.stringToIntegerAux(UTIL.Tools.getValue(line, 6, 9));
                                condition = UTIL.Tools.getValue(line, 0, 0).equals("(") && (value1 != -1.0) && (value2 != -1.0);
                            }

                        }
                    }
                }
            }
            reader.close();
        } catch (Exception e) {
            e.printStackTrace();
            return -1;
        }
        return counter;
    }
}
