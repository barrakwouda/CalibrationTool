package GUI;

import FACADE.Facade;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.File;
import java.util.List;
import javax.swing.ImageIcon;
import javax.swing.JOptionPane;

public class Expirated extends javax.swing.JFrame {

    Facade facade = new Facade();
    private String path_trf;
    private String vol_weight = "";
    private String speed_weight = "";
    private String[][] parameters_set;
    private String[][] actual_data_netsim;
    private String[][] actual_data_fresim;
    private boolean check_path_trf = false;
    private boolean check_vol_weight = false;
    private boolean check_speed_weight = false;
    private boolean check_parameters_set = false;
    private boolean check_extracted_parameters = false;
    private boolean button1On = true;
    private boolean button2On = false;
    private boolean button3On = false;
    private boolean button4On = false;
    private boolean button5On = false;
    /*Algorithm Settings*/
    String algorithm;
    //SPSA
    private double A;
    private double Alpha;
    private double Gamma;
    private double a;
    private double c;
    //GA
    private double Pop;
    private double Gen;
    private double CrossPer;
    private double SelPer;
    private double Step;
    private double Fat;
    private double MutPer;
    //SA
    private double IniTemp;
    private double CoolRate;
    //TS
    private double Tenure;
    private double NeighSize;

    public Expirated() {
        initComponents();

        setLocationRelativeTo(null);
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel11 = new javax.swing.JLabel();
        jLabel12 = new javax.swing.JLabel();
        jLabel13 = new javax.swing.JLabel();
        jMenuBar1 = new javax.swing.JMenuBar();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("Calibration Tool");

        jLabel11.setFont(new java.awt.Font("Tahoma", 1, 24)); // NOI18N
        jLabel11.setText("The license has expired. ");

        jLabel12.setText("You have use the max number of calibrations.");

        jLabel13.setText("Please contact the administrator of your system.");
        setJMenuBar(jMenuBar1);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(56, 56, 56)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel13)
                    .addComponent(jLabel12)
                    .addComponent(jLabel11))
                .addContainerGap(25, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(36, 36, 36)
                .addComponent(jLabel11)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jLabel12)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel13)
                .addContainerGap(71, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    public static void main(String args[]) {

        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Expirated.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Expirated.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Expirated.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Expirated.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                if(UTIL.Tools.expirated()){

                
                }else{
                new Expirated().setVisible(true);
                }
            }
        });
    }
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JMenuBar jMenuBar1;
    // End of variables declaration//GEN-END:variables

    /**
     * @return the A
     */
    public double getA() {
        return A;
    }

    /**
     * @param A the A to set
     */
    public void setA(double A) {
        this.A = A;
    }

    /**
     * @return the Alpha
     */
    public double getAlpha() {
        return Alpha;
    }

    /**
     * @param Alpha the Alpha to set
     */
    public void setAlpha(double Alpha) {
        this.Alpha = Alpha;
    }

    /**
     * @return the Gamma
     */
    public double getGamma() {
        return Gamma;
    }

    /**
     * @param Gamma the Gamma to set
     */
    public void setGamma(double Gamma) {
        this.Gamma = Gamma;
    }

    /**
     * @return the a
     */
    public double geta() {
        return a;
    }

    /**
     * @param a the a to set
     */
    public void seta(double a) {
        this.a = a;
    }

    /**
     * @return the c
     */
    public double getC() {
        return c;
    }

    /**
     * @param c the c to set
     */
    public void setC(double c) {
        this.c = c;
    }

    /**
     * @return the Pop
     */
    public double getPop() {
        return Pop;
    }

    /**
     * @param Pop the Pop to set
     */
    public void setPop(double Pop) {
        this.Pop = Pop;
    }

    /**
     * @return the Gen
     */
    public double getGen() {
        return Gen;
    }

    /**
     * @param Gen the Gen to set
     */
    public void setGen(double Gen) {
        this.Gen = Gen;
    }

    /**
     * @return the CrossPer
     */
    public double getCrossPer() {
        return CrossPer;
    }

    /**
     * @param CrossPer the CrossPer to set
     */
    public void setCrossPer(double CrossPer) {
        this.CrossPer = CrossPer;
    }

    /**
     * @return the SelPer
     */
    public double getSelPer() {
        return SelPer;
    }

    /**
     * @param SelPer the SelPer to set
     */
    public void setSelPer(double SelPer) {
        this.SelPer = SelPer;
    }

    /**
     * @return the Step
     */
    public double getStep() {
        return Step;
    }

    /**
     * @param Step the Step to set
     */
    public void setStep(double Step) {
        this.Step = Step;
    }

    /**
     * @return the Fat
     */
    public double getFat() {
        return Fat;
    }

    /**
     * @param Fat the Fat to set
     */
    public void setFat(double Fat) {
        this.Fat = Fat;
    }

    /**
     * @return the MutPer
     */
    public double getMutPer() {
        return MutPer;
    }

    /**
     * @param MutPer the MutPer to set
     */
    public void setMutPer(double MutPer) {
        this.MutPer = MutPer;
    }

    /**
     * @return the IniTemp
     */
    public double getIniTemp() {
        return IniTemp;
    }

    /**
     * @param IniTemp the IniTemp to set
     */
    public void setIniTemp(double IniTemp) {
        this.IniTemp = IniTemp;
    }

    /**
     * @return the CoolRate
     */
    public double getCoolRate() {
        return CoolRate;
    }

    /**
     * @param CoolRate the CoolRate to set
     */
    public void setCoolRate(double CoolRate) {
        this.CoolRate = CoolRate;
    }

    /**
     * @return the Tenure
     */
    public double getTenure() {
        return Tenure;
    }

    /**
     * @param Tenure the Tenure to set
     */
    public void setTenure(double Tenure) {
        this.Tenure = Tenure;
    }

    /**
     * @return the NeighSize
     */
    public double getNeighSize() {
        return NeighSize;
    }

    /**
     * @param NeighSize the NeighSize to set
     */
    public void setNeighSize(double NeighSize) {
        this.NeighSize = NeighSize;
    }

    public void setAlgorithm(String alg) {
        this.algorithm = alg;
    }

    public String getAlgorithm() {
        return this.algorithm;
    }
}
