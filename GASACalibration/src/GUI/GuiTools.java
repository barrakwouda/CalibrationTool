
package GUI;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;
import javax.swing.JFileChooser;
import javax.swing.JTable;
import javax.swing.filechooser.FileNameExtensionFilter;

public class GuiTools {
    
    public static String openDialog(String ext){
        JFileChooser f_c = new JFileChooser();
        FileNameExtensionFilter Filter = new FileNameExtensionFilter(ext,ext);
        f_c.setFileFilter(Filter);
        int selection = f_c.showOpenDialog(null);
        if(selection == JFileChooser.APPROVE_OPTION){
            return f_c.getSelectedFile().getAbsolutePath();
        }else{
            return "";
        }        
    }
    
    public static String saveDialog(String ext){
        JFileChooser f_c = new JFileChooser();
        FileNameExtensionFilter Filter = new FileNameExtensionFilter(ext,ext);
        f_c.setFileFilter(Filter);
        int selection = f_c.showSaveDialog(null);
        if(selection == JFileChooser.APPROVE_OPTION){
            if(f_c.getSelectedFile().getAbsolutePath().endsWith("."+ext)){
                return f_c.getSelectedFile().getAbsolutePath();
            }else{
                return f_c.getSelectedFile().getAbsolutePath()+"."+ext;
            }            
        }else{
            return "";
        }        
    }
    
    public static boolean rowIsRepeated(String[] row, JTable table){
        
        boolean flag;
        for(int i=0;i<table.getRowCount();i++){
            flag=true;
            for(int j=0;j<3;j++){
                if(stringToIntegerAux(table.getValueAt(i, j).toString())!=stringToIntegerAux(row[j])){
                    flag=false;
                }
            }
            if(flag)
                return true;
        }
        return false;        
    }
    
    //null its ok in validation
    public static String[] validIntegerMatrix(JTable table){
        String[] output = new String[5];
        int[] aux = new int[5];
        for(int i=0; i<table.getRowCount();i++){
            for(int j=0;j<5;j++){
                aux[j]=stringToIntegerAux(table.getValueAt(i, j).toString());
                if(stringToIntegerAux(table.getValueAt(i, j).toString())==-1
                        || stringToIntegerAux(table.getValueAt(i, j).toString())<0){
                    output[0]=table.getValueAt(i, 0).toString();
                    output[1]=table.getValueAt(i, 1).toString();
                    output[2]=table.getValueAt(i, 2).toString();
                    output[3]=table.getValueAt(i, 3).toString();
                    output[4]=table.getValueAt(i, 4).toString(); 
                    return output;
                }
                if((aux[1]>aux[2] || aux[3]>aux[4] || aux[0]<0 || aux[0]>210)&&(j==4)){
                    
                    output[0]=""+aux[0];
                    output[1]=""+aux[1];
                    output[2]=""+aux[2];
                    output[3]=""+aux[3];
                    output[4]=""+aux[4];
                    return output;
                }
            }
        }
        return null;
    }
    
    public static int stringToIntegerAux(String st){
        String line_aux="";
        int number;
        StringTokenizer clean_text = new StringTokenizer(st);
        while (clean_text.hasMoreElements())
            line_aux += clean_text.nextElement();
        try{
            number = Integer.parseInt(line_aux);
            return number;
        }catch(NumberFormatException nfe){
            return -1;
        }
    }
    
    public static String[][] jtableToMatrix(JTable table){
        String[][] matrix = new String[table.getRowCount()][5];
        for(int i=0;i<table.getRowCount();i++){
            for(int j=0;j<5;j++){
                matrix[i][j]=table.getValueAt(i, j).toString();
            }
        }
        return matrix;       
    }
    
    public static void saveTemp(List L){
        try
        {
            ObjectOutputStream writer = new ObjectOutputStream(new FileOutputStream("temp.tmp"));
            writer.writeObject(L);
            writer.close();            
        }
        catch(Exception e)
        {           
            e.printStackTrace();
        }        
    }
    
    public static List loadTemp(){
        try
        {
            ObjectInputStream reader = new ObjectInputStream(new FileInputStream("temp.tmp"));
            List data = (List)reader.readObject();
            reader.close();
            return data;
        }
        catch(Exception e)
        {
            e.printStackTrace();
            return null;
        }
    }
    
    public static int countTimePeriods(double[][] data){    
        int count=0;
        try{
            int past=(int)data[0][4];
            for(int i=0;i<data.length;i++){            
                if(past!=(int)data[i][4]){
                    count++;
                }
                past = (int)data[i][4];            
            }       
            return count+1;
        }catch(Exception ex){
            return 0;
        }
    }
    
    public static ArrayList<double[][]> returnBidArrays(double[][] data){
        ArrayList<double[][]> ListBidArrays = new ArrayList();
        ArrayList<double[]> AuxMatrix = new ArrayList();        
        try{
            int past=(int)data[0][4];
            for(int i=0;i<data.length;i++){                                        
                if(past!=(int)data[i][4]){
                    ListBidArrays.add(listArrayToMatrixAux(AuxMatrix));
                    AuxMatrix = new ArrayList();  
                }
                double[] aux = new double[5];
                aux[0]=data[i][0];aux[1]=data[i][1];aux[2]=data[i][2];aux[3]=data[i][3];aux[4]=data[i][4];
                AuxMatrix.add(aux);  
                past = (int)data[i][4];         
                if(i==data.length-1){
                    ListBidArrays.add(listArrayToMatrixAux(AuxMatrix));
                }
            }       
            return ListBidArrays;
        }catch(Exception ex){
            return null;
        }                
    }   
    
    public static double[][] listArrayToMatrixAux(ArrayList<double[]> data){
        double[][] output = new double[data.size()][5];
        for(int i=0;i<data.size();i++){
            output[i][0]=data.get(i)[0];
            output[i][1]=data.get(i)[1];
            output[i][2]=data.get(i)[2];
            output[i][3]=data.get(i)[3];
            output[i][4]=data.get(i)[4];
        }
        return output;
    }
    
    public static String[][] doubleArrToStringArrNew(double[][] data){
        try{
            String[][] output = new String[data.length][5];
            for(int i=0;i<data.length;i++){
                for(int j=0;j<5;j++){
                    if(j==0 || j==1)
                        output[i][j]=""+(int)data[i][j];
                    if(j==2 || j==3){
                        output[i][j]=""+data[i][j];
                        if(((int)data[i][j])!=(-1)){
                            output[i][j]="-1";
                        }
                    }
                    if((int)data[i][j]==-1)
                        output[i][j]=""+(int)data[i][j];
                    if(j==4)
                        output[i][j]=""+(int)data[i][j];
                }
            }
            return output;
        }catch(Exception ex){
            return null;
        }
    }
    
    public static String[][] doubleArrToStringArr(double[][] data){
        try{
            String[][] output = new String[data.length][5];
            for(int i=0;i<data.length;i++){
                for(int j=0;j<5;j++){
                    if(j==0 || j==1)
                        output[i][j]=""+(int)data[i][j];
                    if(j==2 || j==3){
                        output[i][j]=""+data[i][j];                        
                    }
                    if((int)data[i][j]==-1)
                        output[i][j]=""+(int)data[i][j];
                    if(j==4)
                        output[i][j]=""+(int)data[i][j];
                }
            }
            return output;
        }catch(Exception ex){
            return null;
        }
    }
    
    public static double[][] jTableToDoubleBidArr(ArrayList<JTable> jTableList){
        int total_rows=0;
        int row_index=0;
        
        for(int i=0;i<jTableList.size();i++){
            total_rows += jTableList.get(i).getRowCount();
        }       
        
        double[][] data = new double[total_rows][5];

        for(int i=0;i<jTableList.size();i++){
            for(int j=0;j<jTableList.get(i).getRowCount();j++){
                //System.out.println();
                for(int k=0;k<jTableList.get(i).getColumnCount();k++){
                    //System.out.print(Double.parseDouble(jTableList.get(i).getValueAt(j, k).toString())+", ");
                    data[row_index][k]=Double.parseDouble(jTableList.get(i).getValueAt(j, k).toString());
                    /*if(k==jTableList.get(i).getColumnCount()){
                        data[row_index][k]=i+1;
                    }*/
                    data[row_index][4]=i+1;
                }
                row_index++;
            }            
        }               
        return data;
    }
    
    public static String[][] doubleBidArrToStrArray(double[][] data){
        String[][] output = new String[data.length][5];
        for(int i=0;i<output.length;i++){
            for(int j=0;j<5;j++){
                output[i][j]=""+data[i][j];
            }
        }
        return output;
    }
    
    public static double[][] stringBidArrToDoubleArray(String[][] data){
        double[][] output = new double[data.length][5];
        for(int i=0;i<output.length;i++){
            for(int j=0;j<5;j++){
                output[i][j]=Double.parseDouble(data[i][j]);
            }
        }
        return output;
    }
    
    public static ArrayList<double[][]> returnBidArraysGraph(double[][] data){
        ArrayList<double[][]> ListBidArrays = new ArrayList();
        ArrayList<double[]> AuxMatrix = new ArrayList();        
        try{
            int past=(int)data[0][2];
            for(int i=0;i<data.length;i++){                                        
                if(past!=(int)data[i][2]){
                    ListBidArrays.add(listArrayToMatrixAux(AuxMatrix));
                    AuxMatrix = new ArrayList();  
                }
                double[] aux = new double[3];
                aux[0]=data[i][0];aux[1]=data[i][1];aux[2]=data[i][2];
                AuxMatrix.add(aux);  
                past = (int)data[i][2];         
                if(i==data.length-1){
                    ListBidArrays.add(listArrayToMatrixAux(AuxMatrix));
                }
            }       
            return ListBidArrays;
        }catch(Exception ex){
            return null;
        }                
    }
    
    public static int maxRangeGraph(double[][] data){
        double max=0;
        for(int i=0;i<data.length;i++){
            if(data[i][0]!=-1){                
                if(data[i][0]>max){
                    max=(int)data[i][0];
                }                 
            }
        } 
        
        for(int i=0;i<data.length;i++){
            if(data[i][1]!=-1){                
                if(data[i][1]>max){
                    max=(int)data[i][1];
                }                 
            }
        }    
        
        return (int)max+1;
    }
    
    public static double stringToDoubleAux(String st){
        String line_aux="";
        double number;
        StringTokenizer clean_text = new StringTokenizer(st);
        while (clean_text.hasMoreElements())
            line_aux += clean_text.nextElement();
        try{
            number = Double.parseDouble(line_aux);
            return number;
        }catch(NumberFormatException nfe){
            return -1;
        }
    }
    
    public static double getMax(double[][] data,String eje){
        double major = 0;
        if(data == null){
            return -1.0;
        }
        int line=0;
        if(eje.equals("x")){
            line = 0;
        }
        if(eje.equals("y")){
            line = 1;
        }
        if(data.length>0){
            major = data[0][line];
            for(int i=1; i<data.length;i++){
                if(data[i][line] > major){
                    major = data[i][line];
                }
            }        
        }
        return major;
    }
    
}
