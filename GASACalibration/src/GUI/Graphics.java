package GUI;


import java.awt.BasicStroke;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Shape;
import java.awt.geom.Line2D;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;
import javax.swing.JFrame;
import org.jfree.chart.*;
import org.jfree.chart.annotations.XYShapeAnnotation;
import org.jfree.chart.axis.NumberAxis;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.plot.XYPlot;
import org.jfree.chart.renderer.xy.XYItemRenderer;
import org.jfree.data.xy.XYDataset;
import org.jfree.data.xy.XYSeries;
import org.jfree.data.xy.XYSeriesCollection;
import org.jfree.util.ShapeUtilities;

public class Graphics extends JFrame {
    
    private static final int SIZE = 500;
    private static String graph_title;
    private static String X_Title;
    private static String Y_Title;
    private static int maxX_Y;
    private static double[][] data;

    public Graphics(String window_tittle, String graph_tittle, String X_Title, String Y_Title,
            int maxX_Y,double[][] data) {        
        super(window_tittle);
        Graphics.graph_title=graph_tittle;
        Graphics.X_Title=X_Title;
        Graphics.Y_Title=Y_Title;
        Graphics.maxX_Y=maxX_Y;
        Graphics.data=data;
        final ChartPanel chartPanel = createDemoPanel();
        chartPanel.setPreferredSize(new Dimension(SIZE, SIZE));
        this.add(chartPanel, BorderLayout.CENTER);
    }

    private ChartPanel createDemoPanel() {
        JFreeChart jfreechart = ChartFactory.createScatterPlot(
            graph_title, X_Title, Y_Title,createSampleData(),
            PlotOrientation.VERTICAL, false, true, false);
        XYPlot xyPlot = (XYPlot) jfreechart.getPlot();
        XYItemRenderer renderer = xyPlot.getRenderer();
        renderer.setSeriesPaint(0, Color.blue);
        Shape dot = ShapeUtilities.createDiamond(3);
        renderer.setSeriesShape(0, dot);
        adjustAxis((NumberAxis) xyPlot.getDomainAxis(), true);
        adjustAxis((NumberAxis) xyPlot.getRangeAxis(), false);
        xyPlot.addAnnotation(new XYShapeAnnotation(
            new Line2D.Double(0, 0, maxX_Y, maxX_Y), new BasicStroke(1.0f,
            BasicStroke.CAP_BUTT, BasicStroke.JOIN_MITER, 10.0f), Color.black));
        
        try {
            BufferedImage img = jfreechart.createBufferedImage(500, 500);
            OutputStream out = new FileOutputStream(new File(UTIL.Log.getFolder()+File.separator+"Graphs"+File.separator+graph_title+".png"));
            ImageIO.write(img, "png", out);
            out.close();
            
        } catch (FileNotFoundException ex) {
            Logger.getLogger(Graphics.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(Graphics.class.getName()).log(Level.SEVERE, null, ex);
        }
        return new ChartPanel(jfreechart);
    }

    private void adjustAxis(NumberAxis axis, boolean vertical) {
        axis.setRange(0, maxX_Y);        
        axis.setVerticalTickLabels(vertical);
    }

    private XYDataset createSampleData() {
        XYSeriesCollection xySeriesCollection = new XYSeriesCollection();
        XYSeries series = new XYSeries("Random");
        for(int i=0;i<data.length;i++){
            series.add(data[i][0],data[i][1]);
        }        
        xySeriesCollection.addSeries(series);        
        return xySeriesCollection;
    }
    
}