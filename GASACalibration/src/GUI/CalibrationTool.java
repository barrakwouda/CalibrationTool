package GUI;

import DOMAIN.Model.CorsimModel;
import FACADE.Facade;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.ImageIcon;
import javax.swing.JOptionPane;

public class CalibrationTool extends javax.swing.JFrame {

    Facade facade = new Facade();
    private String path_trf;
    private String vol_weight = "";
    private String speed_weight = "";
    private String[][] parameters_set;
    private String[][] actual_data_netsim;
    private String[][] actual_data_fresim;
    private boolean check_path_trf = false;
    private boolean check_vol_weight = false;
    private boolean check_speed_weight = false;
    private boolean check_parameters_set = false;
    private boolean check_extracted_parameters = false;
    private boolean button1On = true;
    private boolean button2On = false;
    private boolean button3On = false;
    private boolean button4On = false;
    private boolean button5On = false;
    private boolean button6On = false;
    /*Algorithm Settings*/
    String algorithm;
    //SPSA
    private double A;
    private double Alpha;
    private double Gamma;
    private double a;
    private double c;
    //GA
    private double Pop;
    private double Gen;
    private double CrossPer;
    private double SelPer;
    private double Step;
    private double Fat;
    private double MutPer;
    //SA
    private double IniTemp;
    private double CoolRate;
    //TS
    private double Tenure;
    private double NeighSize;
    //Simulator
    private int simulatorRuns;
    //LOG
    private String filelog;

    public CalibrationTool() {
        initComponents();
//getClass().getResource("/GUI/img/graph_icon.png")
        UTIL.Tools.initializeEntriesDesc();
        status_1_check.setIcon(new ImageIcon(getClass().getResource("/GUI/img/lock_icon.png")));
        jLabel2.setIcon(new ImageIcon(getClass().getResource("/GUI/img/lock_icon.png")));
        jLabel3.setIcon(new ImageIcon(getClass().getResource("/GUI/img/lock_icon.png")));
        jLabel9.setIcon(new ImageIcon(getClass().getResource("/GUI/img/lock_icon.png")));
        jLabel10.setIcon(new ImageIcon(getClass().getResource("/GUI/img/lock_icon.png")));
        jLabel06.setIcon(new ImageIcon(getClass().getResource("/GUI/img/lock_icon.png")));
        jButton1.setEnabled(true);
        jButton2.setEnabled(false);
        jButton3.setEnabled(false);
        jButton4.setEnabled(false);
        jButton5.setEnabled(false);
        jButton6.setEnabled(false);
        jButton7.setVisible(false);
        jButton8.setVisible(false);
        jButton9.setVisible(false);
        jButton10.setVisible(false);
        jButton11.setVisible(false);
//        this.filelog =    "CalibrationToolLog" +UTIL.Tools.actualTimestamp("dd_MM_yy_h_mm_ss_S") + ".txt";
        setLocationRelativeTo(null);
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel1 = new javax.swing.JLabel();
        status_1_check = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        jButton1 = new javax.swing.JButton();
        jButton2 = new javax.swing.JButton();
        jButton3 = new javax.swing.JButton();
        jButton4 = new javax.swing.JButton();
        jLabel7 = new javax.swing.JLabel();
        jButton5 = new javax.swing.JButton();
        jLabel8 = new javax.swing.JLabel();
        jLabel9 = new javax.swing.JLabel();
        jLabel10 = new javax.swing.JLabel();
        jLabel11 = new javax.swing.JLabel();
        jButton6 = new javax.swing.JButton();
        jLabel12 = new javax.swing.JLabel();
        jLabel06 = new javax.swing.JLabel();
        jButton7 = new javax.swing.JButton();
        jButton8 = new javax.swing.JButton();
        jButton9 = new javax.swing.JButton();
        jButton10 = new javax.swing.JButton();
        jButton11 = new javax.swing.JButton();
        jMenuBar1 = new javax.swing.JMenuBar();
        jMenu1 = new javax.swing.JMenu();
        jMenuItem1 = new javax.swing.JMenuItem();
        jMenuItem2 = new javax.swing.JMenuItem();
        jMenuItem3 = new javax.swing.JMenuItem();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("Calibration Tool");

        jLabel1.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel1.setText("Status");

        jLabel4.setText("1. Select a .trf file");

        jLabel5.setText("2. Parameters selection");

        jLabel6.setText("4. Actual data");

        jButton1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/GUI/img/trf_icon.png"))); // NOI18N
        jButton1.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jButton1MouseClicked(evt);
            }
        });

        jButton2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/GUI/img/selection_icon.png"))); // NOI18N
        jButton2.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jButton2MouseClicked(evt);
            }
        });
        jButton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton2ActionPerformed(evt);
            }
        });

        jButton3.setIcon(new javax.swing.ImageIcon(getClass().getResource("/GUI/img/real_icon.png"))); // NOI18N
        jButton3.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jButton3MouseClicked(evt);
            }
        });

        jButton4.setIcon(new javax.swing.ImageIcon(getClass().getResource("/GUI/img/run_icon.png"))); // NOI18N
        jButton4.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jButton4MouseClicked(evt);
            }
        });

        jLabel7.setText("5. Run calibration");

        jButton5.setIcon(new javax.swing.ImageIcon(getClass().getResource("/GUI/img/graph_icon.png"))); // NOI18N
        jButton5.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jButton5MouseClicked(evt);
            }
        });
        jButton5.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton5ActionPerformed(evt);
            }
        });

        jLabel8.setText("6. Print results");

        jLabel11.setFont(new java.awt.Font("Tahoma", 1, 24)); // NOI18N
        jLabel11.setText("Calibration tool");

        jButton6.setIcon(new javax.swing.ImageIcon(getClass().getResource("/GUI/img/node_icon.png"))); // NOI18N
        jButton6.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jButton6MouseClicked(evt);
            }
        });
        jButton6.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton6ActionPerformed(evt);
            }
        });

        jLabel12.setText("3. Node & RT Selection");

        jButton7.setForeground(new java.awt.Color(0, 51, 255));
        jButton7.setIcon(new javax.swing.ImageIcon(getClass().getResource("/GUI/img/Back.png"))); // NOI18N
        jButton7.setText("Back");
        jButton7.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton7ActionPerformed(evt);
            }
        });

        jButton8.setForeground(new java.awt.Color(0, 51, 255));
        jButton8.setIcon(new javax.swing.ImageIcon(getClass().getResource("/GUI/img/Back.png"))); // NOI18N
        jButton8.setText("Back");
        jButton8.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton8ActionPerformed(evt);
            }
        });

        jButton9.setForeground(new java.awt.Color(0, 51, 255));
        jButton9.setIcon(new javax.swing.ImageIcon(getClass().getResource("/GUI/img/Back.png"))); // NOI18N
        jButton9.setText("Back");
        jButton9.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton9ActionPerformed(evt);
            }
        });

        jButton10.setForeground(new java.awt.Color(0, 51, 255));
        jButton10.setIcon(new javax.swing.ImageIcon(getClass().getResource("/GUI/img/Back.png"))); // NOI18N
        jButton10.setText("Back");
        jButton10.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton10ActionPerformed(evt);
            }
        });

        jButton11.setForeground(new java.awt.Color(0, 51, 255));
        jButton11.setIcon(new javax.swing.ImageIcon(getClass().getResource("/GUI/img/Back.png"))); // NOI18N
        jButton11.setText("Back");
        jButton11.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton11ActionPerformed(evt);
            }
        });

        jMenu1.setText("Tools");

        jMenuItem1.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_P, java.awt.event.InputEvent.CTRL_MASK));
        jMenuItem1.setText("Parameters editor");
        jMenuItem1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem1ActionPerformed(evt);
            }
        });
        jMenu1.add(jMenuItem1);

        jMenuItem2.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_A, java.awt.event.InputEvent.CTRL_MASK));
        jMenuItem2.setText("Actual data editor");
        jMenuItem2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem2ActionPerformed(evt);
            }
        });
        jMenu1.add(jMenuItem2);

        jMenuItem3.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_T, java.awt.event.InputEvent.CTRL_MASK));
        jMenuItem3.setText("Algorithm settings");
        jMenuItem3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem3ActionPerformed(evt);
            }
        });
        jMenu1.add(jMenuItem3);

        jMenuBar1.add(jMenu1);

        setJMenuBar(jMenuBar1);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(96, 96, 96)
                        .addComponent(jButton1, javax.swing.GroupLayout.PREFERRED_SIZE, 99, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(46, 46, 46))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(jLabel4, javax.swing.GroupLayout.PREFERRED_SIZE, 123, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)))
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel5, javax.swing.GroupLayout.PREFERRED_SIZE, 147, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                        .addComponent(jButton7)
                        .addComponent(jButton2, javax.swing.GroupLayout.PREFERRED_SIZE, 99, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel12, javax.swing.GroupLayout.PREFERRED_SIZE, 147, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(8, 8, 8)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(jButton8)
                            .addComponent(jButton6, javax.swing.GroupLayout.PREFERRED_SIZE, 99, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addGap(27, 27, 27)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(30, 30, 30)
                        .addComponent(jLabel3, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(123, 123, 123)
                        .addComponent(jLabel9, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(133, 133, 133)
                        .addComponent(jLabel10, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(jButton9)
                            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addComponent(jButton3, javax.swing.GroupLayout.PREFERRED_SIZE, 99, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGroup(layout.createSequentialGroup()
                                    .addGap(12, 12, 12)
                                    .addComponent(jLabel6, javax.swing.GroupLayout.PREFERRED_SIZE, 79, javax.swing.GroupLayout.PREFERRED_SIZE))))
                        .addGap(51, 51, 51)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(jLabel7, javax.swing.GroupLayout.PREFERRED_SIZE, 121, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(42, 42, 42)
                                .addComponent(jLabel8))
                            .addGroup(layout.createSequentialGroup()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                    .addComponent(jButton10)
                                    .addComponent(jButton4, javax.swing.GroupLayout.PREFERRED_SIZE, 99, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGap(64, 64, 64)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                    .addComponent(jButton5, javax.swing.GroupLayout.PREFERRED_SIZE, 99, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(jButton11))))))
                .addGap(95, 95, 95))
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(56, 56, 56)
                        .addComponent(jLabel11))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(20, 20, 20)
                        .addComponent(jLabel1)
                        .addGap(77, 77, 77)
                        .addComponent(status_1_check, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(123, 123, 123)
                        .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(121, 121, 121)
                        .addComponent(jLabel06, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(36, 36, 36)
                .addComponent(jLabel11)
                .addGap(37, 37, 37)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel5, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel4)
                    .addComponent(jLabel7)
                    .addComponent(jLabel6)
                    .addComponent(jLabel8)
                    .addComponent(jLabel12, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jButton1, javax.swing.GroupLayout.DEFAULT_SIZE, 94, Short.MAX_VALUE)
                    .addComponent(jButton2, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jButton3, javax.swing.GroupLayout.DEFAULT_SIZE, 94, Short.MAX_VALUE)
                    .addComponent(jButton4, javax.swing.GroupLayout.DEFAULT_SIZE, 94, Short.MAX_VALUE)
                    .addComponent(jButton5, javax.swing.GroupLayout.DEFAULT_SIZE, 94, Short.MAX_VALUE)
                    .addComponent(jButton6, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addGap(36, 36, 36)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(status_1_check, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel10, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel3, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel9, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(8, 8, 8)
                        .addComponent(jLabel1))
                    .addComponent(jLabel06, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jButton7)
                    .addComponent(jButton8)
                    .addComponent(jButton9)
                    .addComponent(jButton10)
                    .addComponent(jButton11))
                .addContainerGap(57, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jButton1MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jButton1MouseClicked
        UTIL.Tools.executeCommand("clean.bat");

        if (button1On) {
            path_trf = GuiTools.openDialog("trf");
            if (!"".equals(path_trf)) {
                check_path_trf = true;
            }
            if (check_path_trf) {
                button1On = false;
                jButton1.setEnabled(false);
                button2On = true;
                jButton2.setEnabled(true);
                JOptionPane.showMessageDialog(rootPane, "trf File Selected: \n" + path_trf,
                        "File Selected", JOptionPane.INFORMATION_MESSAGE);
                status_1_check.setIcon(new ImageIcon(getClass().getResource("/GUI/img/check_icon.png")));
                facade.setPathTrf(path_trf);
                UTIL.Log.setPathtrf(path_trf);
                jButton7.setVisible(true);
            }
        }
    }//GEN-LAST:event_jButton1MouseClicked

    private void jButton2MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jButton2MouseClicked
        if (button2On) {
        //    ParametersSelection p_s = new ParametersSelection();
            SelectRTparam p_s = new SelectRTparam();
            p_s.addWindowListener(new WindowAdapter() {
                public void windowClosed(WindowEvent e) {
                    File fi = new File("temp.tmp");
                    if (fi.exists()) {
                        if (check_path_trf) {
                            try {
                                List L = GuiTools.loadTemp();
                                vol_weight = (String) L.get(0);
                                check_vol_weight = true;
                                speed_weight = (String) L.get(1);
                                check_speed_weight = true;
                                parameters_set = (String[][]) L.get(2);
                                check_parameters_set = true;
                                facade.newParametersSet(parameters_set);
                                fi.delete();
                                //check_extracted_parameters = true;
                                button2On = false;
                                jButton2.setEnabled(false);
                                button6On = true;
                                jButton6.setEnabled(true);
                                jLabel2.setIcon(new ImageIcon(getClass().getResource("/GUI/img/check_icon.png")));
                                /*int[][] ps = FACADE.FacadeTools.stringMatrixToIntMatrix(parameters_set);
                                 int count = PERSISTENCE.ProcessPersistence.countParametersSet(ps, path_trf);
                                 int[][] extracted = PERSISTENCE.ProcessPersistence.exctractParametersSet(ps, path_trf, count);                            

                                 ExtractedParameters EP = new ExtractedParameters(null, true, extracted);
                                 EP.setVisible(true);
                                 int selection = EP.getReturnStatus();
                                 if(selection == 1){
                                 check_extracted_parameters = true;
                                 button2On=false;
                                 jButton2.setEnabled(false);
                                 button3On=true;                
                                 jButton3.setEnabled(true);
                                 jLabel2.setIcon(new ImageIcon(getClass().getResource("/GUI/img/check_icon.png"));                               
                                 } */
                            } catch (Exception ex) {
                                JOptionPane.showMessageDialog(rootPane, "Error, maybe the working folder does'nt have write permisions",
                                        "Error", JOptionPane.ERROR_MESSAGE);
                            }
                        } else {
                            JOptionPane.showMessageDialog(rootPane, "Error, you must select a trf file first",
                                    "Error", JOptionPane.ERROR_MESSAGE);
                        }
                    }
                }
            });
            p_s.setVisible(true);
            jButton8.setVisible(true);
            jButton7.setVisible(false);
        }
    }//GEN-LAST:event_jButton2MouseClicked

    private void jButton3MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jButton3MouseClicked
        if (button3On) {
            ActualData Ad = new ActualData(this, check_path_trf, null, path_trf);
            Ad.setVisible(true);
            int selection = Ad.getReturnStatus();
            if (selection == 1) {
                List L = GuiTools.loadTemp();
                actual_data_netsim = (String[][]) L.get(0);
                actual_data_fresim = (String[][]) L.get(1);
                File fi = new File("temp.tmp");
                fi.delete();
                button3On = false;
                jButton3.setEnabled(false);
                button4On = true;
                jButton4.setEnabled(true);
                jLabel3.setIcon(new ImageIcon(getClass().getResource("/GUI/img/check_icon.png")));
                jButton9.setVisible(false);
                jButton10.setVisible(true);
            }
        }
    }//GEN-LAST:event_jButton3MouseClicked

    private void jButton4MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jButton4MouseClicked
        if (button4On) {
            this.initalizeGAParameters();
            SetCoefficients sGC = new SetCoefficients(this, true);
            sGC.setVisible(true);
            int selection = sGC.getReturnStatus();
            if (selection == 1) {
                facade.setSPSACoefficients(A, a, c, Alpha, Gamma);
                facade.setGAParameters(Pop, Gen, CrossPer, SelPer, Step, Fat, MutPer);
                facade.setSAParameters(IniTemp, CoolRate);
                facade.setTSParameters(Tenure, NeighSize);
                facade.setSimulatorRuns(simulatorRuns);


                facade.setPercentageVolume(vol_weight);
                facade.setPercentageSpeed(speed_weight);
                facade.newActualDataNetsim(actual_data_netsim);
                facade.newActualDataFresim(actual_data_fresim);

                Process proc = null;
                this.filelog = UTIL.Tools.actualTimestamp("yyyyMMdd_hhmmss");
                String LogPath = UTIL.Log.setLogSettings(filelog + "_" + UTIL.Log.getNametrf() + "_" + this.algorithm);
                UTIL.Tools.createFolder(UTIL.Log.getFolder() + File.separator + "Graphs");
                try {
                    proc = Runtime.getRuntime().exec("cmd /C java -jar CShell.jar \"" + LogPath + "\"");
                    System.out.println("java -jar CShell.jar \"" + LogPath + "\"");
                } catch (IOException ex) {
                    Logger.getLogger(CalibrationTool.class.getName()).log(Level.SEVERE, null, ex);
                }

                facade.runCalibration(this.algorithm);

                JOptionPane.showMessageDialog(rootPane, "Calibration done\n\n ", "Calibration done", JOptionPane.INFORMATION_MESSAGE);
                proc.destroy();
                button4On = false;
                jButton4.setEnabled(false);
                button5On = true;
                jButton5.setEnabled(true);
                jLabel9.setIcon(new ImageIcon(getClass().getResource("/GUI/img/check_icon.png")));
                jLabel10.setIcon(new ImageIcon(getClass().getResource("/GUI/img/check_icon.png")));
                UTIL.Tools.executeCommand("clean.bat");
                jButton10.setVisible(false);
                jButton11.setVisible(true);
            }
        }
    }//GEN-LAST:event_jButton4MouseClicked

    private void jButton5MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jButton5MouseClicked
        // TODO add your handling code here:
    }//GEN-LAST:event_jButton5MouseClicked

    private void jButton5ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton5ActionPerformed
        if (button5On) {
       
            int maxGraph2 = GuiTools.maxRangeGraph(facade.getInitialSpeedsGraph());
            Graphics demo2 = new Graphics("Speeds before calibration graph", "Speeds before calibration", "Actual speeds", "Model speeds", maxGraph2, facade.getInitialSpeedsGraph());
            demo2.pack();
            //demo2.setLocationRelativeTo(null);
            demo2.setVisible(true);

            
            int maxGraph4 = GuiTools.maxRangeGraph(facade.getCalibratedSpeedsGraph());
            Graphics demo4 = new Graphics("Speeds after calibration graph", "Speeds after calibration", "Actual speeds", "Model speeds", maxGraph4, facade.getCalibratedSpeedsGraph());
            demo4.pack();
            //demo4.setLocationRelativeTo(null);
            demo4.setVisible(true);

            //Average multiple runs
            int maxGraph6 = GuiTools.maxRangeGraph(facade.getCalibratedSpeedsGraph());
            Graphics demo6 = new Graphics("Speeds after calibration graph", "Speeds multiple runs averaged results", "Actual speeds", "Model speeds", maxGraph6, facade.getAVGSpeedsGraph());
            demo6.pack();
            demo6.setVisible(true);


            int maxGraph1 = GuiTools.maxRangeGraph(facade.getInitialVolumesGraph());
            Graphics demo1 = new Graphics("Volumes before calibration", "Volumes before calibration", "Actual counts", "Model counts", maxGraph1, facade.getInitialVolumesGraph());
            demo1.pack();
            //demo1.setLocationRelativeTo(null);
            demo1.setVisible(true);

            int maxGraph3 = GuiTools.maxRangeGraph(facade.getCalibratedVolumesGraph());
            Graphics demo3 = new Graphics("Volumes after calibration graph", "Volumes after calibration", "Actual counts", "Model counts", maxGraph3, facade.getCalibratedVolumesGraph());
            demo3.pack();
            //demo3.setLocationRelativeTo(null);
            demo3.setVisible(true);

            int maxGraph8 = GuiTools.maxRangeGraph(facade.getCalibratedVolumesGraph());
            Graphics demo8 = new Graphics("Volumes after calibration graph", "Volumes multiple runs averaged results", "Actual counts", "Model counts", maxGraph8, facade.getAVGVolumesGraph());
            demo8.pack();
            demo8.setVisible(true);




            double maxX;
            double maxY;

            double maxX1 = GuiTools.getMax(facade.getInitialGEHGraph(), "x");
            double maxX2 = GuiTools.getMax(facade.getFinalGEHGraph(), "x");
            if (maxX1 > maxX2) {
                maxX = maxX1;
            } else {
                maxX = maxX2;
            }
            maxX = (int) maxX + 1;

            double maxY1 = GuiTools.getMax(facade.getInitialGEHGraph(), "y");
            double maxY2 = GuiTools.getMax(facade.getFinalGEHGraph(), "y");
            if (maxY1 > maxY2) {
                maxY = maxY1;
            } else {
                maxY = maxY2;
            }
            maxY = (int) maxY + 1;

            LineGraphics lG = new LineGraphics("GEH Graph", "GEH Graph",
                    "Link                Red: Before Calibration- Blue: After Calibration", "GEH", (int) maxX, (int) maxY,
                    facade.getInitialGEHGraph(), facade.getFinalGEHGraph());
            LineGraphics lGAVG = new LineGraphics("GEH Graph", "GEH Graph multiple runs averaged results",
                    "Link                Red: Before Calibration- Blue: After Calibration", "GEH", (int) maxX, (int) maxY,
                    facade.getInitialGEHGraph(), facade.getAVGGEHGraph());
            lG.pack();
            lG.setVisible(true);
            //Average multiple runs
            lGAVG.pack();
            lGAVG.setVisible(true);

        }
    }//GEN-LAST:event_jButton5ActionPerformed

    private void jButton2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton2ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jButton2ActionPerformed

    private void jMenuItem1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem1ActionPerformed
        ParametersSelectionEditor pS = new ParametersSelectionEditor();
        pS.setVisible(true);
    }//GEN-LAST:event_jMenuItem1ActionPerformed

    private void jMenuItem2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem2ActionPerformed
        ActualDataEditor aE = new ActualDataEditor(this, false);
        aE.setVisible(true);
    }//GEN-LAST:event_jMenuItem2ActionPerformed

    private void jMenuItem3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem3ActionPerformed
        // TODO add your handling code here:
        SetCoefficients sc = new SetCoefficients(this, false);
        sc.setVisible(true);
    }//GEN-LAST:event_jMenuItem3ActionPerformed

    private void jButton6MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jButton6MouseClicked
        facade.initializeCorsimModel();
        SelectLinks SL = new SelectLinks(null, true, facade.getCorsimmodel());
        SL.setVisible(true);
        int selection = SL.getReturnStatus();
        System.out.println("Return value= "+selection);
        if (selection == 1) {
            List L = GuiTools.loadTemp();
            CorsimModel modelLoaded = (CorsimModel) L.get(0);
            File f = new File("temp.tmp");
            f.delete();
            
            facade.setCorsimModel(modelLoaded);
//            System.out.println(modelLoaded.toStringCascade());
            facade.autocalculateGAParameters();
            
            facade.tempTestImprovedFilter();//Print theta and theta filtered values
            button6On = false;
            jButton6.setEnabled(false);
            button3On = true;
            jButton3.setEnabled(true);
            facade.tempTestImprovedFilter();
            jLabel06.setIcon(new ImageIcon(getClass().getResource("/GUI/img/check_icon.png")));
            jButton8.setVisible(false);
            jButton9.setVisible(true);
        }
    }//GEN-LAST:event_jButton6MouseClicked

    private void jButton6ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton6ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jButton6ActionPerformed

    private void jButton7ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton7ActionPerformed
        // TODO add your handling code here:
        button1On = true;
        jButton1.setEnabled(true);
        button2On = false;
        jButton2.setEnabled(false);
        jButton7.setVisible(false);
    }//GEN-LAST:event_jButton7ActionPerformed

    private void jButton11ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton11ActionPerformed
        // TODO add your handling code here:
        jButton10.setVisible(true);
        button4On = true;
        jButton4.setEnabled(true);
        button5On = false;
        jButton5.setEnabled(false);
        jButton11.setVisible(false);
        
    }//GEN-LAST:event_jButton11ActionPerformed

    private void jButton8ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton8ActionPerformed
        // TODO add your handling code here:
        jButton7.setVisible(true);
        button2On = true;
        jButton2.setEnabled(true);
        button6On = false;
        jButton6.setEnabled(false);
        jButton8.setVisible(false);
    }//GEN-LAST:event_jButton8ActionPerformed

    private void jButton9ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton9ActionPerformed
        // TODO add your handling code here:
        jButton8.setVisible(true);
        button6On = true;
        jButton6.setEnabled(true);
        button3On = false;
        jButton3.setEnabled(false);
        jButton9.setVisible(false);
    }//GEN-LAST:event_jButton9ActionPerformed

    private void jButton10ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton10ActionPerformed
        // TODO add your handling code here:
        jButton9.setVisible(true);
        button3On = true;
        jButton3.setEnabled(true);
        button4On = false;
        jButton4.setEnabled(false);
        jButton10.setVisible(false);
    }//GEN-LAST:event_jButton10ActionPerformed

    public static void main(String args[]) {

        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(CalibrationTool.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(CalibrationTool.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(CalibrationTool.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(CalibrationTool.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                if (!UTIL.Tools.expiratedUsesNumber(100)) {
                    new CalibrationTool().setVisible(true);//new Expirated().setVisible(true);
                } else {
                    new CalibrationTool().setVisible(true);
                }
            }
        });
    }
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButton10;
    private javax.swing.JButton jButton11;
    private javax.swing.JButton jButton2;
    private javax.swing.JButton jButton3;
    private javax.swing.JButton jButton4;
    private javax.swing.JButton jButton5;
    private javax.swing.JButton jButton6;
    private javax.swing.JButton jButton7;
    private javax.swing.JButton jButton8;
    private javax.swing.JButton jButton9;
    private javax.swing.JLabel jLabel06;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JMenu jMenu1;
    private javax.swing.JMenuBar jMenuBar1;
    private javax.swing.JMenuItem jMenuItem1;
    private javax.swing.JMenuItem jMenuItem2;
    private javax.swing.JMenuItem jMenuItem3;
    private javax.swing.JLabel status_1_check;
    // End of variables declaration//GEN-END:variables

    /**
     * @return the A
     */
    public double getA() {
        return A;
    }

    /**
     * @param A the A to set
     */
    public void setA(double A) {
        this.A = A;
    }

    /**
     * @return the Alpha
     */
    public double getAlpha() {
        return Alpha;
    }

    /**
     * @param Alpha the Alpha to set
     */
    public void setAlpha(double Alpha) {
        this.Alpha = Alpha;
    }

    /**
     * @return the Gamma
     */
    public double getGamma() {
        return Gamma;
    }

    /**
     * @param Gamma the Gamma to set
     */
    public void setGamma(double Gamma) {
        this.Gamma = Gamma;
    }

    /**
     * @return the a
     */
    public double geta() {
        return a;
    }

    /**
     * @param a the a to set
     */
    public void seta(double a) {
        this.a = a;
    }

    /**
     * @return the c
     */
    public double getC() {
        return c;
    }

    /**
     * @param c the c to set
     */
    public void setC(double c) {
        this.c = c;
    }

    /**
     * @return the Pop
     */
    public double getPop() {
        return Pop;
    }

    /**
     * @param Pop the Pop to set
     */
    public void setPop(double Pop) {
        this.Pop = Pop;
    }

    /**
     * @return the Gen
     */
    public double getGen() {
        return Gen;
    }

    /**
     * @param Gen the Gen to set
     */
    public void setGen(double Gen) {
        this.Gen = Gen;
    }

    /**
     * @return the CrossPer
     */
    public double getCrossPer() {
        return CrossPer;
    }

    /**
     * @param CrossPer the CrossPer to set
     */
    public void setCrossPer(double CrossPer) {
        this.CrossPer = CrossPer;
    }

    /**
     * @return the SelPer
     */
    public double getSelPer() {
        return SelPer;
    }

    /**
     * @param SelPer the SelPer to set
     */
    public void setSelPer(double SelPer) {
        this.SelPer = SelPer;
    }

    /**
     * @return the Step
     */
    public double getStep() {
        return Step;
    }

    /**
     * @param Step the Step to set
     */
    public void setStep(double Step) {
        this.Step = Step;
    }

    /**
     * @return the Fat
     */
    public double getFat() {
        return Fat;
    }

    /**
     * @param Fat the Fat to set
     */
    public void setFat(double Fat) {
        this.Fat = Fat;
    }

    /**
     * @return the MutPer
     */
    public double getMutPer() {
        return MutPer;
    }

    /**
     * @param MutPer the MutPer to set
     */
    public void setMutPer(double MutPer) {
        this.MutPer = MutPer;
    }

    /**
     * @return the IniTemp
     */
    public double getIniTemp() {
        return IniTemp;
    }

    /**
     * @param IniTemp the IniTemp to set
     */
    public void setIniTemp(double IniTemp) {
        this.IniTemp = IniTemp;
    }

    /**
     * @return the CoolRate
     */
    public double getCoolRate() {
        return CoolRate;
    }

    /**
     * @param CoolRate the CoolRate to set
     */
    public void setCoolRate(double CoolRate) {
        this.CoolRate = CoolRate;
    }

    /**
     * @return the Tenure
     */
    public double getTenure() {
        return Tenure;
    }

    /**
     * @param Tenure the Tenure to set
     */
    public void setTenure(double Tenure) {
        this.Tenure = Tenure;
    }

    /**
     * @return the NeighSize
     */
    public double getNeighSize() {
        return NeighSize;
    }

    /**
     * @param NeighSize the NeighSize to set
     */
    public void setNeighSize(double NeighSize) {
        this.NeighSize = NeighSize;
    }

    public void setAlgorithm(String alg) {
        this.algorithm = alg;
    }

    public String getAlgorithm() {
        return this.algorithm;
    }

    /**
     * @return the simulatorRuns
     */
    public int getSimulatorRuns() {
        return simulatorRuns;
    }

    /**
     * @param simulatorRuns the simulatorRuns to set
     */
    public void setSimulatorRuns(int simulatorRuns) {
        this.simulatorRuns = simulatorRuns;
    }

    private void initalizeGAParameters() {
        this.Gen = facade.getGen();
        this.Pop = facade.getPop();
        this.Fat = facade.getFat();
        this.CrossPer = facade.getCrossPer();
        this.MutPer = facade.getMutPer();
        this.SelPer = facade.getSelPer();
        this.Step = facade.getStep();
    }
}
