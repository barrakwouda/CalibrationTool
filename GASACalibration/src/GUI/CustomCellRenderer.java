/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package GUI;

import java.awt.Color;
import java.awt.Component;
import javax.swing.JCheckBox;
import javax.swing.JTable;
import javax.swing.table.DefaultTableCellRenderer;

/**
 *
 * @author das
 */
class CustomCellRenderer extends DefaultTableCellRenderer {

    private int[][] data;

    public CustomCellRenderer(int[][] data) {
        this.data = data;
    }

    public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
        Component c = new JCheckBox();
        ((JCheckBox) c).setSelected((boolean) value);
        if (data[row][column] == -1) {
            ((JCheckBox) c).setEnabled(false);
        } else {
            c.setBackground(Color.white);
        }
        return c;
    }

    public int[][] getData() {
        return data;
    }

    public void setData(int[][] data) {
        this.data = data;
    }
}