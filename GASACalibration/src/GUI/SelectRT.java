/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package GUI;

import DOMAIN.Model.CorsimModel;
import static GUI.SelectLinks.RET_CANCEL;
import static GUI.SelectLinks.RET_OK;
import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import javax.swing.AbstractAction;
import javax.swing.ActionMap;
import javax.swing.InputMap;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.KeyStroke;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableModel;

/**
 *
 * @author Carlos CFGM
 */
public class SelectRT extends javax.swing.JDialog {

    /**
     * A return status code - returned if Cancel button has been pressed
     */
    public static final int RET_CANCEL = 0;
    /**
     * A return status code - returned if OK button has been pressed
     */
    public static final int RET_OK = 1;

    public CorsimModel model;
    public int linkNumber;

    /**
     * Creates new form SelectRT
     */
    public SelectRT(java.awt.Frame parent, boolean modal, CorsimModel cModel, int linkNum) {
        super(parent, modal);
        initComponents();

        model = cModel;
        linkNumber = linkNum;

        this.setTitle("List of RT for Link: " + model.getLinks().get(linkNumber).getId());
        this.setLocationRelativeTo(null);

        Object[][] dataJTable = new Object[model.getLinks().get(linkNumber).getRecordtypes().size()][5];

        for (int i = 0; i < model.getLinks().get(linkNumber).getRecordtypes().size(); i++) {
            for (int j = 0; j < 5; j++) {
                if (j == 0) {
                    dataJTable[i][0] = model.getLinks().get(linkNumber).getRecordtypes().get(i).getId();
                }
                if (j==1){
                    dataJTable[i][1] = model.getLinks().get(linkNumber).getRecordtypes().get(i).getDescription();
                }
                if (j == 2) {
                    dataJTable[i][2] = selected(i);
                }
                if (j == 3) {
                    if (model.getLinks().get(linkNumber).getRecordtypes().get(i).getCalibrate()) {
                        dataJTable[i][3] = "yes";
                    } else {
                        dataJTable[i][3] = "no";
                    }
                }
                if (j == 4) {
                    dataJTable[i][4] = new JButton("List Entries RT" + model.getLinks().get(linkNumber).getRecordtypes().get(i).getId());
                }
            }
        }

        String[] columns = {"Record Type","Description", "All entries?", "Has entries to calibrate?", "View - Edit"};

        DefaultTableModel modelJT = new DefaultTableModel(dataJTable, columns) {
            @Override
            public Class getColumnClass(int columnIndex) {
                if (columnIndex == 0 || columnIndex == 1 || columnIndex == 2 ||columnIndex==3) {
                    if (columnIndex == 2) {
                        return Boolean.class;
                    } else {
                        return String.class;
                    }
                } else {
                    return JButton.class;
                }
            }

            @Override
            public boolean isCellEditable(int row, int column) {
                return !(this.getColumnClass(column).equals(JButton.class));
            }
        };

        jTable1.setDefaultRenderer(JButton.class, new TableCellRenderer() {
            @Override
            public Component getTableCellRendererComponent(JTable jtable, Object object, boolean isSelected, boolean hasFocus, int row, int col) {
                return (Component) object;
            }
        });

        DefaultTableCellRenderer centerRenderer = new DefaultTableCellRenderer();
        centerRenderer.setHorizontalAlignment(JLabel.CENTER);
        jTable1.setDefaultRenderer(String.class, centerRenderer);

        jTable1.setShowHorizontalLines(true);
        jTable1.setShowVerticalLines(true);
        jTable1.setModel(modelJT);

        jTable1.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                int row = jTable1.rowAtPoint(e.getPoint());
                int col = jTable1.columnAtPoint(e.getPoint());
                if (jTable1.getModel().getColumnClass(col).equals(JButton.class)) {
                    doClose(RET_OK);
                    SelectEntries sE = new SelectEntries(null, true, model, linkNumber, row);
                    sE.setVisible(true);
                }
            }
        });

        tableListener(jTable1);

        // Close the dialog when Esc is pressed
        /*String cancelName = "cancel";
        InputMap inputMap = getRootPane().getInputMap(JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT);
        inputMap.put(KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0), cancelName);
        ActionMap actionMap = getRootPane().getActionMap();
        actionMap.put(cancelName, new AbstractAction() {
            public void actionPerformed(ActionEvent e) {
                doClose(RET_CANCEL);
            }
        });*/
    }

    private void tableListener(JTable table) {
        table.getModel().addTableModelListener(new TableModelListener() {
            @Override
            public void tableChanged(TableModelEvent e) {
                int col = e.getColumn();
                if (col == 2) {
                    int row = e.getFirstRow();
                    TableModel tModel = (TableModel) e.getSource();
                    Object data = tModel.getValueAt(row, col);
                    boolean flag = (boolean) data;
                    if (flag) {
                        int result = JOptionPane.showConfirmDialog((Component) null, "Are you sure you want to select all entries for this record type in this link?",
                                "alert", JOptionPane.OK_CANCEL_OPTION);
                        if (result == 0) {
                            for (int i = 0; i < model.getLinks().get(linkNumber).getRecordtypes().get(row).getEntries().size(); i++) {
                                model.getLinks().get(linkNumber).getRecordtypes().get(row).getEntries().get(i).setCalibrate(flag);
                            }
                        }
                    } else {
                        int result = JOptionPane.showConfirmDialog((Component) null, "Are you sure you want to unselect all entries for this record type in this link?",
                                "alert", JOptionPane.OK_CANCEL_OPTION);
                        if (result == 0) {
                            for (int i = 0; i < model.getLinks().get(linkNumber).getRecordtypes().get(row).getEntries().size(); i++) {
                                model.getLinks().get(linkNumber).getRecordtypes().get(row).getEntries().get(i).setCalibrate(flag);
                            }
                        }
                    }
                    doClose(RET_OK);
                    SelectRT sRT = new SelectRT(null, true, model, linkNumber);
                    sRT.setVisible(true);
                }

            }
        });
    }

    public boolean selected(int rt) {
        for (int i = 0; i < model.getLinks().get(linkNumber).getRecordtypes().get(rt).getEntries().size(); i++) {
            if (model.getLinks().get(linkNumber).getRecordtypes().get(rt).getEntries().get(i).getCalibrate() == false) {
                return false;
            }
        }
        return true;
    }

    /**
     * @return the return status of this dialog - one of RET_OK or RET_CANCEL
     */
    public int getReturnStatus() {
        return returnStatus;
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        cancelButton = new javax.swing.JButton();
        jScrollPane1 = new javax.swing.JScrollPane();
        jTable1 = new javax.swing.JTable();

        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowClosing(java.awt.event.WindowEvent evt) {
                closeDialog(evt);
            }
        });

        cancelButton.setText("Back (links list)");
        cancelButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cancelButtonActionPerformed(evt);
            }
        });

        jTable1.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        jScrollPane1.setViewportView(jTable1);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(cancelButton))
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 710, Short.MAX_VALUE))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 295, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(cancelButton)
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void cancelButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cancelButtonActionPerformed
        doClose(RET_CANCEL);
        SelectLinks sL = new SelectLinks(null, true, model);
        sL.setVisible(true);
    }//GEN-LAST:event_cancelButtonActionPerformed

    /**
     * Closes the dialog
     */
    private void closeDialog(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_closeDialog
        doClose(RET_CANCEL);
    }//GEN-LAST:event_closeDialog

    private void doClose(int retStatus) {
        returnStatus = retStatus;
        setVisible(false);
        dispose();
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(SelectRT.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(SelectRT.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(SelectRT.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(SelectRT.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the dialog */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                SelectRT dialog = new SelectRT(new javax.swing.JFrame(), true, null, -1);
                dialog.addWindowListener(new java.awt.event.WindowAdapter() {
                    @Override
                    public void windowClosing(java.awt.event.WindowEvent e) {
                        System.exit(0);
                    }
                });
                dialog.setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton cancelButton;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTable jTable1;
    // End of variables declaration//GEN-END:variables

    private int returnStatus = RET_CANCEL;
}
