/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package GUI;

import DOMAIN.Model.CorsimModel;
import DOMAIN.Model.Link;
import PERSISTENCE.InputPersistence;
import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import javax.swing.AbstractAction;
import javax.swing.ActionMap;
import javax.swing.InputMap;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.KeyStroke;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableModel;

/**
 *
 * @author Carlos CFGM
 */
public class SelectLinks extends javax.swing.JDialog {

    /**
     * A return status code - returned if Cancel button has been pressed
     */
    public static final int RET_CANCEL = 0;
    /**
     * A return status code - returned if OK button has been pressed
     */
    public static final int RET_OK = 1;

    public CorsimModel model;

    /**
     * Creates new form SelectLinks
     */
    public SelectLinks(java.awt.Frame parent, boolean modal, CorsimModel cModel) {
        super(parent, modal);
        initComponents();

        this.setTitle("Edit link entries");
        this.setLocationRelativeTo(null);

        model = cModel;

        Object[][] dataJTable = new Object[model.getLinks().size()][4];

        for (int i = 0; i < model.getLinks().size(); i++) {
            for (int j = 0; j < 4; j++) {
                if (j == 0) {
                    dataJTable[i][0] = model.getLinks().get(i).getDescription();
                }
                if (j == 1) {
                    dataJTable[i][1] = selected(i);
                }
                if (j == 2) {
                    if (model.getLinks().get(i).getCalibrate()) {
                        dataJTable[i][2] = "yes";
                    } else {
                        dataJTable[i][2] = "no";
                    }
                }
                if (j == 3) {
                    dataJTable[i][3] = new JButton("List RT - Link " + model.getLinks().get(i).getId());
                }
            }
        }

        String[] columns = {"Link", "All entries?", "Has entries to calibrate?", "View - Edit"};

        DefaultTableModel modelJT = new DefaultTableModel(dataJTable, columns) {
            @Override
            public Class getColumnClass(int columnIndex) {
                if (columnIndex == 0 || columnIndex == 1 || columnIndex == 2) {
                    if (columnIndex == 1) {
                        return Boolean.class;
                    } else {
                        return String.class;
                    }
                } else {
                    return JButton.class;
                }
            }

            @Override
            public boolean isCellEditable(int row, int column) {
                return !(this.getColumnClass(column).equals(JButton.class));
            }
        };

        jTable1.setDefaultRenderer(JButton.class, new TableCellRenderer() {
            @Override
            public Component getTableCellRendererComponent(JTable jtable, Object object, boolean isSelected, boolean hasFocus, int row, int col) {
                return (Component) object;
            }
        });

        DefaultTableCellRenderer centerRenderer = new DefaultTableCellRenderer();
        centerRenderer.setHorizontalAlignment(JLabel.CENTER);
        jTable1.setDefaultRenderer(String.class, centerRenderer);

        jTable1.setShowHorizontalLines(true);
        jTable1.setShowVerticalLines(true);
        jTable1.setModel(modelJT);

        jTable1.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                int row = jTable1.rowAtPoint(e.getPoint());
                int col = jTable1.columnAtPoint(e.getPoint());
                if (jTable1.getModel().getColumnClass(col).equals(JButton.class)) {
                    doClose(RET_OK);
                    SelectRT sRT = new SelectRT(null, true, model, row);
                    sRT.setVisible(true);
                }
            }
        });

        tableListener(jTable1);

        // Close the dialog when Esc is pressed
        /*String cancelName = "cancel";
        InputMap inputMap = getRootPane().getInputMap(JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT);

        inputMap.put(KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0), cancelName);
        ActionMap actionMap = getRootPane().getActionMap();

        actionMap.put(cancelName, new AbstractAction() {

            public void actionPerformed(ActionEvent e) {
                doClose(RET_CANCEL);
            }
        }
        );*/
    }

    private void tableListener(JTable table) {
        table.getModel().addTableModelListener(new TableModelListener() {
            @Override
            public void tableChanged(TableModelEvent e) {
                int col = e.getColumn();
                if (col == 1) {
                    int row = e.getFirstRow();
                    TableModel tModel = (TableModel) e.getSource();
                    Object data = tModel.getValueAt(row, col);
                    boolean flag = (boolean) data;
                    if (flag) {
                        int result = JOptionPane.showConfirmDialog((Component) null, "Are you sure you want to select all entries of all record types in this link?",
                                "alert", JOptionPane.OK_CANCEL_OPTION);
                        if (result == 0) {
                            for (int i = 0; i < model.getLinks().get(row).getRecordtypes().size(); i++) {
                                for (int j = 0; j < model.getLinks().get(row).getRecordtypes().get(i).getEntries().size(); j++) {
                                    model.getLinks().get(row).getRecordtypes().get(i).getEntries().get(j).setCalibrate(flag);
                                }
                            }
                        }
                    } else {
                        int result = JOptionPane.showConfirmDialog((Component) null, "Are you sure you want to unselect all entries of all record types in this link?",
                                "alert", JOptionPane.OK_CANCEL_OPTION);
                        if (result == 0) {
                            for (int i = 0; i < model.getLinks().get(row).getRecordtypes().size(); i++) {
                                for (int j = 0; j < model.getLinks().get(row).getRecordtypes().get(i).getEntries().size(); j++) {
                                    model.getLinks().get(row).getRecordtypes().get(i).getEntries().get(j).setCalibrate(flag);
                                }
                            }
                        }
                    }
                    doClose(RET_OK);
                    SelectLinks sL = new SelectLinks(null, true, model);
                    sL.setVisible(true);
                }

            }
        });
    }

    public boolean selected(int link) {
        for (int i = 0; i < model.getLinks().get(link).getRecordtypes().size(); i++) {
            for (int j = 0; j < model.getLinks().get(link).getRecordtypes().get(i).getEntries().size(); j++) {
                if(model.getLinks().get(link).getRecordtypes().get(i).getEntries().get(j).getCalibrate()==false){
                    return false;
                }
            }
        }
        return true;
    }

    /**
     * @return the return status of this dialog - one of RET_OK or RET_CANCEL
     */
    public int getReturnStatus() {
        return returnStatus;
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        okButton = new javax.swing.JButton();
        jScrollPane1 = new javax.swing.JScrollPane();
        jTable1 = new javax.swing.JTable();
        uncheck = new javax.swing.JButton();
        jButton1 = new javax.swing.JButton();
        jButton2 = new javax.swing.JButton();
        jButton3 = new javax.swing.JButton();
        jSpinner1 = new javax.swing.JSpinner();
        jLabel1 = new javax.swing.JLabel();
        jMenuBar1 = new javax.swing.JMenuBar();
        jMenu1 = new javax.swing.JMenu();
        jMenuItem1 = new javax.swing.JMenuItem();
        jMenuItem2 = new javax.swing.JMenuItem();

        setPreferredSize(new java.awt.Dimension(730, 380));
        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowClosing(java.awt.event.WindowEvent evt) {
                closeDialog(evt);
            }
        });

        okButton.setText("Finish");
        okButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                okButtonActionPerformed(evt);
            }
        });

        jTable1.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        jScrollPane1.setViewportView(jTable1);

        uncheck.setText("Uncheck randomly");
        uncheck.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                uncheckActionPerformed(evt);
            }
        });

        jButton1.setText("Netsim Only");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        jButton2.setText("Fresim Only");
        jButton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton2ActionPerformed(evt);
            }
        });

        jButton3.setText("Select All");
        jButton3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton3ActionPerformed(evt);
            }
        });

        jSpinner1.setModel(new javax.swing.SpinnerNumberModel(0, 0, 100, 1));

        jLabel1.setText("% of the links");

        jMenu1.setText("File");

        jMenuItem1.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_S, java.awt.event.InputEvent.CTRL_MASK));
        jMenuItem1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/GUI/img/Save-icon.png"))); // NOI18N
        jMenuItem1.setText("Save");
        jMenuItem1.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jMenuItem1MouseClicked(evt);
            }
        });
        jMenuItem1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem1ActionPerformed(evt);
            }
        });
        jMenu1.add(jMenuItem1);

        jMenuItem2.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_O, java.awt.event.InputEvent.CTRL_MASK));
        jMenuItem2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/GUI/img/load.png"))); // NOI18N
        jMenuItem2.setText("Load");
        jMenuItem2.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jMenuItem2MouseClicked(evt);
            }
        });
        jMenuItem2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem2ActionPerformed(evt);
            }
        });
        jMenu1.add(jMenuItem2);

        jMenuBar1.add(jMenu1);

        setJMenuBar(jMenuBar1);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(10, 10, 10)
                        .addComponent(jButton3)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jButton2)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jButton1)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(uncheck)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jSpinner1, javax.swing.GroupLayout.PREFERRED_SIZE, 55, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabel1)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(okButton, javax.swing.GroupLayout.PREFERRED_SIZE, 67, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 710, Short.MAX_VALUE))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 290, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(okButton)
                    .addComponent(uncheck)
                    .addComponent(jButton1)
                    .addComponent(jButton2)
                    .addComponent(jButton3)
                    .addComponent(jSpinner1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel1))
                .addContainerGap())
        );

        getRootPane().setDefaultButton(okButton);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void okButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_okButtonActionPerformed
        List temp = new ArrayList();
        temp.add(model);
        GuiTools.saveTemp(temp);
        doClose(RET_OK);
       
    }//GEN-LAST:event_okButtonActionPerformed

    /**
     * Closes the dialog
     */
    private void closeDialog(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_closeDialog
        doClose(RET_CANCEL);
    }//GEN-LAST:event_closeDialog

    private void jMenuItem1MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jMenuItem1MouseClicked

    }//GEN-LAST:event_jMenuItem1MouseClicked

    private void jMenuItem2MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jMenuItem2MouseClicked

    }//GEN-LAST:event_jMenuItem2MouseClicked

    private void jMenuItem1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem1ActionPerformed
        String path = GuiTools.saveDialog("sel");
        if (path != "") {
            InputPersistence.saveModelInput(model, path);
        }
    }//GEN-LAST:event_jMenuItem1ActionPerformed

    private void jMenuItem2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem2ActionPerformed
        String path = GuiTools.openDialog("sel");
        if (path != "") {
            model = PERSISTENCE.InputPersistence.loadModelInput(path);
            doClose(RET_OK);
            SelectLinks sL = new SelectLinks(null, true, model);
            sL.setVisible(true);
        }
    }//GEN-LAST:event_jMenuItem2ActionPerformed

    private void uncheckActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_uncheckActionPerformed

        int compt = (int)jSpinner1.getValue()*(int)model.getLinks().size()/100;
       Integer [] arr = new Integer [(int)model.getLinks().size()];
        for (int i = 0 ; i<arr.length;i++){
            arr[i] =i;
        }
        Collections.shuffle(Arrays.asList(arr));
        for (int i =0; i< (int)model.getLinks().size()&&compt>0;i++) {
            int rand =arr[i] ;
            if(model.getLinks().get(rand).getCalibrate() ){
                model.getLinks().get(rand).setCalibrate(false);
                compt--;
            }
        } 
        doClose(RET_OK);
        SelectLinks sL = new SelectLinks(null, true, model);
        sL.setVisible(true);
    }//GEN-LAST:event_uncheckActionPerformed

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed

        for (int i = 0; i < model.getLinks().size(); i++) {
                for (int j = 0; j < model.getLinks().get(i).getRecordtypes().size(); j++) {
                    if(model.getLinks().get(i).getRecordtypes().get(j).getId().equals("11") || model.getLinks().get(i).getDescription().equals("GLOBAL PARAMETERS")){
                       model.getLinks().get(i).setCalibrate(true);
                    }else{
                        model.getLinks().get(i).setCalibrate(false);
                    }
                }
                
            }

        doClose(RET_OK);
        SelectLinks sL = new SelectLinks(null, true, model);
        sL.setVisible(true);
    }//GEN-LAST:event_jButton1ActionPerformed

    private void jButton2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton2ActionPerformed

        for (int i = 0; i < model.getLinks().size(); i++) {
                for (int j = 0; j < model.getLinks().get(i).getRecordtypes().size(); j++) {
                    if(model.getLinks().get(i).getRecordtypes().get(j).getId().equals("11")){
                            model.getLinks().get(i).setCalibrate(false);
                        }else{
                            model.getLinks().get(i).setCalibrate(true);
                        }
                    }
                }
        doClose(RET_OK);
        SelectLinks sL = new SelectLinks(null, true, model);
        sL.setVisible(true);
    }//GEN-LAST:event_jButton2ActionPerformed

    private void jButton3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton3ActionPerformed

        for (int i = 0; i < model.getLinks().size(); i++) {
            model.getLinks().get(i).setCalibrate(true);
        }
        doClose(RET_OK);
        SelectLinks sL = new SelectLinks(null, true, model);
        sL.setVisible(true);
    }//GEN-LAST:event_jButton3ActionPerformed

    private void doClose(int retStatus) {
        returnStatus = retStatus;
        setVisible(false);
        dispose();
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;

                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(SelectLinks.class
                    .getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(SelectLinks.class
                    .getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(SelectLinks.class
                    .getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(SelectLinks.class
                    .getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the dialog */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                SelectLinks dialog = new SelectLinks(new javax.swing.JFrame(), true, null);
                dialog.addWindowListener(new java.awt.event.WindowAdapter() {
                    @Override
                    public void windowClosing(java.awt.event.WindowEvent e) {
                        System.exit(0);
                    }
                });
                dialog.setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButton2;
    private javax.swing.JButton jButton3;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JMenu jMenu1;
    private javax.swing.JMenuBar jMenuBar1;
    private javax.swing.JMenuItem jMenuItem1;
    private javax.swing.JMenuItem jMenuItem2;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JSpinner jSpinner1;
    private javax.swing.JTable jTable1;
    private javax.swing.JButton okButton;
    private javax.swing.JButton uncheck;
    // End of variables declaration//GEN-END:variables

    private int returnStatus = RET_CANCEL;
}
