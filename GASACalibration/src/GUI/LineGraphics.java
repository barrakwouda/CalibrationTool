package GUI;

import java.awt.BasicStroke;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Rectangle;
import java.awt.Shape;
import java.awt.geom.Line2D;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;
import javax.swing.JFrame;
import org.jfree.chart.*;
import org.jfree.chart.annotations.XYShapeAnnotation;
import org.jfree.chart.axis.NumberAxis;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.plot.XYPlot;
import org.jfree.chart.renderer.xy.XYItemRenderer;
import org.jfree.data.xy.XYDataset;
import org.jfree.data.xy.XYSeries;
import org.jfree.data.xy.XYSeriesCollection;
import org.jfree.util.ShapeUtilities;

public class LineGraphics extends JFrame {

    private static final int SIZE = 500;
    private static String graph_title;
    private static String X_Title;
    private static String Y_Title;
    private static int maxX;
    private static int maxY;
    private static double[][] data;
    private static double[][] data2;

    public LineGraphics(String window_tittle, String graph_tittle, String X_Title, String Y_Title,
            int maxX, int maxY, double[][] data, double[][] data2) {
        super(window_tittle);
        LineGraphics.graph_title = graph_tittle;
        LineGraphics.X_Title = X_Title;
        LineGraphics.Y_Title = Y_Title;
        LineGraphics.maxX = maxX;
        LineGraphics.maxY = maxY;
        LineGraphics.data = data;
        LineGraphics.data2 = data2;
        final ChartPanel chartPanel = createDemoPanel();
        chartPanel.setPreferredSize(new Dimension(SIZE * 2, SIZE));
        this.add(chartPanel, BorderLayout.CENTER);
    }

    private ChartPanel createDemoPanel() {
        JFreeChart jfreechart = ChartFactory.createScatterPlot(
                graph_title, X_Title, Y_Title, createSampleData(),
                PlotOrientation.VERTICAL, false, true, false);
        XYPlot xyPlot = (XYPlot) jfreechart.getPlot();
        XYItemRenderer renderer = xyPlot.getRenderer();
        renderer.setSeriesPaint(0, Color.blue);
        Shape dot = ShapeUtilities.createDiamond(3);
        renderer.setSeriesShape(0, dot);
        adjustAxis((NumberAxis) xyPlot.getDomainAxis(), true);
        adjustAxis((NumberAxis) xyPlot.getRangeAxis(), false);
        /////////////////////////////////////////////////////////////////////////////
        for (int i = 0; i < data.length - 1; i++) {
            xyPlot.addAnnotation(new XYShapeAnnotation(
                    new Line2D.Double(data[i][0], data[i][1], data[i + 1][0], data[i + 1][1]), new BasicStroke(1.0f,
                    BasicStroke.CAP_BUTT, BasicStroke.JOIN_MITER, 10.0f), Color.red));
        }
        for (int i = 0; i < data2.length - 1; i++) {
            xyPlot.addAnnotation(new XYShapeAnnotation(
                    new Line2D.Double(data2[i][0], data2[i][1], data2[i + 1][0], data2[i + 1][1]), new BasicStroke(1.0f,
                    BasicStroke.CAP_BUTT, BasicStroke.JOIN_MITER, 10.0f), Color.blue));
        }

        LegendItemCollection chartLegend = new LegendItemCollection();
        Shape shape = new Rectangle(10, 10);
        chartLegend.add(new LegendItem("Initial GEH", null, null, null, shape, Color.red));
        chartLegend.add(new LegendItem("Final GEH", null, null, null, shape, Color.blue));
        xyPlot.setFixedLegendItems(chartLegend);


        /*xyPlot.addAnnotation(new XYShapeAnnotation(
         new Line2D.Double(2, 2, 3, 6), new BasicStroke(1.0f,
         BasicStroke.CAP_BUTT, BasicStroke.JOIN_MITER, 10.0f), Color.blue));*/



        try {
            BufferedImage img = jfreechart.createBufferedImage(900, 500);
            OutputStream out = new FileOutputStream(new File(UTIL.Log.getFolder() + File.separator + "Graphs" + File.separator + graph_title + ".png"));
            ImageIO.write(img, "png", out);
            out.close();
        } catch (FileNotFoundException ex) {
            Logger.getLogger(Graphics.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(Graphics.class.getName()).log(Level.SEVERE, null, ex);
        }



        return new ChartPanel(jfreechart);
    }

    private void adjustAxis(NumberAxis axis, boolean vertical) {
        if (vertical) {
            axis.setRange(1, maxX);
            axis.setVerticalTickLabels(vertical);
        } else {
            axis.setRange(0, maxY);
            axis.setVerticalTickLabels(vertical);
        }
    }

    private XYDataset createSampleData() {
        XYSeriesCollection xySeriesCollection = new XYSeriesCollection();
        XYSeries series = new XYSeries("Random");
        /*for(int i=0;i<data.length;i++){
         series.add(data[i][0],data[i][1]);
         }*/
        xySeriesCollection.addSeries(series);
        return xySeriesCollection;
    }
}